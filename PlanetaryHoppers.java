/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import dsc.netgame.*;
import dsc.util.CmdLine;
import java.io.*;
import phworld.PHWorldCreator;

public class PlanetaryHoppers extends CloseableLayoutableFrame 
    implements ActionListener {

    private Image backgroundImage;
    private CmdLine cmdLine;

    private Button createButton(String name) {
	Button b = new Button(name);
	b.addActionListener(this);
	return b;
    }

    public void update(Graphics g) {
	paint(g);
    }

    public void paint(Graphics g) {
	g.drawImage(backgroundImage,0,0,this);
    }

    public PlanetaryHoppers() { 
	int xSize, ySize;

	backgroundImage = 
	  GraphicsLoader.getImage("data/graph/still/background/mainimage.jpg");

	Insets in = getInsets();
	xSize=backgroundImage.getWidth(null)+in.right+in.left;
	ySize=backgroundImage.getHeight(null)+in.bottom+in.top;

	setSize(xSize,ySize); // center?
	setResizable(false);

	Button okButton=createButton("OK");
    
	setLayout(null);
	add(okButton);
	okButton.setBounds(xSize-60,ySize-90,50,20);
    }

    public PlanetaryHoppers(String s) {    
    }
    
    public void actionPerformed(ActionEvent evt) {
	String arg = evt.getActionCommand();
    
	if(arg.equals("OK")) {
	    dispose();
	    Frame f = new ConnectsWindow(new phworld.PHWorldCreator(cmdLine));
	    f.show();
	}
    }

    private String getRequiredOption(CmdLine cmdLine, String op) {
	String s = cmdLine.getStringOption(op, null);

	if(s == null) {
	    System.err.println("Client startup requires command line " +
			       "argument '" + op + "'");
	    System.exit(1);
	}

	return s;
    }

    private static String getPassword(String passfile) {
	if(passfile == null)
	    return "";

	BufferedReader in=null;
	try {
	    File f   = new File(passfile);
	    in=new BufferedReader(new FileReader(f));
	} catch (Exception e) {
	    System.err.println("Can't open password file");
	    System.exit(1);
	}

	String s = null;
	try {
	    s = in.readLine();
	} catch (IOException e) {
	    System.err.println("Can't read password file");
	    System.exit(1);
	}

	try {
	    in.close();
	} catch (IOException e) {}

	if(s == null) {
	    System.err.println("Null password in password file");
	    System.exit(1);
	}

	return s;
    }

    public static void main(String[] args) {
	CmdLine cmdLine = new CmdLine(args);
	CmdLine nullLine = new CmdLine(null);

	if(args.length == 1 && args[0].equals("-quick")) {
	    Frame f = new 
		ConnectsWindow(new phworld.PHWorldCreator(nullLine));
	    f.show();
	} else if(args.length >= 1 && args[0].equals("-host")) {
	    HostController c = 
		new HostController("PHgame",
				   new phworld.PHWorldCreator(cmdLine), 
				   false,
				   null);
	    c.start();
	} else if(args.length > 0) {
	    String ip = cmdLine.getStringOption("-hostaddr", "127.0.0.1");
	    int  port = cmdLine.getIntOption("-port", 9090, 1024, 65535);
	    String name = cmdLine.getStringOption("-name", "");
	    String passfile = cmdLine.getStringOption("-passfile", null);
	    String pass = getPassword(passfile);
	    
	    ClientController c = 
		new ClientController(ip,
				     port,
				     name,
				     pass,
				     new PHWorldCreator(nullLine));

	    if(args[0].equals("-ai")) {
		/* fire up ai */
		c.setAI(new phworld.ai.defender.AI());
	    }
	    
	    c.start();
	} else {
	    PlanetaryHoppers f=new PlanetaryHoppers();
	    f.cmdLine = new CmdLine(args);
	    f.show();
	    
	}
    }
}
