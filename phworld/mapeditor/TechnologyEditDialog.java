package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class TechnologyEditDialog extends Dialog implements ActionListener{

    Button    okButton;
    Button    cancelButton;
    
    TextField techStringName;

    Choice    hullChoice,engineChoice,weaponChoice,shieldChoice,miscChoice;

    String    originalTechnologyString;

    PlanetEditorWindow parentFrame;

    PlanetEditorPlanet selPlanet;
    Hashtable planetProperties;

    GridBagLayout gb;
    GridBagConstraints gbc;

    static int BOTH = GridBagConstraints.BOTH;
    static int WEST = GridBagConstraints.WEST;
    static int CENTER = GridBagConstraints.CENTER;
    static int EAST = GridBagConstraints.EAST;


    public TechnologyEditDialog (PlanetEditorWindow parent) {
	super(parent, "Technology Edit Dialog", true);

	parentFrame = parent;

	okButton =  new Button("OK");
	cancelButton =  new Button("Cancel");

	okButton.addActionListener(this);
	cancelButton.addActionListener(this);

	hullChoice = new Choice();
	setupChoice(hullChoice);

	engineChoice = new Choice();
	setupChoice(engineChoice);

	weaponChoice = new Choice();
	setupChoice(weaponChoice);

	shieldChoice = new Choice();
	setupChoice(shieldChoice);

	miscChoice = new Choice();
	setupChoice(miscChoice);


	gb = new GridBagLayout();
	gbc = new GridBagConstraints();
	setLayout(gb);

	try {

	    //LABELS
	    addComponent(this,new Label("Technology set name"),0,0,1,1,BOTH,WEST);
	    addComponent(this,new Label("Hull"),0,2,1,1,BOTH,WEST);
	    addComponent(this,new Label("Engine"),0,3,1,1,BOTH,WEST);
	    addComponent(this,new Label("Weapon"),0,4,1,1,BOTH,WEST);
	    addComponent(this,new Label("Shield"),0,5,1,1,BOTH,WEST);
	    addComponent(this,new Label("Misc"),0,6,1,1,BOTH,WEST);

	    //TEXTFIELDS
	    addComponent(this,techStringName = new TextField(10),1,0,1,1,BOTH,CENTER);

	    //CHOICES
	    addComponent(this,hullChoice,1,2,1,1,BOTH,CENTER);
	    addComponent(this,engineChoice,1,3,1,1,BOTH,CENTER);
	    addComponent(this,weaponChoice,1,4,1,1,BOTH,CENTER);
	    addComponent(this,shieldChoice,1,5,1,1,BOTH,CENTER);
	    addComponent(this,miscChoice,1,6,1,1,BOTH,CENTER);

	    //BUTTONS
	    addComponent(this,okButton,0,10,1,1,BOTH,CENTER);
	    addComponent(this,cancelButton,1,10,2,1,BOTH,CENTER);
	    
	} catch (AWTException e) {}

	setSize(300,300);
    }

    private void addComponent(Container cont,Component comp, int gridx, int gridy,
			      int gridwidth,int gridheight,int fill,int anchor) 
	throws AWTException {
	gbc.gridx = gridx; gbc.gridy=gridy;
	gbc.gridwidth = gridwidth; gbc.gridheight = gridheight;
	gbc.fill = fill;
	gbc.anchor=anchor;
	cont.add(comp,gbc);
    }


    public void actionPerformed(ActionEvent e) {
	validate();

	if (e.getActionCommand().equals("OK")) {
	    String techString = new String();

	    techString += hullChoice.getSelectedItem() + "/";
	    techString += engineChoice.getSelectedItem() + "/";
	    techString += weaponChoice.getSelectedItem() + "/";
	    techString += shieldChoice.getSelectedItem() + "/";
	    techString += miscChoice.getSelectedItem();

	    String newName = "T" + techStringName.getText().trim();

	    planetProperties.remove(newName);

	    planetProperties.put(newName,techString);

	    parentFrame.resetMatProdTechDialog(2,newName); // 0 - material, 1 - prod ...

	} else if (e.getActionCommand().equals("Cancel")) {
	} else {
	    System.err.println("Undefined Button Pressed!");
	    System.exit(3);
	}

	setVisible(false);
    }

    public void setupTechnologyEditDialog(PlanetEditorPlanet planet, Hashtable pp, String tString) {

	selPlanet = planet;
	planetProperties = pp;


	if (tString.equals("-"))
	    techStringName.setText(selPlanet.getName());
	else techStringName.setText(tString.substring(1));


	StringTokenizer st;

	if (tString.equals("-")) {
	    if (selPlanet.getPlanetTypeString().equals("HOME")) {
		phworld.TechLevel tl = phworld.PlanetType.getHomePlanetTechLevel();
		String s = tl.getHullTech()+ "/" +
	  	           tl.getEngineTech()+ "/" +
	  	           tl.getWeaponTech()+ "/" +
	  	           tl.getShieldTech()+ "/" +
		           tl.getMiscTech();
		st = new StringTokenizer(s,"/");
	    }
	    else st = new StringTokenizer("0/0/0/0/0","/");
	}	
	else {
	    st = new StringTokenizer((String) planetProperties.get(
				  tString),"/");
	}

	hullChoice.select(st.nextToken());
	engineChoice.select(st.nextToken());
	weaponChoice.select(st.nextToken());
	shieldChoice.select(st.nextToken());
	miscChoice.select(st.nextToken());

	show();

    }

    private void setupChoice(Choice choice) {

	choice.removeAll();

	for(int i=0;i <= phworld.TechLevel.MAX_TECH;i++) {
	    choice.add(i+"");
	}
    }

}
