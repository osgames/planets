package phworld.mapeditor;

import java.awt.*;
import java.awt.event.*;

public class MapSizeDialog extends Dialog implements ActionListener{

    int       xs;
    int       ys;
    TextField xSize;
    TextField ySize;
    
    Button    okButton;
    Button    cancelButton;

    GridBagLayout gb;
    GridBagConstraints gbc;

    static int BOTH = GridBagConstraints.BOTH;
    static int WEST = GridBagConstraints.WEST;
    static int CENTER = GridBagConstraints.CENTER;
    static int EAST = GridBagConstraints.EAST;



    public MapSizeDialog (Frame parent,int x,int y) {
	super(parent, "Change Map Size", true);

	gb = new GridBagLayout();
	gbc = new GridBagConstraints();
	setLayout(gb);

	xs=x; ys=y;
	xSize = new TextField(Integer.toString(xs),6);
	ySize = new TextField(Integer.toString(ys),6);

	okButton =  new Button("OK");
	cancelButton =  new Button("Cancel");
	okButton.addActionListener(this);
	cancelButton.addActionListener(this);

	try {
	    addComponent(this,new Label("X Size"),0,0,2,1,BOTH,WEST);
	    addComponent(this,new Label("Y Size"),0,1,2,1,BOTH,WEST);

	    addComponent(this,xSize,2,0,2,1,BOTH,EAST);
	    addComponent(this,ySize,2,1,2,1,BOTH,EAST);
	    
	    addComponent(this,okButton,0,2,2,1,BOTH,CENTER);
	    addComponent(this,cancelButton,2,2,2,1,BOTH,CENTER);
	} catch (AWTException e) {}

	setSize(160,120);
    }

    private void addComponent(Container cont,Component comp, int gridx, int gridy,
			      int gridwidth,int gridheight,int fill,int anchor) 
	throws AWTException {
	gbc.gridx = gridx; gbc.gridy=gridy;
	gbc.gridwidth = gridwidth; gbc.gridheight = gridheight;
	gbc.fill = fill;
	gbc.anchor=anchor;
	cont.add(comp,gbc);
    }


    public void actionPerformed(ActionEvent e) {
	validate();

	if (e.getActionCommand().equals("OK")) {
	    try {
		xs=Integer.parseInt(xSize.getText());
		ys=Integer.parseInt(ySize.getText());
	    } catch (NumberFormatException nfe) {}
	    // just keep the old numbers then
		
	} else if (e.getActionCommand().equals("Cancel")) {
	} else {
	    System.err.println("Undefined Button Pressed!");
	    System.exit(3);
	}

	setVisible(false);
    }

    
    public int getXSize() {
	try {
	    xs=Integer.parseInt(xSize.getText());
	} catch (NumberFormatException nfe) {}
	return xs;
    }

    public int getYSize() {
	try {
	    ys=Integer.parseInt(ySize.getText());
	} catch (NumberFormatException nfe) {}
	return ys;
    }

    public void setPanelSize(int x, int y) {
	xs=x;
	ys=y;
	xSize.setText(Integer.toString(xs));
	ySize.setText(Integer.toString(ys));
    }
    
}









