/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2002 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;
import dsc.util.*;

public class PHWorldOptionsUI extends WorldOptionsUI implements ItemListener, ActionListener{
    private Choice planetsPerPlayerChoice, richnessChoice, densityChoice;
    private Choice rotationChoice, allocationChoice;
    private Choice tharganChoice;
    private Panel customPanel, randomPanel, tuningPanel;
    private Checkbox endTurnWithMissingPlayersBox;
    private Checkbox randomBox, customBox;
    private CheckboxGroup checkboxes;
    private Choice mapChoice;
    private Hashtable maps;
    private TextField firstRnd,rndTime,timeIncrement,maxRnd;

    private TextField staticHome, staticDode, staticOther, staticSpacePort;
    private TextField cumHome, cumDode, cumOther, cumSpacePort;
    private TextField gameEndTurn, limitConstant;
    private Checkbox turnLimitBox, scoreLimitBox;

    private CardLayout cardLayout;
    private Panel cardPanel;
    private Button turnButton, mapButton, scoreButton;

    private PHWorldOptions phWorldOptions;
  
    public PHWorldOptionsUI(PHWorldOptions phWorldOptions) {
	this.phWorldOptions = phWorldOptions;

	cardLayout = new CardLayout();
	GridBagConstraints gbc = new GridBagConstraints();

	setLayout(new GridBagLayout());
 
	gbc.weightx=100;
	gbc.weighty=0;
	gbc.gridwidth=1;
	gbc.gridheight=1;
	gbc.gridx=0;
	gbc.gridy=0;
	gbc.fill=GridBagConstraints.HORIZONTAL;
	gbc.anchor=GridBagConstraints.NORTH;


	turnButton = new Button("Turns");
	turnButton.addActionListener(this);
	add(turnButton, gbc);

	gbc.gridx=1;
	
	mapButton = new Button("Map");
	mapButton.addActionListener(this);
	add(mapButton, gbc);

	gbc.gridx=2;
	
	scoreButton = new Button("Scoring");
	scoreButton.addActionListener(this);
	add(scoreButton, gbc);


	gbc.weightx=100;
	gbc.weighty=100;
	gbc.gridx=0;
	gbc.gridy=1;
	gbc.gridwidth=3;
	gbc.gridheight=10;
	gbc.fill=GridBagConstraints.BOTH;
	gbc.anchor=GridBagConstraints.CENTER;

	cardPanel = new Panel();
	cardLayout = new CardLayout();
	cardPanel.setLayout(cardLayout);

	Panel roundPanel = createRoundPanel(phWorldOptions);
	Panel mapPanel   = createMapPanel(phWorldOptions);
	Panel scoringPanel = createScoringPanel(phWorldOptions);

	cardPanel.add(roundPanel, "Turns");
	cardPanel.add(mapPanel, "Map");
	cardPanel.add(scoringPanel, "Scoring");

	add(cardPanel, gbc);
    }

    private Panel createRoundPanel(PHWorldOptions phWorldOptions) {
	GridBagConstraints gbc = new GridBagConstraints();
	Panel p = new Panel();
	p.setLayout(new GridBagLayout());

	gbc.weightx=100;
	gbc.weighty=0;
	gbc.gridwidth=1;
	gbc.gridheight=1;
	gbc.gridx=0;
	gbc.gridy=0;
	gbc.fill=GridBagConstraints.BOTH;
	gbc.anchor=GridBagConstraints.CENTER;


	// LABELS
	Label rt = new Label("Roundtime");
	p.add(rt,gbc);

	gbc.gridy=1;
	p.add(new Label("First rnd"),gbc);

	gbc.gridy=2;
	p.add(new Label("Rnd time"),gbc);

	gbc.gridy=3;
	p.add(new Label("Increment per rnd"),gbc);

	gbc.gridy=4;
	p.add(new Label("Max rnd time"),gbc);

	gbc.gridx=2;
	gbc.gridy=1;
	p.add(new Label("s"),gbc);

	gbc.gridy=2;
	p.add(new Label("s"),gbc);

	gbc.gridy=3;
	p.add(new Label("s"),gbc);

	gbc.gridy=4;
	p.add(new Label("s"),gbc);

	gbc.gridy=5;
	gbc.gridx=1;
	endTurnWithMissingPlayersBox = new Checkbox("Wait offline players", true);
	p.add(endTurnWithMissingPlayersBox, gbc);

	// TEXTFIELDS
	gbc.gridx=1;
	gbc.gridy=1;
	firstRnd = new TextField(phWorldOptions.getFirstRoundTime() + "", 4);
	p.add(firstRnd,gbc);

	gbc.gridy=2;
	rndTime = new TextField(phWorldOptions.getBaseRoundTime() + "", 4);
	p.add(rndTime,gbc);

	gbc.gridy=3;
	timeIncrement = new TextField(phWorldOptions.getRoundTimeIncrement() +
				      "", 4);
	p.add(timeIncrement,gbc);

	gbc.gridy=4;
	maxRnd = new TextField(phWorldOptions.getMaxRoundTime() + "", 4);
	p.add(maxRnd,gbc);

	return p;
    }

    private Panel createMapPanel(PHWorldOptions phWorldOptions) {
	GridBagConstraints gbc = new GridBagConstraints();
	Panel p = new Panel();
	p.setLayout(new GridBagLayout());

	gbc.weightx=100;
	gbc.weighty=0;
	gbc.gridwidth=1;
	gbc.gridheight=1;
	gbc.gridx=0;
	gbc.gridy=0;
	gbc.fill=GridBagConstraints.BOTH;
	gbc.anchor=GridBagConstraints.CENTER;


	gbc.gridx=0;
	gbc.gridy=1;
	gbc.gridwidth=3;
	p.add(new Label("Composition of the Universe"),gbc);
	gbc.gridwidth=1;

	checkboxes = new CheckboxGroup();

	if(phWorldOptions.isCustomWorld()) {
	    randomBox=new Checkbox("Random", checkboxes, false);
	    customBox=new Checkbox("Custom", checkboxes, true);
	} else {
	    randomBox=new Checkbox("Random", checkboxes, true);
	    customBox=new Checkbox("Custom", checkboxes, false);
	}
	randomBox.addItemListener(this);
	customBox.addItemListener(this);

	gbc.gridy=2;
	p.add(customBox,gbc);

	gbc.gridy=4;
	p.add(randomBox,gbc);

	customPanel=new Panel();
	customPanel.setLayout(new BorderLayout());
    
	customPanel.add(new Label("Select map:"),"West");
	mapChoice=generateMapChoice();
	customPanel.add(mapChoice,"East");
    
	String map = phWorldOptions.getMapName();
	if(map != null)
	    mapChoice.select(map);
    
	gbc.gridx=0;
	gbc.gridy=6;
	gbc.gridwidth=3;
	p.add(customPanel,gbc);
	customPanel.setEnabled(map != null);

	planetsPerPlayerChoice = new Choice();
    
	planetsPerPlayerChoice.add("4");
	planetsPerPlayerChoice.add("6");
	planetsPerPlayerChoice.add("8");
	planetsPerPlayerChoice.add("10");
	planetsPerPlayerChoice.add("12");
	planetsPerPlayerChoice.add("15");
	planetsPerPlayerChoice.add("18");
	planetsPerPlayerChoice.add("22");
	planetsPerPlayerChoice.add("24");
	planetsPerPlayerChoice.add("28");
	planetsPerPlayerChoice.add("32");
	planetsPerPlayerChoice.add("40");
	planetsPerPlayerChoice.add("50");
	planetsPerPlayerChoice.add("65");
	planetsPerPlayerChoice.add("85");
	planetsPerPlayerChoice.add("100");
	planetsPerPlayerChoice.add("125");
	planetsPerPlayerChoice.add("150");
	planetsPerPlayerChoice.add("175");
	planetsPerPlayerChoice.select(phWorldOptions.getPlanetsPerPlayer() 
				      + "");

	richnessChoice = new Choice();

	richnessChoice.add("25");
	richnessChoice.add("50");
	richnessChoice.add("60");
	richnessChoice.add("70");
	richnessChoice.add("80");
	richnessChoice.add("90");
	richnessChoice.add("100");
	richnessChoice.add("110");
	richnessChoice.add("125");
	richnessChoice.add("150");
	richnessChoice.add("175");
	richnessChoice.add("200");
	richnessChoice.add("250");
	richnessChoice.select(phWorldOptions.getRichness() + "");

	densityChoice = new Choice();

	densityChoice.add("50");
	densityChoice.add("75");
	densityChoice.add("85");
	densityChoice.add("100");
	densityChoice.add("125");
	densityChoice.add("150");
	densityChoice.add("200");
	densityChoice.select(phWorldOptions.getPlanetDensity() + "");


	rotationChoice = new Choice();

	rotationChoice.add("0");
	rotationChoice.add("1");
	rotationChoice.add("3");
	rotationChoice.add("5");
	rotationChoice.add("8");
	rotationChoice.add("10");
	rotationChoice.add("15");
	rotationChoice.add("20");
	rotationChoice.add("25");
	rotationChoice.add("30");
	rotationChoice.add("40");
	rotationChoice.select(phWorldOptions.getOrbitalVelocity() + "");


	allocationChoice = new Choice();

	allocationChoice.add("0");
	allocationChoice.add("10");
	allocationChoice.add("20");
	allocationChoice.add("30");
	allocationChoice.add("40");
	allocationChoice.add("50");
	allocationChoice.add("60");
	allocationChoice.add("70");
	allocationChoice.add("80");
	allocationChoice.add("90");
	allocationChoice.add("100");
	allocationChoice.select(phWorldOptions.getPlanetAllocationPercent() 
				+ "");

	randomPanel=new Panel();
	randomPanel.setLayout(new GridLayout(5,2));

	randomPanel.add(new Label("Planets per player:"));
	randomPanel.add(planetsPerPlayerChoice);

	randomPanel.add(new Label("Material richness:"));
	randomPanel.add(richnessChoice);

	randomPanel.add(new Label("Planet density:"));
	randomPanel.add(densityChoice);

	randomPanel.add(new Label("Map velocity:"));
	randomPanel.add(rotationChoice);

	randomPanel.add(new Label("Preallocation percent"));
	randomPanel.add(allocationChoice);

	gbc.gridx=0;
	gbc.gridy=7;
	gbc.gridheight=4;
	gbc.gridwidth=3;
	p.add(randomPanel,gbc);
	randomPanel.setEnabled(true);


	tharganChoice = new Choice();

	tharganChoice.add("0");
	tharganChoice.add("25");
	tharganChoice.add("50");
	tharganChoice.add("75");
	tharganChoice.add("100");
	tharganChoice.add("150");
	tharganChoice.add("200");
	tharganChoice.add("400");
	tharganChoice.add("750");
	tharganChoice.add("1000");
	tharganChoice.add("2000");
	tharganChoice.add("3000");
	tharganChoice.add("4000");
	tharganChoice.add("5000");
	tharganChoice.select(phWorldOptions.getTharganActivity() 
				+ "");

	gbc.gridx=0;
	gbc.gridy=11;
	gbc.gridheight=1;
	gbc.gridwidth=2;
	p.add(new Label("Thargon activity %"), gbc);

	gbc.gridx=2;
	gbc.gridy=11;
	gbc.gridheight=1;
	gbc.gridwidth=1;
	p.add(tharganChoice,gbc);


	return p;
    }

    private Panel createScoringPanel(PHWorldOptions phWorldOptions) {
	GridBagConstraints gbc = new GridBagConstraints();
	Panel p = new Panel();
	p.setLayout(new GridBagLayout());

	gbc.weightx=100;
	gbc.weighty=0;
	gbc.gridwidth=1;
	gbc.gridheight=1;
	gbc.gridx=0;
	gbc.gridy=0;
	gbc.fill=GridBagConstraints.BOTH;
	gbc.anchor=GridBagConstraints.CENTER;

	gbc.gridx=0;
	gbc.gridy=0;
	gbc.gridwidth=3;
	gbc.gridheight=1;
	gbc.weighty=100;
	p.add(new Panel(), gbc);

	// LABELS
	gbc.weighty=0;
	gbc.gridwidth=1;
	gbc.gridy=1;
	gbc.gridx=0;

	Label rt = new Label("Scores");
	p.add(rt,gbc);

	gbc.gridwidth=1;
	gbc.gridy=2;
	gbc.gridx=1;
	p.add(new Label("Static"), gbc);

	gbc.gridx=2;
	p.add(new Label("Cumulative"), gbc);

	gbc.gridy=3;
	gbc.gridx=0;
	p.add(new Label("Dodechaedron"),gbc);

	gbc.gridy=4;
	p.add(new Label("Home Worlds"),gbc);

	gbc.gridy=5;
	p.add(new Label("Other Planets"),gbc);

	gbc.gridy=6;
	p.add(new Label("Space Ports"),gbc);


	gbc.gridx=1;
	gbc.gridy=3;
	staticDode = new TextField(phWorldOptions.getStaticDodeScore()+"", 6);
	p.add(staticDode,gbc);

	gbc.gridx=2;
	gbc.gridy=3;
	cumDode = new TextField(phWorldOptions.getCumDodeScore()+"", 6);
	p.add(cumDode,gbc);

	gbc.gridx=1;
	gbc.gridy=4;
	staticHome = new TextField(phWorldOptions.getStaticHomeScore()+"", 6);
	p.add(staticHome,gbc);

	gbc.gridx=2;
	gbc.gridy=4;
	cumHome = new TextField(phWorldOptions.getCumHomeScore()+"", 6);
	p.add(cumHome,gbc);

	gbc.gridx=1;
	gbc.gridy=5;
	staticOther = new TextField(phWorldOptions.getStaticOtherScore()+"", 6);
	p.add(staticOther,gbc);

	gbc.gridx=2;
	gbc.gridy=5;
	cumOther = new TextField(phWorldOptions.getCumOtherScore()+"", 6);
	p.add(cumOther,gbc);

	gbc.gridx=1;
	gbc.gridy=6;
	staticSpacePort = new TextField(phWorldOptions.getStaticSpacePortScore()+"", 6);
	p.add(staticSpacePort,gbc);

	gbc.gridx=2;
	gbc.gridy=6;
	cumSpacePort = new TextField(phWorldOptions.getCumSpacePortScore()+"", 6);
	p.add(cumSpacePort,gbc);

	gbc.gridx=0;
	gbc.gridy=7;
	gbc.gridwidth=3;
	gbc.gridheight=1;
	gbc.weighty=100;
	p.add(new Panel(), gbc);

	gbc.weighty=0;
	gbc.gridy=8;
	gbc.gridx=0;
	gbc.gridwidth=1;
	p.add(new Label("Game End Condition"), gbc);

	gbc.gridx=2;
	gbc.gridy=9;
	gameEndTurn = new TextField(phWorldOptions.getTurnLimit()+"", 4);
	p.add(gameEndTurn,gbc);

	gbc.gridx=2;
	gbc.gridy=10;
	limitConstant = new TextField(phWorldOptions.getScoreLimit()+"", 3);
	limitConstant.setEnabled(false);
	p.add(limitConstant,gbc);

	gbc.gridy=9;
	gbc.gridx=0;
	turnLimitBox = new Checkbox("Turn Limit", true);
	p.add(turnLimitBox, gbc);
	turnLimitBox.addItemListener(this);

	gbc.gridy=10;
	gbc.gridx=0;
	scoreLimitBox = new Checkbox("Score Limit", false);
	p.add(scoreLimitBox, gbc);
	scoreLimitBox.addItemListener(this);

	gbc.gridx=0;
	gbc.gridy=12;
	gbc.gridwidth=3;
	gbc.gridheight=1;
	gbc.weighty=100;
	p.add(new Panel(), gbc);

	return p;
    }


    private Choice generateMapChoice() {
	maps = MapLoader.getLoadableMaps();
	Choice c=new Choice();
    
	Enumeration e=maps.keys();
	while (e.hasMoreElements()) {
	    String s= (String) e.nextElement();
	    c.add(s);
	}

	return c;
    }  

    public void itemStateChanged(ItemEvent evt) {
	if(evt.getItem().equals("Turn Limit"))
	    gameEndTurn.setEnabled(turnLimitBox.getState());

	if(evt.getItem().equals("Score Limit")) {
	    limitConstant.setEnabled(scoreLimitBox.getState());
	}

	if(evt.getStateChange()!=ItemEvent.DESELECTED) {
 	    if(evt.getItem().equals("Random")){
		randomPanel.setEnabled(true);
		customPanel.setEnabled(false);
	    }
	    else if (evt.getItem().equals("Custom")) {
		randomPanel.setEnabled(false);
		customPanel.setEnabled(true);
	    }
	}
    
	validate();
    }

    private boolean isRandomWorld() {
	return randomBox.getState();
    }


    private boolean isCustomWorld() {
	return (!isRandomWorld());
    }

    private String getMapName() {
	return mapChoice.getSelectedItem();
    }

    private String getMapFilename() {
	return (String) maps.get(getMapName());
    }

    private int getPlanetsPerPlayer() {
	String nroPlanets=planetsPerPlayerChoice.getSelectedItem();
	int n=12;

	try {
	    n=Integer.parseInt(nroPlanets);
	} catch (NumberFormatException e) {
	    System.err.println("String: \"" + nroPlanets + "\" caused exception: " +
			       e);
	    System.err.println("Defaulting to 12 planets per player");
	}
    
	return n;
    }

    private int getRichness() {
	String r=richnessChoice.getSelectedItem();
	int n=100;

	try {
	    n=Integer.parseInt(r);
	} catch (NumberFormatException e) {
	    System.err.println("String: \"" + r + "\" caused exception: " +
			       e);
	    System.err.println("Defaulting to richness of 100%");
	}
    
	return n;
    }

    private int getOrbitalVelocity() {
	if(rotationChoice == null || isCustomWorld()) {
	    return 0;
	} else {
	    String r=rotationChoice.getSelectedItem();
	    int n=0;

	    try {
		n=Integer.parseInt(r);
	    } catch (NumberFormatException e) {
		System.err.println("Internal error: String: \"" + 
				   r + "\" caused exception: " +
				   e);
		System.err.println("Defaulting to 0 orbital velocity");
	    }
    
	    return n;
	}
    }

    private int getPlanetAllocationPercent() {
	if(allocationChoice == null || isCustomWorld()) {
	    return 0;
	} else {
	    String a=allocationChoice.getSelectedItem();
	    int n=0;

	    try {
		n=Integer.parseInt(a);
	    } catch (NumberFormatException e) {
		System.err.println("Intenal error: String: \"" + 
				   a + "\" caused exception: " +
				   e);
		System.err.println("Defaulting to 0% planet preallocation");
	    }
    
	    return n;
	}
    }

    private int getPlanetDensity() {
	String r=densityChoice.getSelectedItem();
	int n=100;

	try {
	    n=Integer.parseInt(r);
	} catch (NumberFormatException e) {
	    System.err.println("String: \"" + r + "\" caused exception: " +
			       e);
	    System.err.println("Defaulting to density of 100%");
	}
    
	return n;
    }

    private int getTharganActivity() {
	String r=tharganChoice.getSelectedItem();
	int n=100;

	try {
	    n=Integer.parseInt(r);
	} catch (NumberFormatException e) {
	    System.err.println("String: \"" + r + "\" caused exception: " +
			       e);
	    System.err.println("Defaulting to activity of 100%");
	}
    
	return n;
    }

    private int getFirstRoundTime() {
	int time = 300;

	try {
	    time = (int) Float.valueOf(firstRnd.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return time;
    }

    private int getBaseRoundTime() {
	int time = 60;

	try {
	    time = (int) Float.valueOf(rndTime.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return time;
    }

    private double getRoundTimeIncrement() {
	double time = 1.0;

	try {
	    time = Float.valueOf(timeIncrement.getText()).doubleValue();
	} catch (NumberFormatException e) {}
    
	return time;
    }

    private int getMaxRoundTime() {
	int time = 150;

	try {
	    time = (int) Float.valueOf(maxRnd.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return time;
    }

    private boolean endTurnWhenAway() {
        return !endTurnWithMissingPlayersBox.getState();
    }

    private int getStaticDodeScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(staticDode.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getStaticHomeScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(staticHome.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getStaticOtherScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(staticOther.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getStaticSpacePortScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(staticSpacePort.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getCumDodeScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(cumDode.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getCumHomeScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(cumHome.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getCumOtherScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(cumOther.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getCumSpacePortScore() {
	int score = 0;

	try {
	    score = (int) Float.valueOf(cumSpacePort.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return score;
    }

    private int getTurnLimit() {
	int limit = 0;

	if(!turnLimitBox.getState()) {
	    return 0;
	}

	try {
	    limit = (int) Float.valueOf(gameEndTurn.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return limit;
    }

    private int getScoreLimit() {
	int limit = 0;

	if(!scoreLimitBox.getState()) {
	    return 0;
	}

	try {
	    limit = (int) Float.valueOf(limitConstant.getText()).floatValue();
	} catch (NumberFormatException e) {}
    
	return limit;
    }

    public WorldOptions getOptions() {
	return (WorldOptions) phWorldOptions;
    }

    public void updateOptions() {
	if(isRandomWorld()) {
	    phWorldOptions.setMapFilename(null, null);
	} else {
	    phWorldOptions.setMapFilename(getMapName(), getMapFilename());
	}

	phWorldOptions.setPlanetsPerPlayer(getPlanetsPerPlayer());
	phWorldOptions.setRichness(getRichness());
	phWorldOptions.setOrbitalVelocity(getOrbitalVelocity());
	phWorldOptions.setPlanetAllocationPercent(getPlanetAllocationPercent());

	phWorldOptions.setPlanetDensity(getPlanetDensity());
	phWorldOptions.setTharganActivity(getTharganActivity());

	phWorldOptions.setFirstRoundTime(getFirstRoundTime());
	phWorldOptions.setBaseRoundTime(getBaseRoundTime());
	phWorldOptions.setMaxRoundTime(getMaxRoundTime());
	phWorldOptions.setRoundTimeIncrement(getRoundTimeIncrement());
	phWorldOptions.setEndTurnWhenAway(endTurnWhenAway());

	phWorldOptions.setStaticDodeScore(getStaticDodeScore());
	phWorldOptions.setStaticHomeScore(getStaticHomeScore());
	phWorldOptions.setStaticOtherScore(getStaticOtherScore());
	phWorldOptions.setStaticSpacePortScore(getStaticSpacePortScore());
	phWorldOptions.setCumDodeScore(getCumDodeScore());
	phWorldOptions.setCumHomeScore(getCumHomeScore());
	phWorldOptions.setCumOtherScore(getCumOtherScore());
	phWorldOptions.setCumSpacePortScore(getCumSpacePortScore());

	phWorldOptions.setTurnLimit(getTurnLimit());
	phWorldOptions.setScoreLimit(getScoreLimit());
    }

    public void actionPerformed(ActionEvent evt) {
	String arg = evt.getActionCommand();

	if(arg == null) {
	    MenuItem mi = (MenuItem) evt.getSource();
	    arg=mi.getLabel();
	}

	if(arg.equals("Turns")) 
	    cardLayout.show(cardPanel, "Turns");
	else if(arg.equals("Map"))
	    cardLayout.show(cardPanel, "Map");
	else if(arg.equals("Scoring"))
	    cardLayout.show(cardPanel, "Scoring");
    }
}
