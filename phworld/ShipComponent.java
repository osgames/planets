/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.io.*;
import dsc.util.Sortable;


/**
 * ShipComponent is a superclass for ship components. It handles 
 * damage control and has abstract inteface to build costs and power
 * consumption of the components 
 *
 * @author Dodekaedron Software Creations, Inc. -- Lexa
 */  

public abstract class ShipComponent extends TransferableComponent implements dsc.util.Sortable{

  public static final int HULL   = 1;
  public static final int ENGINE = 2;
  public static final int WEAPON = 3;
  public static final int SHIELD = 4;
  public static final int MISC   = 5;

  public static final int [] smallTechRequirement = 
  {0, 160, 110, 210, 400, 620, 760, 900,1500};
  
  public static final int [] mediumTechRequirement = 
  {0, 200, 200, 480, 900,1100,1480,2000,2600};

  public static final int [] highTechRequirement = 
  {0, 240, 280, 600,1200,1700,2000,3000,4500};  

  private boolean isActive;
  protected int techLevel;

  ShipComponent(int techlevel) {
    super();
    techLevel=techlevel;
    damageReceived=0;
    isActive=true;
  }

  public int getTech() {
    return techLevel;
  }

  public int greaterThan(Sortable s) {
    ShipComponent sc = (ShipComponent) s;
    if(sc.getTech() < getTech())
      return 1;
    if(sc.getTech() > getTech())
      return -1;
    return 0;
  }

  /** Returns true if the Component is active.
   * Inactive components do not operate, nor consume power.
   *
   * @return True if component is active, otherwise false
   */ 
  public boolean isActive() { 
    return isActive; 
  }

  /** Sets the activity flag of the component.
   *
   * @see #isActive
   */
  public void setActive(boolean active) {
    isActive=active;
  }

  /** Sets the component to be active.
   *
   * @see #isActive
   */
  public void activate() {
    isActive=true;
  }

  /** Sets the component to be inactive.
   *
   * @see #isActive
   */
  public void deactivate() {
    isActive=false;
  }

  /** Returns materials required to build component. 
   * Each subclass has a static field to define all the materials required
   * to build a component of certain tech level - getBuildMaterials() should
   * return MaterialBundle that has all the materials required to build the
   * ShipComponent.
   *
   * @param tech Tech level
   * @return Materials required to build this component
   */
  public static MaterialBundle getBuildMaterials(int tech) {
    return (new MaterialBundle(MaterialBundle.UNLIMITED));
  }

  /** Returns the time taken to build the ship component.
   * Each component has a distinct builing time.
   *
   * @param tech Tech level
   * @return Time taken in building a component of the specified level
   */
  public static int getBuildTime(int tech) {
    return 0;
  }


//TECHLEVELS
  /** Returns the time taken for upgrading tech level of the planet.
   * Default time taken for developing one technology level of level tech
   * Each ShipComponent subclass can override this to provide different time 
   * requirements for different techtypes: hulls, engines, weapons, shields 
   *
   * @param tech Tech level
   * @return Time taken in upgrading one tech level
   */

  static public int getTechTime(int tech) {
    return tech;
  }

  /** Returns the materials  required in upgrading the tech level of a planet.
   * Each ShipComponent subclass can override this to provide different material 
   * requirements for different techtypes: hulls, engines, weapons, shields 
   *
   * @param tech Tech level
   * @return Materials required in upgrading one tech level
   */
  public static MaterialBundle getTechMaterials(int tech) {
    return new MaterialBundle(MaterialBundle.UNLIMITED);
  }


//POWERCONSUMPTION

  /** Returns the power consumption of the component.
   * All ShipComponents consume power, just some components consume power
   * an amount which is numerically equal to zero (by default)
   * If non-zero power consumption is desirable (engines...) method should
   * be overdriven
   *
   * @return Power consumption of the ShipComponent
   */ 
  public int getPowerConsumption() { return 0; }


  //POWEROUTPUT

  /** Returns the power output the component produces. The form of this
   * power depends on the the nature of the component. An Engine produces
   * engine power, a shield produces shield power, etc., the exeption being
   * that hulls do not produces any sort of power and hence do not implement
   * this method but return the default 0
   *
   * @return Power output of the component
   */
  public int getPowerOutput() {return 0;}

//DAMAGE

  private int damageReceived;

  /** Returns the maximum damgage that the ShipComponent can receive 
   * being totally destroyed.
   * 
   * @return Maximum obtainable damage
   */
  public abstract int getMaxDamage();


  /** Returns the damage received by the ShipComponent.
   *
   * @return Returns the damage received
   */
  public int getDamageReceived() {
    return damageReceived;
  }
  
  /** This method is used to set the damage of the ShipComponent.
   * this method is primarily used  by other functions
   * such as addDamage, repairDamage...
   *
   * @parm damage Damage received by the ShipComponent
   */
  void setDamageReceived(int damage) {
    damageReceived = damage;
  }

  /** If isDestroyed() is true then the ShipComponent is destroyed.
   * 
   * @return true if ShipComponent destroyed, otherwise false
   */
  public boolean isDestroyed() {
    if(getDamageReceived()>=getMaxDamage()) return true;
    else return false;
  }

  /** Returns the damage received as a percentage.
   *
   * @return Damage received as a percentage (0-100)
   */
  public int getDamagePercentage() {
    return (getDamageReceived()*100)/getMaxDamage();
  }
 
  /** Add damage to the component. Component discards damage that
   * exceeds it's total damage resistance and takes only 100% damage.
   *
   * @param damage Amount of damage to take
   * @throws IllegalArgumentException If commanded to take negative damage.
   * @see #repairDamage 
   */

  void addDamage(int damage) {
    if( damage<0 ) throw new 
     IllegalArgumentException("Damage to be inflicted  cannot be negative");
    if ( damage+getDamageReceived() > getMaxDamage() ) 
      setDamageReceived(getMaxDamage());
    else setDamageReceived(getDamageReceived()+damage);
  } 

  /** Repair damage inflicted to the ShipComponent. 
   * If more damage is repaired than the ShipComponent has been inflicted,
   * damage received will be set to zero.
   *
   * @param damage Amount of damage to be repaired
   * @throws IllegalArgumentException If negative damage is passed as a paraemeter.
   * @see #addDamage 
   */

  void repairDamage(int damage) {
    if( damage<0 ) throw new 
     IllegalArgumentException("Damage to be repaired cannot be negative");
    if(getDamagePercentage()==100)
      return;
    if (getDamageReceived()-damage < 0) 
      setDamageReceived(0);
    else setDamageReceived(getDamageReceived()-damage);
  }

  public Object clone() {
    return super.clone();
  }

  public String getFullName() {
    return toString() + " (" + (100-getDamagePercentage()) + "%)"; 
  }

  public abstract int getTechType();
}
