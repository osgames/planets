/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;
import dsc.util.*;


public class HostTransferer extends HostWorldComponent {

    private Location waypoint;
    private MaterialBundle cargo;
    private int targetId;

    public void putMessage(WorldComponentMessage m) {
	return;
    }

    private void unload() {
	HostWorldComponent hwc = hostWorld.getComponentById(targetId);
	if(hwc instanceof HostPlanet) {
	    HostPlanet hp = (HostPlanet) hwc;
	    hp.getMaterials().addFullBundle(cargo);
	}

	hostWorld.removeComponent(getId());
    }

    public void doRound() {
	doMovement();
    }

    void doMovement() {
	if(waypoint.equals(location)) {
	    unload();
	} else {
	    int speed       = getSpeed();
	    int distance    = location.distance(waypoint);

	    if(distance<speed) {
		speed=distance;
	    }	

	    location=location.moveTowards(waypoint, speed); 
	}
    }

    public int getSpeed() {
	return 30;
    }

    public WorldComponent getWorldComponent(int player) {
	return null;
    }

    public HostTransferer(HostWorld hw, 
			  Location location, 
			  Location target,
			  int targetId,
			  MaterialBundle m) {
	super(hw,-1,location);
    
	waypoint = target;
	this.targetId = targetId;
	scanRange=0;
	name="Transfer entity"; 
	cargo = m;
    }

    public boolean isVisible() {
	return false;
    }

    public static int getCost(int distance) {
	return (distance*100)/300;
    }

    public static int getEfectivityPercentage(int distance) {
	return 10000/(100+getCost(distance));
    }
}
