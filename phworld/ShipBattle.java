/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;


public class ShipBattle {

  private static final int MAX_BATTLE_LENGTH = 15;
  private static final double DIRECT_CRITICAL_PROB = 0.5;
  private static final double MAX_EXP_RATIO = 2.0;
  private static final double MIN_EXP_RATIO = 0.1;
  private static final double CRITICAL_BASE_PROB = 0.05;

  private HostWorld hostWorld;

  public ShipBattle(HostWorld hw) {
    hostWorld=hw;
  }
  
  /**
   *
   * @return Hashtable containing BattleOutcomes for all players that
   * were involved in battles.
   *
   */

  public Hashtable shipBattle() {
    Hashtable battleOutcomes = new Hashtable();

    Vector allShips = new Vector();
    Vector battles = new Vector();
    Enumeration e = hostWorld.getComponents();

    while (e.hasMoreElements()) {
      HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
      if(hwc instanceof HostShip)
	allShips.addElement(hwc);
    }

    dsc.util.Quicksort.quicksort(allShips);

    Vector currentBattle = null;

    /* Now the allShips vector should be sorted, so ships in the same
       location are next to eachother. We scan through the vector
       and try to find ships in the same location. If we find two or
       more ships in the same location we form a new battle for them
       and place them in it. */

    for(int i=1; i<allShips.size(); i++) {
      if(((HostShip) allShips.elementAt(i-1)).
	 getLocation().equals(((HostShip)allShips.elementAt(i)).
			      getLocation())) {
	if(currentBattle == null) {
	  currentBattle = new Vector();
	  currentBattle.addElement(allShips.elementAt(i-1));
	} 
	currentBattle.addElement(allShips.elementAt(i));
      } else if(currentBattle != null) {
	  battles.addElement(currentBattle);
	  currentBattle=null;
      }
    }

    if(currentBattle != null)
      battles.addElement(currentBattle);

    int battleNumber=0;
    Enumeration b = battles.elements();
    while (b.hasMoreElements()) {
      battleNumber++;
      Vector v = (Vector) b.nextElement();

      HostShip oneShip = (HostShip) v.elementAt(0);
      HostPlanet nearestPlanet = ((PHHostWorld) hostWorld).
	findNearestPlanet(oneShip.getLocation());
      String nearestPlanetName = "NONE";
      if(nearestPlanet != null)
	nearestPlanetName=nearestPlanet.getName();

      String battleName = "NONAMEYEAT";
      if(nearestPlanet.getLocation().equals(oneShip.getLocation())) {
	battleName = battleNumber + ". over " + nearestPlanetName;
      } else {
	battleName = battleNumber + ". near " + nearestPlanetName;
      }

      Vector participants = calculateParticipants(v);
      BattleReport report=handleBattle(battleName, participants, v, hostWorld.getAlliancesClone());

      if(report!=null)
	for (int i = 0; i < participants.size() ; i++) {
	  addBattleReport(((Integer) participants.elementAt(i)).intValue(), 
			  report, 
			  battleOutcomes);
	}
    }

    return battleOutcomes;
  }

  private void addBattleReport(int player, 
			       BattleReport report, 
			       Hashtable battleOutcomes) {
    
    BattleOutcomes myBattleoutcomes = 
      (BattleOutcomes) battleOutcomes.get(new Integer(player));
    
    if(myBattleoutcomes == null) {
      myBattleoutcomes = new BattleOutcomes();
      battleOutcomes.put(new Integer(player), myBattleoutcomes);
    }
    
    myBattleoutcomes.addReport(report);
  }

  private Vector getEnemyShips(Vector ships, Alliance alliance) {
    Vector v = new Vector();

    for(int i=0; i<ships.size(); i++) {
      HostShip hs = (HostShip) ships.elementAt(i);
      
      if(!alliance.isAlly(hs.getOwner()))
	v.addElement(hs);
    }
    
    return v;
  }

  private Vector handleDestroyedShips(Vector ships) {
    Vector v = new Vector();

    int expShips = ships.size() - 1;
    int totalExp = 0;
    if(expShips < 1)
	expShips = 1;

    for(int i=0; i<ships.size(); i++) {
      HostShip hs = (HostShip) ships.elementAt(i);
      
      if(hs.getDamagePercentage() < 100)
	v.addElement(hs);
      else
	totalExp += hs.getExpValue();

      /* System.out.println(hs.getName() + " exp is now " + hs.getExperience()); */

    }
    
    if(totalExp > 0) {
	Enumeration e = v.elements();

	while(e.hasMoreElements()) {
	    HostShip hs = (HostShip) e.nextElement();
	    hs.addExperience(totalExp/expShips);
	}
    }

    return v;
  }


  private Vector getExistingShips(Vector a, Vector b) {
    Vector remainingShips = new Vector();

    for (int i = 0; i < a.size(); i++) {
      for (int j = 0; j < b.size(); j++) {
	if(a.elementAt(i) == b.elementAt(j))
	  remainingShips.addElement(a.elementAt(i));
      }
    }
    
    return remainingShips;
  }

    private boolean isCriticalHit(HostShip shooter, HostShip target) {
	double expRatio = ((double) shooter.getExperience()) /
	    ((double) target.getExperience());

	if(expRatio < MIN_EXP_RATIO) {
	    expRatio = MIN_EXP_RATIO;
	}
	if(expRatio > MAX_EXP_RATIO) {
	    expRatio = MAX_EXP_RATIO;
	}

	double crit_prob = CRITICAL_BASE_PROB * expRatio;
	
	return(Math.random() <= crit_prob);
    }

  private BattleReport handleBattle(String name, 
				    Vector participants,
				    Vector ships, 
				    Alliance [] alliances) {

    if(!isThisBattle(ships))
      return null;

    BattleReport battleReport = 
      new BattleReport(name);

    addBattleParticipants(battleReport, participants);

    for(int i=0; i<ships.size(); i++) {
      HostShip hs = (HostShip) ships.elementAt(i);
      
      hs.initBattle();
    }

    Vector allShips = (Vector) ships.clone();

    boolean someoneShooted=true;

    Vector newShooters      = new Vector();
    Vector oldShooters      = null;
    int battleLasted = 0;

    while(someoneShooted && battleLasted < MAX_BATTLE_LENGTH) {
      battleLasted++;
      someoneShooted=false;

      ships = handleDestroyedShips(ships);

      oldShooters   = newShooters;
      newShooters   = new Vector();

      for(int i=0; i<ships.size(); i++) {
	HostShip hs = (HostShip) ships.elementAt(i);
	hs.doPreBattleRoundActions();
      }

      for(int i=0; i<ships.size(); i++) {
	HostShip hs = (HostShip) ships.elementAt(i);

	Vector possibleTargets = 
	  getEnemyShips(ships, alliances[hs.getOwner()]);
	HostShip target = 
	  hs.selectShootingTarget(possibleTargets,
				  getExistingShips(oldShooters,possibleTargets));
	
	if(target != null) {
	    int hullDamage   = hs.getWeaponHullDamage();
	    int shieldDamage = hs.getWeaponShieldDamage();
	    boolean critical = isCriticalHit(hs, target);
	    int componentDamage = 0; /* direct component damage */

	    if(hullDamage > 0) {
		if(critical) {
		    if(Math.random() < DIRECT_CRITICAL_PROB) {
			/* direct critical, disable additional attacks */
			componentDamage = hullDamage/3;
			/*			System.out.println("Ship " + hs.getName() +
					   " directcriticalled " +
					   target.getName() +
					   " for " + componentDamage + 
					   " points."); */
		    } else {
			hullDamage += hs.getWeaponHullDamage();
			shieldDamage += hs.getWeaponShieldDamage();
			/*			System.out.println("Ship " + hs.getName() +
					   " criticalled " +
					   target.getName() + "."); */
		    }
		}

		newShooters.addElement(hs);
		someoneShooted=true;
		target.takeDamage(hullDamage, shieldDamage, componentDamage,
				  hs);
		alliances[target.getOwner()].setAlly(hs.getOwner(), false);
		battleReport.addAttack(hs.getOwner(), target.getOwner());
	    }
	}
      }
    }

    for(int i=0; i<allShips.size(); i++) {
      HostShip hs = (HostShip) allShips.elementAt(i);
      
      hostWorld.sendChatMessage(hs.getOwner(), hs.getBattleSummary());
      battleReport.addShip(hs.getBattleReportMessage());
    }

    return battleReport;
  }

  private boolean isThisBattle(Vector ships) {
    if(ships.size()<2)
      throw new IllegalStateException("Battle should contain at least two ships.");

    /*
      int first = ((HostShip) ships.elementAt(0)).getOwner();
    
      for(int i=1; i<ships.size(); i++) {
      if(((HostShip) ships.elementAt(i)).getOwner() != first) {
      return true;
      }
    }    
    */

    for(int i=0; i < ships.size(); i++)
      for(int j=0; j < ships.size(); j++) {
	if(!((HostShip) ships.elementAt(i)).canAttack())
	  continue;

	if(!hostWorld.isAlly(((HostShip) ships.elementAt(i)).getOwner(),
			     ((HostShip) ships.elementAt(j)).getOwner()))
	  return true;
      }

    return false;
  }

  private Vector calculateParticipants(Vector ships) {
    Vector participants = new Vector();

    for (int i = 0; i <ships.size() ; i++) {
      int owner = ((HostShip) ships.elementAt(i)).getOwner();
      
      int j=0;
      for (j = 0; j < participants.size() ; j++) {
	if(((Integer)participants.elementAt(j)).intValue() == owner)
	  break;
      }

      if(j == participants.size())
	participants.addElement(new Integer(owner));
    }

    return participants;
  }

  private void addBattleParticipants(BattleReport battleReport,
				     Vector participants) {

    for (int i = 0; i < participants.size() ; i++) {
      Integer ii = (Integer) participants.elementAt(i);
      battleReport.addParticipant(ii.intValue());
    }
  }
}
