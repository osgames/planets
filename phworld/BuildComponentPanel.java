/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;


class BuildComponentPanel extends PlanetSidePanel implements ItemListener {
  private GridBagConstraints gbc;
  private Checkbox hullBox, engineBox, weaponBox, shieldBox, miscBox;
  private Label hullLabel, engineLabel, weaponLabel, shieldLabel, miscLabel;
  private Label ETALabel;
  private TextArea productionMaterialsArea;
  private Choice techLevel;
  private CheckboxGroup checkboxGroup;

  BuildComponentPanel() {
    super("data/graph/still/panel/planet.component.gif");

    checkboxGroup=new CheckboxGroup();
    contentsPanel.setLayout(new GridBagLayout());
    gbc=new GridBagConstraints();

    gbc.fill=GridBagConstraints.BOTH;
    gbc.anchor=GridBagConstraints.NORTH;
    gbc.weightx=100;
    gbc.weighty=0;

    hullBox=addCheckbox(gbc,2,7,6,1,"Hull",checkboxGroup,false);
    engineBox=addCheckbox(gbc,2,8,6,1,"Engine",checkboxGroup,false);
    weaponBox=addCheckbox(gbc,2,9,6,1,"Weapon",checkboxGroup,false);
    shieldBox=addCheckbox(gbc,2,10,6,1,"Shield",checkboxGroup,false);
    miscBox=addCheckbox(gbc,2,11,6,1,"Misc",checkboxGroup,false);

    hullLabel=new Label("0");
    engineLabel=new Label("0");
    weaponLabel=new Label("0");
    shieldLabel=new Label("0");
    miscLabel=new Label("0");
    productionMaterialsArea=new TextArea("",13,22,TextArea.SCROLLBARS_NONE);
    productionMaterialsArea.setEditable(false);
    ETALabel=new Label("[Turns]");

    techLevel=new Choice();
    techLevel.addItemListener(this);

    gbc.gridwidth=8;
    gbc.gridheight=1;
    gbc.gridx=0;
    
    gbc.gridy=7;
    contentsPanel.add(hullLabel, gbc);
    gbc.gridy=8;
    contentsPanel.add(engineLabel, gbc);
    gbc.gridy=9;
    contentsPanel.add(weaponLabel, gbc);
    gbc.gridy=10;
    contentsPanel.add(shieldLabel, gbc);
    gbc.gridy=11;
    contentsPanel.add(miscLabel, gbc);

    gbc.gridy=13;
    contentsPanel.add(techLevel, gbc);

    gbc.gridy=15;
    gbc.gridwidth=8;
    gbc.gridx=0;
    gbc.gridheight=5;
    contentsPanel.add(productionMaterialsArea, gbc);

    gbc.gridy=20;
    gbc.gridwidth=4;
    gbc.gridheight=1;
    gbc.gridx=2;
    contentsPanel.add(ETALabel, gbc);

    gbc.gridy=16;
    gbc.gridwidth=8;
    gbc.gridx=0;
    gbc.weighty=100;
    contentsPanel.add(new Label(""), gbc);

  }

  void initializeContents(Planet p) {
    TechLevel tl = p.getTechLevels();
    
    hullLabel.setText("  " + tl.getHullTech());
    engineLabel.setText("  " + tl.getEngineTech());
    weaponLabel.setText("  " + tl.getWeaponTech());
    shieldLabel.setText("  " + tl.getShieldTech());
    miscLabel.setText("  "   + tl.getMiscTech());

    Checkbox c = checkboxGroup.getSelectedCheckbox();

    if(c == null) {
      techLevel.removeAll();
      techLevel.addItem("None");
      productionMaterialsArea.setText("\n\n");
      ETALabel.setText("[x turns]");     
    } else {
      reconstructList(TechLevel.getTechNumber(c.getLabel()));
      updateLabelTexts(p);
    }
  }

    
    private void updateLabelTexts(Planet p) {
	Checkbox c = checkboxGroup.getSelectedCheckbox();

	ETALabel.setText("[" + 
	  TechLevel.getBuildTime(TechLevel.getTechNumber(c.getLabel()) ,
						getSelectedLevel()) +
			 " turns]");
	
	String s=TechLevel.
	    getShortDescription(TechLevel.getTechNumber(c.getLabel()),
							getSelectedLevel());

	s=s+"\nRequirements / On planet:\n";

	MaterialBundle buildMats = 
	    TechLevel.getBuildMaterials(TechLevel.getTechNumber(c.getLabel()) ,
					getSelectedLevel());

	s=s+buildMats.mergeToString(p.getMaterials());
	productionMaterialsArea.setText(s);
    }

  private Checkbox addCheckbox(GridBagConstraints gbc,int x,int y, 
			       int w, int h, String name,
			       CheckboxGroup g,boolean v) {
    Checkbox c=new Checkbox(name,g,v);
    c.addItemListener(this);
    gbc.gridx=x;
    gbc.gridy=y;
    gbc.gridwidth=w;
    gbc.gridheight=h;
    contentsPanel.add(c,gbc);
    return c;
  }

  private int getSelectedLevel() {
    Checkbox c = checkboxGroup.getSelectedCheckbox();

    String s = c.getLabel();
    String tech     = techLevel.getSelectedItem();

    if(tech.equals("None"))
      return 0;

    return Integer.parseInt(tech.substring(1,2));
  }

  boolean OKAction() {    
    Checkbox c = checkboxGroup.getSelectedCheckbox();
    if(c == null)
      return false;

    String s = c.getLabel();
    String tech     = techLevel.getSelectedItem();

    if(tech.equals("None"))
      return true;

    int lvl = Integer.parseInt(tech.substring(1,2));

    getPlanet().startBuildComponent(TechLevel.getTechNumber(s), lvl);

    return true;
  }

  private void reconstructList(int type) {
    techLevel.removeAll();
    
    int max = getPlanet().getTechLevels().getTech(type);

    if(max == 0) {
      techLevel.addItem("None");
    } else {
      for (int i = 1; i <= max; i++) {
	techLevel.addItem(TechLevel.getName(type, i));
      }

      techLevel.select(TechLevel.getName(type, max));
    }
  }

  public void itemStateChanged(ItemEvent evt) {
    if(evt.getStateChange()==ItemEvent.DESELECTED) return;
    Checkbox c = checkboxGroup.getSelectedCheckbox();
    TechLevel tl = getPlanet().getTechLevels();
 
    if(evt.getItem().equals("Hull")){
      reconstructList(ShipComponent.HULL);
    }
    else if (evt.getItem().equals("Engine")) {
      reconstructList(ShipComponent.ENGINE);     
    }
    else if (evt.getItem().equals("Weapon")) {
      reconstructList(ShipComponent.WEAPON);     
    }
    else if (evt.getItem().equals("Shield")) {
      reconstructList(ShipComponent.SHIELD);     
    } 
    else if (evt.getItem().equals("Misc")) {
      reconstructList(ShipComponent.MISC);     
    } 
      

    updateLabelTexts(getPlanet());
  }

  void initiateHelp() {
    HelpWindow.callForHelp("data" + java.io.File.separator + "help",
			   "Playing",
			   "Build Component");
  }

  public void initiateTechGallery() {

    Checkbox c = checkboxGroup.getSelectedCheckbox();
    if(c == null) {
      TechGallery.showTechGallery();      
      return;
    }

    String s = c.getLabel();
    String tech = techLevel.getSelectedItem();

    if(tech.equals("None")) {
      TechGallery.showTechGallery();
      return;
    }

    int lvl = Integer.parseInt(tech.substring(1,2));

    TechGallery.showTechGallery(s+"s", lvl);
  }
}
