/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;


public class ShipHull extends ShipComponent {

  private static int mass[] =   {0,150,500,370,1000,12000,780,2300,4440};
  private static int volume[] = {0,135,390,850,720,900,2400,1700,9999};

  private static int fuel[] = {0,0,0,0,0,0,0,0,0};
  private static int dodechadrium[] = {0,0,0,0,0,0,0,0,252};
  private static int metals[] =   {0, 75, 175,185, 385, 600, 550, 825, 3000};
  private static int minerals[] = {0, 70, 150,105, 325, 525, 250, 685, 2050};
  private static int oil[] =      {0, 75, 150,105, 350, 750, 225, 925, 2525};
  private static int troops[] =   {0,  4,  15,  4,  35, 100,  10, 250, 1500}; 

  private static int techFuel[] = {0,0,0,0,0,0,0,0,0};
  private static int techDodechadrium[] = {0,0,0,0,0,0,0,175,650};
  private static int techMetals[] = highTechRequirement;
  private static int techMinerals[] = mediumTechRequirement;
  private static int techOil[] = smallTechRequirement;
    
  private static int techTroops[] = {0,0,0,0,0,0,0,0,0};

  private static int powerConsumption[] = {0,1,5,4,12,35,8,30,85}; 
  private static int damageResistance[] = {0,300,1000,150,2400,
					   7000,300,7000,75000}; 

  private static int scanRange[]={ 0, 200, 150, 100, 150, 325, 125, 150, 150 };
    
  private static int upkeepCost[] = { 0, 10, 25, 100, 80, 50, 220, 125, 500 };

    private static String[] techName = { "",
				"Scout",
				"Destroyer",
				"Freighter",
				"Cruiser",
				"Space Station",
				"Freighter",
				"Battleship",
				"Battlestar" };

  static PHNameDatabase nameDatabase = null;

  private String name;

//CONSTRUCTOR
  public ShipHull(int player, int tech) {
    super(tech);

    if(nameDatabase == null)
	name="NoName";
    else
	name=nameDatabase.getNewShipName(player, tech);
   }

  /** Returns materials required to build the ShipHull. 
   * getBuildMaterials() returns a MaterialBundle that has all 
   * the materials required to build the ShipHull.
   *
   * @param tech Tech level
   * @return Materials required to build the hull of spec. tech level
   */

  public static MaterialBundle getBuildMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(fuel[tech]);
    mb.putDodechadrium(dodechadrium[tech]);
    mb.putMetals(metals[tech]);
    mb.putMinerals(minerals[tech]);
    mb.putOil(oil[tech]);
    mb.putTroops(troops[tech]);

    return mb;
  }

  /** Returns the time taken to build the ShipHull.
   * Hulls of different tech level have varying building times. Presently,
   * the building time is directly proportional to the tech level of the
   * hull.
   *
   * @param tech Tech level
   * @return Time taken in building the ShipHull of the spec. tech level
   */
  public static int getBuildTime(int tech) {
      if(tech <= 3)
	  return 1;
      if(tech <= 4)
	  return 2;
      if(tech <= 5)
	  return 3;
      if(tech <= 6)
	  return 2;
      if(tech <= 7)
	  return 4;
      if(tech <= 8)
	  return 5;

      return tech;
  }


  /** Returns the materials required in upgrading the ShipHull tech level 
   * of a planet.
   *
   * @param tech Tech level
   * @return Materials required in upgrading one tech to given techlevel
   */
  public static MaterialBundle getTechMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(techFuel[tech]);
    mb.putDodechadrium(techDodechadrium[tech]);
    mb.putMetals(techMetals[tech]);
    mb.putMinerals(techMinerals[tech]);
    mb.putOil(techOil[tech]);
    mb.putTroops(techTroops[tech]);

    return mb;
  }


  /** Returns the power consumption of the ShipHull.
   * ShipHulls do consume power, only if the ship is orbitting a 
   * (friendly?) planet, it can restrain from using up fuel (solarpanels). 
   *
   * @return Power consumption of the ShipComponent
   */ 
  public int getPowerConsumption() {
    return powerConsumption[techLevel];
  }


  /** Returns the maximum damgage that the ShipHull can receive without 
   * being totally destroyed.
   * 
   * @return Maximum obtainable damage
   */
  public int getMaxDamage() {
    return damageResistance[techLevel];
  }

  /** Returns the maximum damgage that the ShipHull can receive without 
   * being totally destroyed.
   * 
   * @param techLevel The tech for which the data is requested.
   * @return Maximum obtainable damage
   */
  public static int getMaxDamage(int techLevel) {
    return damageResistance[techLevel];
  }

  /** Returns the mass of the ShipHull.
   *
   * @return Mass of the ShipHull
   */
  public int getMass() {
    return mass[techLevel];
  }

  /** Returns the volume of the ShipHull.
   * The volume is used in determining, how much one can fit in
   * the ShipHull, the ShipHull is not just a bottomless sack!
   *
   * @return Volume of the ShipHull
   */
  public int getVolume() {
    return volume[techLevel];
  }

  public String toString() {
    return "H"+techLevel+" " +
      name + 
      " (" + (100-getDamagePercentage()) + "%)" ; 
  }

  public String getName() {
    return name;
  }

  public String getFullName() {
    return toString();
  }

  public static String getMapImageName(int tech) {
    if(tech<=0 || tech>TechLevel.MAX_TECH)
      return null;

    return "data/graph/still/components/ships/ship." + tech + ".gif";
  }

    public static String getCloakMapImageName(int tech) {
    if(tech<=0 || tech>TechLevel.MAX_TECH)
      return null;

    return "data/graph/still/components/ships/ship.cloak." + tech + ".gif";

    }

    public int getTechType() {
	return ShipComponent.HULL;
    }

    public int getScanRange() {
	return scanRange[techLevel];
    }

    public int getUpkeepCost() {
	return upkeepCost[techLevel];
    }

    public static String getTechName(int level) {
	if(level<=0 || level>TechLevel.MAX_TECH)
	    return "Invalid Tech";

	return "H" + level + " " + techName[level];
    }
}

