/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import dsc.awt.*;

public class ComponentControlsPanel extends ShipSidePanel implements ItemListener {

  private ScrollPane componentPane;
  private Panel p;
  private GridBagConstraints gbc;

  private Ship ship;
  private Vector engines;
  private Vector shields;
  private Vector weapons;
  private Vector misc;
  private Label engineDivider, weaponDivider, shieldDivider, miscDivider;

  private Vector componentLineVector;

  public ComponentControlsPanel() {
    super(null);

    componentPane = new ScrollPane();
    p = new Panel();
    componentPane.add(p);
    p.setLayout(new GridBagLayout());
    gbc = new GridBagConstraints();

    contentsPanel.setLayout(new GridBagLayout());

    gbc.fill=GridBagConstraints.BOTH;
    gbc.anchor=GridBagConstraints.NORTH;
    gbc.weightx=100;
    gbc.weighty=1;
    gbc.gridwidth=8; 
    gbc.gridheight=1;
    gbc.gridx=0;
    gbc.gridy=1;
    gbc.gridwidth=8; gbc.gridheight=6;
    gbc.weighty=100;
    contentsPanel.add(componentPane, gbc);

    gbc = new GridBagConstraints();

    engineDivider=new Label("== ENGINES ==");
    weaponDivider=new Label("== WEAPONS ==");
    shieldDivider=new Label("== SHIELDS ==");
    miscDivider  =new Label("==  MISC   ==");
    engineDivider.setBackground(Color.blue);
    weaponDivider.setBackground(Color.green);
    shieldDivider.setBackground(Color.red);
    miscDivider.setBackground(Color.black);
    engineDivider.setForeground(Color.white);
    weaponDivider.setForeground(Color.white);
    shieldDivider.setForeground(Color.white);
    miscDivider.setForeground(Color.white);
  }

  void initializeContents(BaseShip bs) {
    ship=(Ship) bs;

    p.removeAll();  

    engines = ship.getEngines();
    weapons = ship.getWeapons();
    shields = ship.getShields();
    misc    = ship.getMisc();

    gbc.anchor=GridBagConstraints.WEST;

    gbc.weightx=1;
    gbc.gridx=0;
    gbc.gridy=0;


    componentLineVector = new Vector();
    
    addComponentLines(engines, engineDivider, gbc);
    addComponentLines(weapons, weaponDivider, gbc);    
    addComponentLines(shields, shieldDivider, gbc);
    addComponentLines(misc,    miscDivider,   gbc);

  }

  private void addComponentLines(Vector v, Label l, GridBagConstraints gbc) {
    
    ComponentLine cl;

    Enumeration e = v.elements();
    
    if(e.hasMoreElements()) {
      gbc.gridwidth=18;
      gbc.fill=GridBagConstraints.HORIZONTAL;
      p.add(l,gbc);
      gbc.gridy++;
      gbc.fill=GridBagConstraints.NONE;

      while (e.hasMoreElements()) {
	ShipComponent sc = (ShipComponent) e.nextElement();
	cl = new ComponentLine(sc);
	componentLineVector.addElement(cl);
	gbc.gridwidth=2; 
	p.add(cl,gbc);
	gbc.gridx=2;
	gbc.gridwidth=8; 
	p.add(new Label(sc+""),gbc);
	gbc.gridx=10;
	gbc.gridwidth=4; 
	p.add(new Label((100-sc.getDamagePercentage())+"%"),gbc);
	gbc.gridx=14;
	gbc.gridwidth=4; 
	p.add(new Label(sc.getPowerOutput()+""),gbc);
	
	gbc.gridy++;
	gbc.gridx=0;
      }
    }
  } 

  public void itemStateChanged(ItemEvent e) {
  }

  void initiateHelp() {
	HelpWindow.callForHelp("data" + java.io.File.separator + "help",
			       "Playing",
			       "Engineering");

  }

  boolean OKAction() {
    Enumeration e = componentLineVector.elements();
    Vector v = new Vector();
    ShipComponentState scs;

    while (e.hasMoreElements()) {
     ComponentLine cl = (ComponentLine) e.nextElement();
     scs = cl.getStateChange();
     if(scs!=null) {
       v.addElement(scs);
     }
    }
    
    if(v.isEmpty()) return true;
    else ship.setSubcomponentStates(v); 

    return true;
  }

  
}
