/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import dsc.awt.*;

public class MaterialTransferPanel extends ShipSidePanel implements AdjustmentListener/*, ItemListener*/  {

  private static final int SCROLLBAR_VISIBLE_AREA = 0;
  private Ship   ship;

  private MaterialBundle fromBundle;
  private MaterialBundle toBundle;

  private Label fromLabel;
  private Label toLabel;

  private Label fromAmountLabel;
  private Label toAmountLabel;

  private Label dodechadriumLabel, fuelLabel, metalsLabel, mineralsLabel,
    oilLabel, troopsLabel;

  private Scrollbar dodechadriumBar, fuelBar, metalsBar, mineralsBar,
    oilBar, troopsBar;

  private ScrollPane topComponentsPane, bottomComponentsPane;

  private Panel topComponentsPanel, bottomComponentsPanel;

  private Vector allTopComponents, allBottomComponents, bottomCheckboxes, topCheckboxes;

  private Label fromDodechadriumLabel, fromFuelLabel, fromMetalsLabel, 
    fromMineralsLabel, fromOilLabel, fromTroopsLabel;

  private Label toDodechadriumLabel, toFuelLabel, toMetalsLabel, 
    toMineralsLabel, toOilLabel, toTroopsLabel;

  //private Label etaLabel;

  private int fromDodechadrium, fromFuel, fromMetals,
    fromMinerals, fromOil, fromTroops;

  private int toDodechadrium, toFuel, toMetals,
    toMinerals, toOil, toTroops;

  private int fromFreeSpace, toFreeSpace;

  private WorldComponent transferTarget;


  public MaterialTransferPanel() {
    //    super("data/graph/still/panel/material.transfer.gif"); 
    super(null);

    contentsPanel.setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();

    gbc.anchor=GridBagConstraints.NORTH;
    gbc.fill=GridBagConstraints.NONE;
    gbc.gridx=0;
    gbc.gridy=0;
    gbc.gridwidth=4;
    gbc.gridheight=1;
    gbc.weightx=0;
    gbc.weighty=0;

    Panel upPanel = new Panel();
    upPanel.setLayout(new GridBagLayout());

    fromLabel=new Label("From");
    toLabel=new Label("To");
    fromAmountLabel=new Label("From ");
    toAmountLabel=new Label(" To  ");
    
    gbc.weightx=1;
    gbc.anchor=GridBagConstraints.WEST;
    upPanel.add(fromLabel, gbc);
    gbc.gridx=4;
    gbc.anchor=GridBagConstraints.EAST;
    upPanel.add(toLabel, gbc);

    gbc.gridx=0; gbc.gridy=0; gbc.gridwidth=8;
    gbc.anchor=GridBagConstraints.NORTH;
    gbc.fill=GridBagConstraints.BOTH;
    contentsPanel.add(upPanel, gbc);

    gbc.weightx=0;
    gbc.fill=GridBagConstraints.NONE;
    gbc.anchor=GridBagConstraints.WEST;
    gbc.gridy=1; gbc.gridx=0; gbc.gridwidth=2;
    contentsPanel.add(fromAmountLabel, gbc);

    gbc.anchor=GridBagConstraints.EAST;
    gbc.gridx=6; gbc.gridwidth=2;
    contentsPanel.add(toAmountLabel, gbc);
    

    gbc.anchor=GridBagConstraints.CENTER;
    gbc.gridx=0;
    gbc.gridwidth=8;

    dodechadriumLabel=new Label("Dodechadrium");
    fuelLabel=new Label("Fuel");
    metalsLabel=new Label("Metals");
    mineralsLabel=new Label("Minerals");
    oilLabel=new Label("Oil");
    troopsLabel=new Label("Troops");

    gbc.gridy=1; contentsPanel.add(dodechadriumLabel, gbc);
    gbc.gridy=3; contentsPanel.add(fuelLabel, gbc);
    gbc.gridy=5; contentsPanel.add(metalsLabel, gbc);
    gbc.gridy=7; contentsPanel.add(mineralsLabel, gbc);
    gbc.gridy=9; contentsPanel.add(oilLabel, gbc);
    gbc.gridy=11; contentsPanel.add(troopsLabel, gbc);

    gbc.anchor=GridBagConstraints.WEST;
    gbc.fill=GridBagConstraints.HORIZONTAL;
    gbc.gridx=0;
    gbc.gridwidth=2;
    
    fromDodechadriumLabel=new Label("000");
    fromFuelLabel        =new Label("000");
    fromMetalsLabel      =new Label("000");
    fromMineralsLabel    =new Label("000");
    fromOilLabel         =new Label("000");
    fromTroopsLabel      =new Label("000");

    gbc.gridy=2; contentsPanel.add(fromDodechadriumLabel, gbc);
    gbc.gridy=4; contentsPanel.add(fromFuelLabel, gbc);
    gbc.gridy=6; contentsPanel.add(fromMetalsLabel, gbc);
    gbc.gridy=8; contentsPanel.add(fromMineralsLabel, gbc);
    gbc.gridy=10; contentsPanel.add(fromOilLabel, gbc);
    gbc.gridy=12; contentsPanel.add(fromTroopsLabel, gbc);

    gbc.anchor=GridBagConstraints.EAST;
    gbc.gridx=6;
    gbc.gridwidth=2;
    
    toDodechadriumLabel=new Label("000");
    toFuelLabel        =new Label("000");
    toMetalsLabel      =new Label("000");
    toMineralsLabel    =new Label("000");
    toOilLabel         =new Label("000");
    toTroopsLabel      =new Label("000");

    gbc.gridy=2; contentsPanel.add(toDodechadriumLabel, gbc);
    gbc.gridy=4; contentsPanel.add(toFuelLabel, gbc);
    gbc.gridy=6; contentsPanel.add(toMetalsLabel, gbc);
    gbc.gridy=8; contentsPanel.add(toMineralsLabel, gbc);
    gbc.gridy=10; contentsPanel.add(toOilLabel, gbc);
    gbc.gridy=12; contentsPanel.add(toTroopsLabel, gbc);


    // startvalue, bubblesize, min, max
    gbc.weightx=10;
    dodechadriumBar=new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
    fuelBar=new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
    metalsBar=new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
    mineralsBar=new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
    oilBar=new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);
    troopsBar=new Scrollbar(Scrollbar.HORIZONTAL, 0, SCROLLBAR_VISIBLE_AREA, 0, 255);

    dodechadriumBar.addAdjustmentListener(this);
    fuelBar.addAdjustmentListener(this);
    metalsBar.addAdjustmentListener(this);
    mineralsBar.addAdjustmentListener(this);
    oilBar.addAdjustmentListener(this);
    troopsBar.addAdjustmentListener(this);

    gbc.gridwidth=4;
    gbc.gridx=2;
    gbc.fill=GridBagConstraints.HORIZONTAL;

    gbc.gridy=2; contentsPanel.add(dodechadriumBar, gbc);
    gbc.gridy=4; contentsPanel.add(fuelBar, gbc);
    gbc.gridy=6; contentsPanel.add(metalsBar, gbc);
    gbc.gridy=8; contentsPanel.add(mineralsBar, gbc);
    gbc.gridy=10; contentsPanel.add(oilBar, gbc);
    gbc.gridy=12; contentsPanel.add(troopsBar, gbc);

    gbc.gridwidth=8;
    gbc.gridheight=4;
    gbc.weighty=100;
    gbc.gridx=0;
    gbc.fill=GridBagConstraints.BOTH;

    gbc.weightx=100;
    topComponentsPane=new ScrollPane();
    bottomComponentsPane=new ScrollPane();

    gbc.gridy=14; contentsPanel.add(topComponentsPane, gbc);
    gbc.gridy=18; contentsPanel.add(bottomComponentsPane, gbc);

    /*  etaLabel = new Label("");
	gbc.fill=GridBagConstraints.HORIZONTAL;
	gbc.anchor=GridBagConstraints.SOUTH;
	gbc.weighty=0;
	gbc.weightx=1;
	gbc.gridwidth=8;
	gbc.gridheight=1;
	gbc.gridx=0;
	gbc.gridy=22;

	contentsPanel.add(etaLabel, gbc);*/

    ///
    ///

    topComponentsPanel=new Panel();
    topComponentsPanel.setLayout(new GridBagLayout());
    topComponentsPane.add(topComponentsPanel);

    bottomComponentsPanel=new Panel();
    bottomComponentsPanel.setLayout(new GridBagLayout());
    bottomComponentsPane.add(bottomComponentsPanel);
        
  }

    void initiateHelp() {
	HelpWindow.callForHelp("data" + java.io.File.separator + "help",
			       "Playing",
			       "Material Transfers");
    }

  private void resetValues() {

    setEnableCheckbox(topCheckboxes, fromFreeSpace);
    setEnableCheckbox(bottomCheckboxes, toFreeSpace);


    fromDodechadriumLabel.setText(fromDodechadrium + "");
    fromFuelLabel.setText(fromFuel + "");
    fromMetalsLabel.setText(fromMetals + "");
    fromMineralsLabel.setText(fromMinerals + "");
    fromOilLabel.setText(fromOil + "");
    fromTroopsLabel.setText(fromTroops + "");

    toDodechadriumLabel.setText(toDodechadrium + "");
    toFuelLabel.setText(toFuel + "");
    toMetalsLabel.setText(toMetals + "");
    toMineralsLabel.setText(toMinerals + "");
    toOilLabel.setText(toOil + "");
    toTroopsLabel.setText(toTroops + "");

    int min, max;

    min=0;
    if(toDodechadrium > fromFreeSpace)
      min+=toDodechadrium-fromFreeSpace;
    max=fromDodechadrium + toDodechadrium;
    if(fromDodechadrium > toFreeSpace)
      max=toFreeSpace + toDodechadrium;
 
    dodechadriumBar.setValues(toDodechadrium, SCROLLBAR_VISIBLE_AREA, min, max+1);

    min=0;
    if(toFuel > fromFreeSpace)
      min+=toFuel-fromFreeSpace;
    max=fromFuel + toFuel;
    if(fromFuel > toFreeSpace)
      max=toFreeSpace + toFuel;

    //System.out.println("" + min + ":" + max + ":" + toFuel + ":" + fromFuel);
    fuelBar.setValues(toFuel, SCROLLBAR_VISIBLE_AREA, min, max+1);

    min=0;
    if(toMetals > fromFreeSpace)
      min+=toMetals-fromFreeSpace;
    max=fromMetals + toMetals;
    if(fromMetals > toFreeSpace)
      max=toFreeSpace + toMetals;

    metalsBar.setValues(toMetals, SCROLLBAR_VISIBLE_AREA, min, max+1);

    min=0;
    if(toMinerals > fromFreeSpace)
      min+=toMinerals-fromFreeSpace;
    max=fromMinerals + toMinerals;
    if(fromMinerals > toFreeSpace)
      max=toFreeSpace + toMinerals;

    mineralsBar.setValues(toMinerals, SCROLLBAR_VISIBLE_AREA, min, max+1);

    min=0;
    if(toOil > fromFreeSpace)
      min+=toOil-fromFreeSpace;
    max=fromOil + toOil;
    if(fromOil > toFreeSpace)
      max=toFreeSpace + toOil;

    oilBar.setValues(toOil, SCROLLBAR_VISIBLE_AREA, min, max+1);

    min=0;
    if(toTroops > fromFreeSpace)
      min+=toTroops-fromFreeSpace;
    max=fromTroops + toTroops;
    if(fromTroops > toFreeSpace)
      max=toFreeSpace + toTroops;

    troopsBar.setValues(toTroops, SCROLLBAR_VISIBLE_AREA, min, max+1);
  }

  void setTransferTarget(Ship s) {
    transferTarget = s;
  }

  void setTransferTarget(Planet p) {
    transferTarget = p;
  }

  void initializeContents(BaseShip bs) {
    ship=(Ship) bs;

    if(transferTarget==null) {
      contentsPanel.setEnabled(false);
    } else {
      contentsPanel.setEnabled(true);
      
      if(transferTarget.getOwner() == ship.getOwner()) {
	if(transferTarget instanceof Ship)
	  toBundle=((Ship)transferTarget).getMaterials();
	else if(transferTarget instanceof Planet)
	  toBundle=((Planet)transferTarget).getMaterials();
	else toBundle=new MaterialBundle(0);
      }
      else
	toBundle=new MaterialBundle(MaterialBundle.UNLIMITED);

      fromBundle=ship.getMaterials();
      
      fromLabel.setText(ship.getName());
      toLabel.setText(transferTarget.getName());

      fromDodechadrium=fromBundle.getDodechadrium();
      fromFuel=fromBundle.getFuel();
      fromMetals=fromBundle.getMetals();
      fromMinerals=fromBundle.getMinerals();
      fromOil=fromBundle.getOil();
      fromTroops=fromBundle.getTroops();

      toDodechadrium=toBundle.getDodechadrium();
      toFuel=toBundle.getFuel();
      toMetals=toBundle.getMetals();
      toMinerals=toBundle.getMinerals();
      toOil=toBundle.getOil();
      toTroops=toBundle.getTroops();



      ///
      ///

      initializeVectors(toBundle,fromBundle);
      topCheckboxes = updateComponentPickPanel(topComponentsPanel,
					       allTopComponents);
      bottomCheckboxes = updateComponentPickPanel(bottomComponentsPanel,
						  allBottomComponents); 

      ///
      ///

      fromFreeSpace=fromBundle.getFreeSpace();
      toFreeSpace=toBundle.getFreeSpace();

      resetValues();
      validate();
    }
  }

  boolean OKAction() {
    MaterialBundle toMaterials=new MaterialBundle(MaterialBundle.UNLIMITED);
    MaterialBundle fromMaterials=new MaterialBundle(MaterialBundle.UNLIMITED);
    
    if(toDodechadrium > toBundle.getDodechadrium())
      toMaterials.putDodechadrium(toDodechadrium-toBundle.getDodechadrium());
    if(toFuel > toBundle.getFuel())
      toMaterials.putFuel(toFuel-toBundle.getFuel());
    if(toMetals > toBundle.getMetals())
      toMaterials.putMetals(toMetals-toBundle.getMetals());
    if(toMinerals > toBundle.getMinerals())
      toMaterials.putMinerals(toMinerals-toBundle.getMinerals());
    if(toOil > toBundle.getOil())
      toMaterials.putOil(toOil-toBundle.getOil());
    if(toTroops > toBundle.getTroops())
      toMaterials.putTroops(toTroops-toBundle.getTroops());

    if(fromDodechadrium > fromBundle.getDodechadrium())
      fromMaterials.putDodechadrium(fromDodechadrium-
				    fromBundle.getDodechadrium());
    if(fromFuel > fromBundle.getFuel())
      fromMaterials.putFuel(fromFuel-fromBundle.getFuel());
    if(fromMetals > fromBundle.getMetals())
      fromMaterials.putMetals(fromMetals-fromBundle.getMetals());
    if(fromMinerals > fromBundle.getMinerals())
      fromMaterials.putMinerals(fromMinerals-fromBundle.getMinerals());
    if(fromOil > fromBundle.getOil())
      fromMaterials.putOil(fromOil-fromBundle.getOil());
    if(fromTroops > fromBundle.getTroops())
      fromMaterials.putTroops(fromTroops-fromBundle.getTroops());

    Vector finalToComponents   = getFinalToComponents();
    Vector finalFromComponents = getFinalFromComponents();

    Enumeration validToEnumeration =
      fromBundle.takeComponents(finalToComponents.elements());

    Enumeration validFromEnumeration =
      toBundle.takeComponents(finalFromComponents.elements());

    fromBundle.subtractFullBundle(toMaterials);
    toBundle.addFullBundle(toMaterials);
    toBundle.subtractFullBundle(fromMaterials);
    fromBundle.addFullBundle(fromMaterials);
    
    while (validToEnumeration.hasMoreElements()) {
      TransferableComponent tc = 
	(TransferableComponent) validToEnumeration.nextElement();

      toBundle.addComponent(tc);
    }

    while (validFromEnumeration.hasMoreElements()) {
      TransferableComponent tc = 
	(TransferableComponent) validFromEnumeration.nextElement();

      fromBundle.addComponent(tc);
    }
    
    ship.startMaterialTransfer(transferTarget.getId(), 
			       toMaterials, 
			       fromMaterials,
			       finalToComponents,
			       finalFromComponents);
    return true;
  }

  private Vector getFinalFromComponents() {
    Enumeration e = allBottomComponents.elements();
    Vector v = new Vector();

    while (e.hasMoreElements()) {
      v.addElement(new Integer (((TransferableComponent) e.nextElement()).getId()));
    }

    return v;    // This is vector of Integers.
  }

  private Vector getFinalToComponents() {
    Enumeration e = allTopComponents.elements();
    Vector v = new Vector();

    while (e.hasMoreElements()) {
      v.addElement(new Integer (((TransferableComponent) e.nextElement()).getId()));
    }

    return v;    // This is vector of Integers.
  }
      
  public void adjustmentValueChanged(AdjustmentEvent evt) {
    Adjustable adj = evt.getAdjustable();
    int diff=0;
    
    if(adj == dodechadriumBar) {
      diff=toDodechadrium-adj.getValue();
      toDodechadrium=adj.getValue();
      fromDodechadrium+=diff;
    } else if(adj == fuelBar) {
      diff=toFuel-adj.getValue();
      toFuel=adj.getValue();
      fromFuel+=diff;
    } else if(adj == metalsBar) {
      diff=toMetals-adj.getValue();
      toMetals=adj.getValue();
      fromMetals+=diff;
    } else if(adj == mineralsBar) {
      diff=toMinerals-adj.getValue();
      toMinerals=adj.getValue();
      fromMinerals+=diff;
    } else if(adj == oilBar) {
      diff=toOil-adj.getValue();
      toOil=adj.getValue();
      fromOil+=diff;
    } else if(adj == troopsBar) {
      diff=toTroops-adj.getValue();
      toTroops=adj.getValue();
      fromTroops+=diff;
    }      

    fromFreeSpace-=diff;
    toFreeSpace+=diff;

    resetValues();
  }  	 
  /*
    public void setEtaLabel(String s) {
    etaLabel.setText(s);
    }*/



  //////////////
  // Copied (and modified) from BuildShipPanel...
  ////////////



  private void initializeVectors(MaterialBundle mbTop,
				 MaterialBundle mbBottom) {
    allTopComponents=new Vector();
    allBottomComponents=new Vector();

    Enumeration e = mbTop.components();
    
    while(e.hasMoreElements()) {
      allTopComponents.addElement((TransferableComponent) e.nextElement());
    }

    e = mbBottom.components();

    while(e.hasMoreElements()) {
      allBottomComponents.addElement((TransferableComponent) e.nextElement());
    }
  }

  void componentPicked(TransferableComponent transferablecomponent) {
    TransferableComponent tc;
 
    if (allTopComponents.contains(transferablecomponent)) {
      allBottomComponents.addElement(transferablecomponent);
      allTopComponents.removeElement(transferablecomponent);
      fromFreeSpace -= transferablecomponent.getMass();
      toFreeSpace += transferablecomponent.getMass();
    }
    else if (allBottomComponents.contains(transferablecomponent)) {
      allTopComponents.addElement(transferablecomponent);
      allBottomComponents.removeElement(transferablecomponent);
      fromFreeSpace += transferablecomponent.getMass();
      toFreeSpace -= transferablecomponent.getMass();
    }
    else throw new IllegalStateException("Tried to move a non-existing transferable component");

    topCheckboxes = updateComponentPickPanel(topComponentsPanel,
					     allTopComponents);
    bottomCheckboxes =  updateComponentPickPanel(bottomComponentsPanel, 
						 allBottomComponents);
    
    resetValues();
    validate();
  }

  void setEnableCheckbox(Vector cb, int freespace) {
    Enumeration e;
    
    e = cb.elements();
    MaterialTransferCheckbox mtc;
    TransferableComponent tc;

    while (e.hasMoreElements()) {
      mtc = (MaterialTransferCheckbox) e.nextElement();
      tc = mtc.getTransferableComponent();
      if (tc.getMass() > freespace) {
	mtc.setEnabled(false);
	mtc.setLabelColor(Color.red);
      }
      else {
	mtc.setEnabled(true);
	mtc.setLabelColor(Color.black);
      }
    }	
  }

  private Vector updateComponentPickPanel(Panel pan,
					Vector components)
  {
    Vector checkboxes = new Vector();
    GridBagConstraints gbc = new GridBagConstraints();

    gbc.anchor=GridBagConstraints.WEST;
    
    gbc.gridwidth=16; 
    gbc.gridx=0;
    gbc.gridy=0;

    pan.removeAll();

    Enumeration e = components.elements();

    while (e.hasMoreElements()) {
      TransferableComponent tc=(TransferableComponent) e.nextElement();
      Label l = new Label();
      MaterialTransferCheckbox c = new MaterialTransferCheckbox(this,tc,l);

      c.setState(false);

      checkboxes.addElement(c);

      gbc.gridx=0; gbc.gridwidth=2;
      pan.add(c, gbc);
    

      l.setText(tc.getFullName());
      gbc.gridx=2; gbc.gridwidth=11;
      pan.add(l, gbc);

      l = new Label();
      l.setText(tc.getMass() + "");
      gbc.gridx=13; gbc.gridwidth=3;
      pan.add(l, gbc);

      gbc.gridy++;
    }
    return checkboxes;
  }

}






