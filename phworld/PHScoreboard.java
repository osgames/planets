/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;

public class PHScoreboard extends dsc.netgame.Scoreboard {
    
    private int staticHome, staticDodechaedron, staticOther, staticSpacePort;
    private int cumHome, cumDodechaedron, cumOther, cumSpacePort;

    PHScoreboard(PHWorldOptions options, PHHostWorld world) {
	super(options, world);

	PHWorldOptions phwo = (PHWorldOptions) options;

	turnLimit  = phwo.getTurnLimit();
	pointLimit = phwo.getScoreLimit();

	staticHome         = phwo.getStaticHomeScore();
	staticDodechaedron = phwo.getStaticDodeScore();
	staticOther        = phwo.getStaticOtherScore();
	staticSpacePort    = phwo.getStaticSpacePortScore();

	cumHome            = phwo.getCumHomeScore();
	cumDodechaedron    = phwo.getCumDodeScore();
	cumOther           = phwo.getCumOtherScore();
	cumSpacePort       = phwo.getCumSpacePortScore();
    }

    protected ScoreEntry getNewScoreEntry(String playerName, int number) {
	return new PHScoreEntry(playerName, number);
    }

    int getStaticHomeScore() {
	return staticHome;
    }

    int getStaticDodeScore() {
	return staticDodechaedron;
    }

    int getStaticOtherScore() {
	return staticOther;
    }

    int getStaticSpacePortScore() {
	return staticSpacePort;
    }

    int getCumHomeScore() {
	return cumHome;
    }

    int getCumDodeScore() {
	return cumDodechaedron;
    }

    int getCumOtherScore() {
	return cumOther;
    }

    int getCumSpacePortScore() {
	return cumSpacePort;
    }
}
