/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;

class PHScoreEntry extends dsc.netgame.ScoreEntry {

    int staticScore, cumScore;

    PHScoreEntry(String playerName, int playerNro) {
	super(playerName, playerNro);

	staticScore = 0;
	cumScore    = 0;
    }
    
    protected void update(Scoreboard scoreboard,
			  int newRoundNro,
			  HostWorld world) {
	PHScoreboard phs = (PHScoreboard) scoreboard;
	staticScore = 0;

	Enumeration e = world.getComponents();
	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if(hwc instanceof HostPlanet) {
		HostPlanet hp = (HostPlanet) hwc;
		
		if(hp.getOwner() == playerNumber) {
		    if(hp.getName().equals("Dodechaedron")) {
			staticScore += phs.getStaticDodeScore();
			cumScore    += phs.getCumDodeScore();
		    } else if(hp.getOwnershipVisibility()) {
			staticScore += phs.getStaticHomeScore();
			cumScore    += phs.getCumHomeScore();
		    } else {
			staticScore += phs.getStaticOtherScore();
			cumScore    += phs.getCumOtherScore();
		    }

		    boolean isSpacePort = false;
		    Enumeration sameLocation = world.getComponentsByLocation(hp.getLocation());
		    while(sameLocation.hasMoreElements()) {
			HostWorldComponent hwcl = 
			    (HostWorldComponent) sameLocation.nextElement();
			if(hwcl.getOwner() == playerNumber &&
			   hwcl instanceof HostShip) {
			    HostShip ship = (HostShip) hwcl;
			    if(ship.getHullTech() == 5) {
				isSpacePort = true;
			    }
			}
		    }
		    
		    if(isSpacePort) {
			staticScore += phs.getStaticSpacePortScore();
			cumScore    += phs.getCumSpacePortScore();
		    }
		}
	    }
	}
    }

    public int getScore() {
	return staticScore + cumScore;
    }

}
