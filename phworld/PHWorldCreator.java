/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.util.CmdLine;

public class PHWorldCreator implements WorldCreator {

  private WorldProperties worldProperties;
  private PHWorldOptionsUI worldOptionsUI;

  public World getClientWorld(GameWindow gw, ClientController c) {
    return (new phworld.PHWorld(gw,c,worldProperties));
  }

  public HostWorld getHostWorld(HostController hc, HostScreen hw) {
    return (new phworld.PHHostWorld(hc,hw,worldOptionsUI.getOptions()));
  }
  
  public WorldOptionsUI getWorldOptionsUI() {
    return worldOptionsUI;
  }

  public WorldProperties getWorldProperties() {
    return worldProperties;
  }

  public Scoreboard getScoreboard(HostWorld world) {
    return (new PHScoreboard((PHWorldOptions) getWorldOptionsUI().getOptions(),
			     (PHHostWorld) world));
  }

  public PHWorldCreator(CmdLine cmdLine) {
    PHWorldOptions worldOptions;

    worldProperties=new PHWorldProperties();
    worldOptions=new PHWorldOptions(cmdLine);
    worldOptionsUI=new PHWorldOptionsUI(worldOptions);
  }

}
