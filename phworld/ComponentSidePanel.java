/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;


abstract class ComponentSidePanel extends Panel implements ActionListener {

  private Button okButton;
  private Button cancelButton;
  private Button helpButton, techButton;
  private SidePanelOwner panelOwner;
  private ImageComponent topImage;

  protected Panel contentsPanel;

  ComponentSidePanel(String imageFile) {
    setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();

    gbc.weightx=100;
    gbc.weighty=1;
    gbc.anchor=GridBagConstraints.SOUTH;
    gbc.fill=GridBagConstraints.HORIZONTAL;
    gbc.gridx=0;
    gbc.gridy=19;
    gbc.gridwidth=4;
    gbc.gridheight=1;

    okButton=new Button("OK");
    add(okButton, gbc);
    okButton.addActionListener(this);

    cancelButton=new Button("Cancel");
    cancelButton.addActionListener(this);
    gbc.gridx=4;
    add(cancelButton, gbc);

    helpButton=new Button("Help");
    helpButton.addActionListener(this);
    gbc.gridy=18;
    add(helpButton, gbc);

    techButton=new Button("Gallery");
    techButton.addActionListener(this);
    gbc.gridx=0;
    add(techButton, gbc);

    gbc.anchor=GridBagConstraints.NORTH;
    gbc.fill=GridBagConstraints.NONE;
    gbc.gridx=0;
    gbc.gridy=0;
    gbc.gridwidth=8;
    gbc.gridheight=5;

    if(imageFile!=null) {
      topImage=new ImageComponent(
	GraphicsLoader.getImage(imageFile));
      add(topImage, gbc);
    }

    gbc.anchor=GridBagConstraints.NORTH;
    gbc.fill=GridBagConstraints.BOTH;
    gbc.gridx=0;
    gbc.gridy=6;
    gbc.gridwidth=8;
    gbc.gridheight=12;
    gbc.weighty=100;
    contentsPanel=new Panel();
    add(contentsPanel, gbc);
  }

  void initialize(SidePanelOwner s) {
    panelOwner=s;
    initializeContents(s);
    //validate();
  }

  public void actionPerformed(ActionEvent evt) {
    Object src = evt.getSource();

    if(src == okButton) {
      if(OKAction())
	panelOwner.sidePanelOut();
    }

    if(src == cancelButton) {
      cancelAction();
    }

    if(src == techButton) {
      initiateTechGallery();
    }

    if(src == helpButton) {
      initiateHelp();
    }
  }

  void cancelAction() {
    panelOwner.sidePanelOut();
  }

  SidePanelOwner getOwner() {
    return panelOwner;
  }

  protected void initiateTechGallery() {
    TechGallery.showTechGallery();
  }

  protected void setImage(String imageFile) {
    topImage.setImage(GraphicsLoader.getImage(imageFile));
    topImage.repaint();
  }

  abstract void initiateHelp();
  abstract void initializeContents(SidePanelOwner s);
  abstract boolean OKAction();
}
