/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;


public class MaterialTransferWCM extends WorldComponentMessage {

  private MaterialBundle toMaterials, fromMaterials;
  private Vector         toComponents, fromComponents;
  private int transferTo;

  private HostShip initiator=null; // Set at the host side.

  public MaterialTransferWCM(int toComponent, 
			     int transferTo, 
			     MaterialBundle toMaterials,
			     MaterialBundle fromMaterials,
			     Vector         toComponents,
			     Vector         fromComponents) {

    super(toComponent);
    this.transferTo=transferTo;

    this.toMaterials=toMaterials;
    this.fromMaterials=fromMaterials;

    this.toComponents=toComponents;
    this.fromComponents=fromComponents;
  }

  public MaterialBundle getToMaterials() {
    if(toMaterials==null)
      return new MaterialBundle(MaterialBundle.UNLIMITED);
    else
      return toMaterials;
  }

  public MaterialBundle getFromMaterials() {
    if(fromMaterials==null)
      return new MaterialBundle(MaterialBundle.UNLIMITED);
    else
      return fromMaterials;
  }

  public int getTransferTarget() {
    return transferTo;
  }

  public Enumeration getToComponents() {
    return createIntegerEnumeration(toComponents);
  }

  public Enumeration getFromComponents() {
    return createIntegerEnumeration(fromComponents);
  }

  public void forceOneWayTransfer() {
    fromMaterials = new MaterialBundle(MaterialBundle.UNLIMITED);
    fromComponents = new Vector();
  }

  private Enumeration createIntegerEnumeration(Vector v) {
    if(v == null)
      return (new Vector().elements());

    Enumeration e = v.elements();

    while (e.hasMoreElements()) {
      Object o = e.nextElement();

      if(!(o instanceof Integer)) {
	return (new Vector()).elements();
      }
    }

    return v.elements();
  }

  protected void setInitiator(HostShip h) {
    initiator=h;
  }

  protected HostShip getInitiator() {
    return initiator;
  }
}
