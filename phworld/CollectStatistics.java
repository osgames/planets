package phworld;

import dsc.netgame.*;

import java.util.Vector;
import java.util.Enumeration;
import java.io.*;


public class CollectStatistics implements Serializable {

    class PlayerStatistics {
	private MaterialBundle materialProduction, totalMaterials;
	private int planets;
	private int ships;
	private int totalShieldPower, totalWeaponHPower, totalWeaponSPower;
	private int maxHullTech, maxEngineTech, maxWeaponTech;
	private int maxShieldTech, maxMiscTech, totalTech;
	private int score;

	PlayerStatistics() {
	    materialProduction = new MaterialBundle(MaterialBundle.UNLIMITED);
	    totalMaterials = new MaterialBundle(MaterialBundle.UNLIMITED);
	    planets = 0;
	    ships = 0;
	    totalShieldPower = totalWeaponHPower = totalWeaponSPower = 0;
	    maxHullTech = maxEngineTech = maxWeaponTech = 0;
	    maxShieldTech = maxMiscTech = 0;
	    totalTech = 0;
	    score = 0;
	}

      public boolean isPlaying() {
	if(planets > 0)
	  return true;
	return false;
      }

	public void newShip(Ship s) {
	    ships++;
	    totalShieldPower += s.getShieldPower();
	    totalWeaponHPower +=
		ShipMath.calcWeaponHullPower(s.getWeapons());
	    totalWeaponSPower +=
		ShipMath.calcWeaponShieldPower(s.getWeapons());
	    totalMaterials.addFullBundle(s.getMaterials());
	}

	public void newPlanet(Planet p) {
	    planets++;
	    materialProduction.addFullBundle(p.getProduction());
	    totalMaterials.addFullBundle(p.getMaterials());

	    TechLevel tl = p.getTechLevels();
	    if (maxHullTech < tl.getHullTech())
		maxHullTech = tl.getHullTech();
	    if (maxEngineTech < tl.getEngineTech())
		maxEngineTech = tl.getEngineTech();
	    if (maxWeaponTech < tl.getWeaponTech())
		maxWeaponTech = tl.getWeaponTech();
	    if (maxShieldTech < tl.getShieldTech())
		maxShieldTech = tl.getShieldTech();
	    if (maxMiscTech < tl.getMiscTech())
		maxMiscTech = tl.getMiscTech();

	    totalTech += tl.getHullTech() + tl.getEngineTech() +
		tl.getWeaponTech() + tl.getShieldTech() +
		tl.getMiscTech();
	}

	public void addScore(int s) {
	    score += s;
	}

	public String toString() {
	    return
		planets + " " +
		ships + " " +
		totalShieldPower + " " +
		totalWeaponHPower + " " +
		totalWeaponSPower + " " +
		totalTech + " " +
		maxHullTech + " " +
		maxEngineTech + " " +
		maxWeaponTech + " " +
		maxShieldTech + " " +
		maxMiscTech + " " +
		materialProduction.getFuel() + " " +
		materialProduction.getDodechadrium() + " " +
		materialProduction.getMetals() + " " +
		materialProduction.getMinerals() + " " +
		materialProduction.getOil() + " " +
		materialProduction.getTroops() + " " +
		totalMaterials.getFuel() + " " +
		totalMaterials.getDodechadrium() + " " +
		totalMaterials.getMetals() + " " +
		totalMaterials.getMinerals() + " " +
		totalMaterials.getOil() + " " +
		totalMaterials.getTroops() + " " +
		score;
	}
    }

    private PHHostWorld world;
    private String baseFilename;
    private int maxPlayer;
    private String playerNames[];
    private boolean activePlayers[];
    private int currentRound;

    /**
     * Creates a statistics collector object.
     *
     * @param world the world of the game (saved in this object)
     * @param baseFilename base file name for generated files
     */
    public CollectStatistics(PHHostWorld world, String baseFilename) {
	int i;

	this.world = world;
	this.baseFilename = baseFilename;
	maxPlayer = GlobalDefines.maxPlayers;

	currentRound = 0;
	activePlayers = null;

	playerNames = new String[maxPlayer];
	for (i = 0; i < maxPlayer; i++)
	    playerNames[i] = world.getPlayerName(i);
    }

    /**
     * Adds information from the world into the statistics. Should
     * be called at the start of every round in the game.
     */
    public void newRoundToStatistics() {
	PlayerStatistics ts = new PlayerStatistics();
	PlayerStatistics ps[] = new PlayerStatistics[maxPlayer];
	Enumeration e;
	int i;

	for (i = 0; i < maxPlayer; i++) {
	    ps[i] = new PlayerStatistics();
	    ScoreEntry se = world.getScore(i);
	    if(se != null) {
		int s = se.getScore();
		ps[i].addScore(s);
		ts.addScore(s);
	    }
	}

	currentRound++;

	/* Look at planets and ships */

	e = world.getComponents();	
	while (e.hasMoreElements()) {
	    HostWorldComponent wc = (HostWorldComponent) e.nextElement();
	    int owner = wc.getOwner();

	    if((wc instanceof HostPlanet)) {
		Planet p = 
		    (Planet) ((HostPlanet) wc).getWorldComponent(owner);
		ts.newPlanet(p);
		if (owner >= 0)
		    ps[owner].newPlanet(p);
	    } else if ((wc instanceof HostShip)) {
		Ship s =
		    (Ship) ((HostShip) wc).getWorldComponent(owner);
		ts.newShip(s);
		if (owner >= 0)
		    ps[owner].newShip(s);
	    }
	}

	if (currentRound == 1) {
	    /* Initialise array of active players */
	    activePlayers = new boolean[maxPlayer];
	    for (i = 0; i < maxPlayer; i++)
		activePlayers[i] = ps[i].isPlaying();

	    initialiseStatsFiles();
	}

	appendToStatsFiles(ts, ps);
    }

    private void initialiseStatsFiles() {
	try {
	    writePlayersFile(baseFilename + ".players");
	    
	    newFile(baseFilename + ".total");

	    for (int i = 0; i < maxPlayer; i++) {
		if (activePlayers[i] == true)
		    newFile(baseFilename + ".player" + i);
	    }
	} catch (Exception e) {
	    System.err.println(""+e);
	}	
    }

    private void appendToStatsFiles(PlayerStatistics ts,
				    PlayerStatistics ps[]) {
	try {
	    appendToFile(baseFilename + ".total",
			 currentRound + " " + ts);
	    
	    for (int i = 0; i < maxPlayer; i++)
		if (activePlayers[i] == true)
		    appendToFile(baseFilename + ".player" + i,
				 currentRound + " " + ps[i]);
	} catch (Exception e) {
	    System.err.println(""+e);
	}
    }

    /** Creates a new, empty file */
    private void newFile(String filename) throws IOException {
	FileOutputStream fs = new FileOutputStream(filename);
	fs.close();
    }

    /** Appends a line to a file. */
    private void appendToFile(String filename, String line)
	throws IOException {
	FileOutputStream fs = new FileOutputStream(filename, true);
	PrintWriter ps = new PrintWriter(fs);

	ps.println(line);
	ps.close();
	fs.close();
    }

    /** Writes the names of active players into a file */
    private void writePlayersFile(String filename) throws IOException {
	FileOutputStream fs = new FileOutputStream(filename);
	PrintWriter ps = new PrintWriter(fs);

	for (int i = 0; i < maxPlayer; i++) {
	    if (activePlayers[i] == true)
		ps.println(i + " " + playerNames[i]);
	}
	ps.close();
	fs.close();
    }
}
