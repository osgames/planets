/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.io.*;


public class TechGallery extends LayoutableFrame implements ActionListener, ItemListener {
  
  private Button previousButton, nextButton, closeButton;
  private Choice categoryChoice, levelChoice;
  private ImageLoadBuffer imageLoadBuffer;
  private DoubleImageComponent imageComponent;
  private TextArea descriptionText;

  private static final String dataPath="data/graph/still/techgallery/";
  private static final String textPath="data/tech";

  public TechGallery() {
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) { setVisible(false); } 
    });
    setSize(640,400);
    setTitle("Technology Gallery");
    setBackground(new Color(100,100,100));
    setForeground(new Color(255,255,255));
    setBackground(new Color(0));

    GridBagLayout gbl = new GridBagLayout();
    setLayout(gbl);
    
    Image i=GraphicsLoader.getImage("data/graph/still/icons/game.gif");
    setIconImage(i);
    imageLoadBuffer=new ImageLoadBuffer();

    previousButton=createButton("<<<<", this);
    nextButton=createButton(">>>>", this);
    closeButton=createButton("Close", this);

    categoryChoice = new Choice();
    categoryChoice.add("Hulls");
    categoryChoice.add("Engines");
    categoryChoice.add("Weapons");
    categoryChoice.add("Shields");
    categoryChoice.add("Misc");

    categoryChoice.select("Hulls");

    levelChoice = new Choice();
    for(int n=1; n<=8; n++) {
      levelChoice.add("Tech " + n );
    }

    levelChoice.select("Tech 1");

    categoryChoice.addItemListener(this);
    levelChoice.addItemListener(this);

    imageComponent=new DoubleImageComponent(imageLoadBuffer.getImage(
      getImageFilename()), imageLoadBuffer.getImage("data/graph/still/techgallery/border.gif"));

    descriptionText=new TextArea("",20,10,TextArea.SCROLLBARS_VERTICAL_ONLY);
    descriptionText.setEditable(false);

    gbAdd(imageComponent,1,1,0,0,10,10);
    gbAdd(categoryChoice,100,0,GridBagConstraints.CENTER,
	  GridBagConstraints.HORIZONTAL,10,0,5,1);
    gbAdd(levelChoice,100,0,GridBagConstraints.CENTER,
	  GridBagConstraints.HORIZONTAL,15,0,5,1);
    gbAdd(previousButton,1,10,GridBagConstraints.SOUTH,
	  GridBagConstraints.HORIZONTAL,0,14,5,1);
    gbAdd(nextButton,1,10,GridBagConstraints.SOUTH,
	  GridBagConstraints.HORIZONTAL,5,14,5,1);
    gbAdd(closeButton,100,10,GridBagConstraints.SOUTH,
	  GridBagConstraints.HORIZONTAL,10,14,10,1);
    gbAdd(descriptionText,200,200,GridBagConstraints.CENTER,
          GridBagConstraints.BOTH,10,3,10,12);

    placeDescriptionText();
  }

  private String getImageFilename() {
    String category = categoryChoice.getSelectedItem();
    String tech     = levelChoice.getSelectedItem();
    int lvl = Integer.parseInt(tech.substring(5));

    if(category.equals("Hulls")) {
      return (dataPath+"hull"+lvl+".gif");
    }

    if(category.equals("Engines")) {
      return (dataPath+"engine"+lvl+".gif");
    }

    if(category.equals("Weapons")) {
      return (dataPath+"weapon"+lvl+".gif");
    }

    if(category.equals("Shields")) {
      return (dataPath+"shield"+lvl+".gif");
    }

    if(category.equals("Misc")) {
      return (dataPath+"misc"+lvl+".gif");
    }

    return null;
  }

  private int getSelectionType() {
    String category = categoryChoice.getSelectedItem();
    return TechLevel.getTechNumber(category);
  }

  private int getSelectionLevel() {
    String tech = levelChoice.getSelectedItem();
    int lvl = Integer.parseInt(tech.substring(5));
    return lvl;
  }

  private String getTextFilename() {
    String category = categoryChoice.getSelectedItem();
    String tech     = levelChoice.getSelectedItem();
    int lvl = Integer.parseInt(tech.substring(5));
    String filename="NoSuchTechError";

    if(category.equals("Hulls")) {
      filename="hulls";
    } else if(category.equals("Engines")) {
      filename="engines";
    } else if(category.equals("Weapons")) {
      filename="weapons";
    } else if(category.equals("Shields")) {
      filename="shields";
    } else if(category.equals("Misc")) {
      filename="misc";
    }

    return (textPath+File.separator+filename+File.separator+lvl+".text");
  }

  private void placeDescriptionText() {
    BufferedReader in=null;

    try {
      File f   = new File(getTextFilename());
      in=new BufferedReader(new FileReader(f));
    } catch (Exception e) {
      System.err.println("Error Loading " + getTextFilename()); System.exit(1);
    }

    descriptionText.setText("");

    String s;
    try {
      do {
	s = in.readLine();
	if(s!=null)
	  descriptionText.append(s + "\n");
      } while(s != null);
    } catch (IOException e) {}

    try {
      in.close();
    } catch (IOException e) {}
  }

  private void placeBuildCostText() {
      MaterialBundle build;
      MaterialBundle tech;

      build = TechLevel.getBuildMaterials(getSelectionType(),
					  getSelectionLevel());
      tech  = TechLevel.getTechMaterials(getSelectionType(),
					 getSelectionLevel(), 0);

    descriptionText.append("\nBuild/Tech requirements:\n");
    descriptionText.append(build.mergeToString(tech));
    descriptionText.append("\n");
  }

  private void placeCapabilitiesText() {
    String category = categoryChoice.getSelectedItem();
    int lvl = getSelectionLevel();
    ShipComponent sc = null;

    if(category.equals("Hulls")) {
      sc = new ShipHull(-1, lvl);
    } else if(category.equals("Engines")) {
      sc = new ShipEngine(lvl);
    } else if(category.equals("Weapons")) {
      sc = new ShipWeapon(lvl);
    } else if(category.equals("Shields")) {
      sc = new ShipShield(lvl);
    } else if(category.equals("Misc")) {
      sc = new ShipMisc(lvl);
    }

    descriptionText.append("Properties:\n");
    descriptionText.append("Mass: " + sc.getMass() + "\n");
    descriptionText.append("Hit points: " + sc.getMaxDamage() + "\n");
    descriptionText.append("Power consumption: "+sc.getPowerConsumption()+
			   "\n");

    if(category.equals("Hulls")) {
      descriptionText.append("Volume: " + ((ShipHull) sc).getVolume() + "\n");
      descriptionText.append("Base scan range: " + 
			     (((ShipHull) sc).getScanRange()/
			      ShipMath.SPEED_CONST) + "\n");
      descriptionText.append("Upkeep cost: " +
			     ((ShipHull) sc).getUpkeepCost() + " credits");
    } else if(category.equals("Engines")) {
      descriptionText.append("Power output: " + 
			     ((ShipEngine) sc).getMaxPowerProduction() + "\n");
    } else if(category.equals("Weapons")) {
      descriptionText.append("Max damage (hulls/shields): " + 
			     ((ShipWeapon) sc).getMaxInflictableHullDamage() +
			     "/"+
			     ((ShipWeapon) sc).getMaxInflictableShieldDamage() +
			     "\n");
      descriptionText.append("Max troop damage: " +
			     ((ShipWeapon) sc).getMaxInflictableTroopDamage() + "\n");
    } else if(category.equals("Shields")) {
      descriptionText.append("Shield power: " + 
			     ((ShipShield) sc).getMaxShieldPower() + "\n");
      descriptionText.append("Peristent shield power: " + 
			     ((ShipShield) sc).getMinShieldPower() + "\n");
    } else if(category.equals("Misc")) {
      descriptionText.append(((ShipMisc) sc).getPowerDescription() + "\n");
    }
  }

  private void updateDisplay() {
    imageComponent.setImage(imageLoadBuffer.getImage(getImageFilename()));
    imageComponent.repaint();
    placeDescriptionText();
    placeBuildCostText();
    placeCapabilitiesText();
    descriptionText.setCaretPosition(0);
    repaint();
  }

  public void setSubject(String techtype, int level) {
    levelChoice.select("Tech "+level);    
    categoryChoice.select(techtype);
    updateDisplay();
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();

    if(arg.equals("Close")) {
      setVisible(false);
    }
    else if (arg.equals("Help")) {
      HelpWindow.callForHelp("data/help", "Window", "TechGallery");
    }
    else if (arg.equals(">>>>")) {
      int n=levelChoice.getSelectedIndex();
      if(n<7) {
	levelChoice.select(n+1);
	updateDisplay();
      }
    }
    else if (arg.equals("<<<<")) {
      int n=levelChoice.getSelectedIndex();
      if(n>0) {
	levelChoice.select(n-1);
	updateDisplay();
      }
    }
  }

  public void itemStateChanged(ItemEvent evt) {
    if(evt.getStateChange()==ItemEvent.DESELECTED) return;
    updateDisplay();
  }

  public static void main(String args[]) {
      showTechGallery();
  }

  private static TechGallery techGallery = null;

  public static void showTechGallery(String techtype, int level) {
    showTechGallery();
    techGallery.setSubject(techtype, level);
  }

  public static void showTechGallery() {
    if(techGallery!=null) {
      techGallery.setVisible(true);
      techGallery.show();
    } else {
      techGallery=new TechGallery();
      techGallery.show();
      techGallery.setSubject("Hulls", 1);
    }    

  }
}
