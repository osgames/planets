/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;


public class HostMine extends HostMiscComponent {
  private static final int DECAYRATE = 1000;
  private static final int MIN_POWER = 1000;
  private int power;

    private int age; /* in rounds */
    
    /** Minumum age of effective minefieds */
    public static final int EFFECTIVE_AGE=1;

    public int getRadius() {
	return Mine.calcRadius(power);
    }

    public void addMines(ShipMisc mine) {
	age=0;
	power += (int) (mine.getPowerOutput()*mine.getPowerOutput()*Math.PI);

	hostWorld.sendChatMessage(owner, getName() + " has grown to " +
				  power + " mines.");

    }

  public void doRound() { 
      super.doRound();

      power-=DECAYRATE;
      age+=1;

      if(power<MIN_POWER) {
	  hostWorld.removeComponent(getId());

	  HostPlanet planet = 
	      ((PHHostWorld)hostWorld).findNearestPlanet(getLocation());
	  String npn = "";
	  if(planet != null)
	      npn=planet.getName();

	  hostWorld.sendChatMessage(owner, name +
				    " has run out of mines near "+
				    npn +".");
      }
  }

    public WorldComponent getWorldComponent(int player) {
	Mine m;

	if(hostWorld.isVisibleByPlayer(player, this))
	    m=new Mine(id, name, owner, location, power);
	else {
	    return null;
	}
    
	m.setScanRange(scanRange);
	m.setMessage("Mines: " + power);
	return m;
  }

    public void takeDamage(int amount, HostShip shooter) {
	if(!isEffective()) {
	    amount = amount * 5;
	}

	power-=amount;
	if(power<1)
	    power=1;

	hostWorld.sendChatMessage(owner, 
				  hostWorld.getPlayerName(shooter.getOwner()) +
				  "\'s ship " + shooter.getName() + 
				  " destroyed " + 
				  amount + 
				  " mines ("+power+" remaining) from your " +
				  name +".");
	hostWorld.sendChatMessage(shooter.getOwner(), 

				  "Your ship " + shooter.getName() + 
				  " destroyed " + 
				  amount + 
				  " mines ("+power+" remaining) from " +
				  hostWorld.getPlayerName(getOwner()) +
				  "'s " + name +".");
    }

    public void setName(String n) {
	name = n;
    }

    public boolean isEffective() {
	if(age < EFFECTIVE_AGE) {
	    return false;
	} else {
	    return true;
	}
    }

  public HostMine(HostWorld hw, 
		  int owner, 
		  Location location,
		  ShipMisc mine) {
    super(hw,owner,location);

    power=(int) (mine.getPowerOutput()*mine.getPowerOutput()*Math.PI);
    age  = 0;

    HostPlanet planet = ((PHHostWorld)hostWorld)
	.findNearestPlanet(getLocation());
    String npn = "";
    if(planet != null)
	npn=planet.getName();

    hostWorld.sendChatMessage(owner, "A new minefield with " + power +
				    " mines has been deployed near " +
				    npn + ".");
  }

  public boolean isVisible() {
    return true;
  }
}

