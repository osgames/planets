/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;


public class Planet extends WorldComponent implements ActionListener,
                            SidePanelOwner {

  private static Panel controlPanel = null;  
  private static GridBagConstraints controlPanelConstraints = null;

  /* Primary Card components */
  private static Panel generalPanel = null;
  private static TextArea infoText = null;
  private static Panel buttonPanel = null;
  private static Button shipBuildButton = null;
  private static Button componentBuildButton = null;
  private static Button techBuildButton = null;
  private static Button miningSurveyButton = null;
  private static Button improveProductionButton = null;
  private static Button idleButton = null;
  private static Button autotransferButton = null;
  private static Planet buttonListener = null;
  private static ImageComponent imageComponent;

  private static PlanetSidePanel improveTech    = null;
  private static PlanetSidePanel buildShip      = null;
  private static PlanetSidePanel buildComponent = null;
  private static PlanetSidePanel miningSurvey   = null;

  private static Font textFont = null;
  private static FontMetrics textFontMetrics = null;
  private static final int TEXT_FONT_SIZE = 12; 

  private MaterialBundle materials;
  private MaterialBundle materialProduction;
  private TechLevel techLevels;
  private int activityType;
  private int activityDuration;
  private String activityText;
  private int planetType=-1;
  private int edtCircleRadius=0;
  private int autotransferTarget=-1;
  private int troops;
  private int defensePower = 0;
  private CloneShipInfo cloneShipInfo = null;

  public Planet (int id, String name, int owner, Location location) {
    super(id,name,owner,location);
  }

  private void changeControlPanel(Panel newPanel) {
    if(controlPanelConstraints == null) {
      controlPanelConstraints = new GridBagConstraints();

      controlPanelConstraints.fill=GridBagConstraints.BOTH;
      controlPanelConstraints.anchor=GridBagConstraints.NORTH;
      controlPanelConstraints.weightx=100;
      controlPanelConstraints.weighty=1;
      controlPanelConstraints.gridwidth=1; 
      controlPanelConstraints.gridheight=1;
      controlPanelConstraints.gridx=0;
      controlPanelConstraints.gridy=0;
    }

    controlPanel.removeAll();
    controlPanel.add(newPanel, controlPanelConstraints);
    controlPanel.validate();
  }

  private void createControlPanel() {
    controlPanel=new Panel();
    controlPanel.setLayout(new GridBagLayout());

    generalPanel=new Panel();
    changeControlPanel(generalPanel);

    imageComponent = new ImageComponent(
					world.getImage(PlanetType.getPanelImageFileName(planetType)));
    generalPanel.add(imageComponent);

    infoText=new TextArea("",15,22,TextArea.SCROLLBARS_NONE);
    infoText.setEditable(false);
    
    generalPanel.setLayout(new VerticalFlowLayout());
    generalPanel.add(infoText);

    buttonPanel=new Panel();
    buttonPanel.setLayout(new VerticalFlowLayout());

    shipBuildButton=new Button("Build Ship");
    buttonPanel.add(shipBuildButton);

    componentBuildButton=new Button("Build Component");
    buttonPanel.add(componentBuildButton);

    techBuildButton=new Button("Improve Tech");
    buttonPanel.add(techBuildButton);

    improveProductionButton=new Button("Improve Production");
    buttonPanel.add(improveProductionButton);

    idleButton=new Button("Idle Mode");
    buttonPanel.add(idleButton);

    autotransferButton=new Button("Autotransfer");
    buttonPanel.add(autotransferButton);

    miningSurveyButton=new Button("Mining Survey");
    buttonPanel.add(miningSurveyButton);

    generalPanel.add(buttonPanel);

    improveTech=new ImproveTechPanel();
    buildShip=new BuildShipPanel();
    buildComponent=new BuildComponentPanel();
    miningSurvey=new MiningSurveyPanel();

  }

  protected void resetInfoText() {
      if(infoText == null)
	  return;

    infoText.setText(getName() + "\n\n");

    if(materials == null) {
	if(getOwner() == -1) {
	    infoText.append("Unknown owner\n\n");
	} else {
	    infoText.append("Owned by " + 
			    world.getController().
			    playersInfo.
			    getPlayerName(getOwner()) + "\n\n");
	}
      if(troops >= 0)
	  infoText.append("" + troops + " troops");
    }
    else {
      infoText.append(materials + "");
      if(defensePower > 0) {
	  infoText.append("Defense power: " + defensePower);
      }

      infoText.append("\n");
    }

    if(techLevels != null)
      infoText.append(techLevels +"\n\n");

    if(activityText != null)
      infoText.append(activityText + "\n");

    if(getAutotransferTargetName() != null)
      infoText.append("Autotransf. to: " + getAutotransferTargetName() + 
		      "(" + getAutotransferEfectivity() + "%)");
  }

  protected void resetControlPanel() {

    edtCircleRadius=0;
    imageComponent.setImage(world.getImage(
					   PlanetType.getPanelImageFileName(planetType)));
    
    resetInfoText();
    
    if(buttonListener!=null) {
      shipBuildButton.removeActionListener(buttonListener);
      componentBuildButton.removeActionListener(buttonListener);
      techBuildButton.removeActionListener(buttonListener);
      miningSurveyButton.removeActionListener(buttonListener);
      idleButton.removeActionListener(buttonListener);
      autotransferButton.removeActionListener(buttonListener);
      improveProductionButton.removeActionListener(buttonListener);
      buttonListener=null;
    }

    if(world.getController().playerNumber==getOwner()) {
      shipBuildButton.addActionListener(this);
      techBuildButton.addActionListener(this);
      miningSurveyButton.addActionListener(this);
      idleButton.addActionListener(this);
      autotransferButton.addActionListener(this);
      componentBuildButton.addActionListener(this);
      improveProductionButton.addActionListener(this);
      buttonListener=this;
      buttonPanel.setEnabled(true);
    } else {
      buttonPanel.setEnabled(false);
    }

    changeControlPanel(generalPanel);
  }

    public Menu getMenu() {
	if(cloneShipInfo == null) {
	    return null;
	} else {
	    Menu m = new Menu("Planet");

	    MenuItem di = new MenuItem("Clone Ship \""+
				       cloneShipInfo.getName() + "\"", 
				       new MenuShortcut(KeyEvent.VK_L));
	    di.addActionListener(this);
	    m.add(di);

	    return m;
	}
    }

  public String getMapImage() {
    return PlanetType.getImageFileName(planetType);
  }

  public boolean isVisible() {
    return true;
  }

  public Panel getControlPanel() {
    if(controlPanel==null) {
      createControlPanel();
    }
    
    resetControlPanel();

    return controlPanel;
  }

  public boolean paintToForeground() {
    return false;
  }

  public void sidePanelOut() {
    this.edtCircleRadius=0;
    changeControlPanel(generalPanel);
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();
    Object src = evt.getSource();

    if(arg == null) {
	MenuItem mi = (MenuItem) evt.getSource();
	arg=mi.getLabel();
    }

    if(arg.equals("Improve Tech")) {
      improveTech.initialize(this);
      changeControlPanel(improveTech);
    } 
    else if(arg.equals("Build Ship")) {
      buildShip.initialize(this);
      changeControlPanel(buildShip);
    } 
    else if(arg.equals("Build Component")) {
      buildComponent.initialize(this);
      changeControlPanel(buildComponent);
    }
    else if(arg.equals("Mining Survey")) {
      miningSurvey.initialize(this);
      changeControlPanel(miningSurvey);
    }
    else if(arg.equals("Idle Mode")) {
      startIdling();
    }
    else if(arg.equals("Improve Production")) {
      startProductionImproving();
    }
    else if(arg.equals("Autotransfer")) {
      world.requestComponent();
    }
    else if(arg.startsWith("Clone Ship")) {
      startShipCloning();
    }
  }

  public void drawAdditionalPreGraphs(ScaledGraphics g) {
    if(autotransferTarget == -1)
      return;

    if(!((PHWorld)world).isWaypointDrawingForAllEnabled())
      return;

    Color pri = GlobalDefines.getPrimaryColor(-1);
    g.setColor(pri);

    Location targetLocation=null;

    targetLocation = 
	((PHWorld) world).getComponentById(autotransferTarget).getLocation();

    g.drawLine(location.getX(), location.getY(),
	       targetLocation.getX(), targetLocation.getY());
  }


  public void drawAdditionalGraphs(ScaledGraphics g) {
    if(textFont==null) {
      textFont = new Font("SansSerif", Font.ITALIC, TEXT_FONT_SIZE);
      textFontMetrics = g.getFontMetrics(textFont);
    }
    
    String s  = getName();
    int width = textFontMetrics.stringWidth(s);
    int height= textFontMetrics.getHeight();
    int x     = getLocation().getX();
    int y     = getLocation().getY();
    int scale = g.getScaleFactor();

    Color pri = GlobalDefines.getPrimaryColor(owner);
    Color sec = GlobalDefines.getSecondaryColor(owner);

    g.setColor(pri);
    g.setFont(textFont);
    g.drawString(s, x-(width*scale)/2, y+25+height*scale);

    if(!sec.equals(pri)) {
      g.setColor(sec);
      g.drawLine(x-(width*scale)/2,y+27+height*scale ,
		 x+(width*scale)/2, y+27+height*scale);
    }

    drawIdleMarker(g);
    drawEDTCircle(g);
    drawActivityText(g);
  }

  public void drawEDTCircle(ScaledGraphics g) {
    if(world.getActiveComponent() != this)
      return;

    if(edtCircleRadius<=0)
      return;

    if(!((PHWorld)world).isEDTCircleDrawingEnabled())
      return;

    g.setColor(Color.green);
    g.drawArc(getLocation().getX()-edtCircleRadius,
	      getLocation().getY()-edtCircleRadius,
	      edtCircleRadius*2,
	      edtCircleRadius*2,
	      0,
	      360);
  }


  private void drawIdleMarker(ScaledGraphics g) {
    if(world.getController().playerNumber!=getOwner())
      return;

    if(activityText==null ||
       activityText.equals("Doing nothing") ||
       activityText.startsWith("Just") ||
       activityText.startsWith("Improved")) {
      Image i = world.getImage(getMapImage());
      int dxsize = i.getWidth(generalPanel)/2;
      int dysize = i.getHeight(generalPanel)/2;

      if(activityText!=null && (
	 activityText.startsWith("Improved") ||
	 activityText.startsWith("Just built new ") ||
	 activityText.startsWith("Just cloned ")))
	g.setColor(Color.green);
      else
	g.setColor(Color.blue);
      g.drawRect(getLocation().getX()-dxsize-2,
		 getLocation().getY()-dysize-2,
		 i.getWidth(generalPanel)+4,
		 i.getHeight(generalPanel)+4);

    }
  }

    public boolean isDoingSomething() {
	if(activityText.equals("Doing nothing") ||
	   activityText.startsWith("In idle mode") ||
	   activityText.startsWith("Improved") ||
	   activityText.startsWith("Just built"))
	    return false;

	if(getActivityType() == HostPlanet.NOTHING ||
	   getActivityType() == HostPlanet.WASTE_TIME)
	    return false;

	return true;
    }

  private void drawActivityText(ScaledGraphics g) {
    if(world.getController().playerNumber!=getOwner())
      return;

    if(!((PHWorld)world).isActivityTextDrawingEnabled())
      return;

    if(activityText==null)
      return;

    if(activityText.equals("Doing nothing") ||
       activityText.startsWith("In idle mode") ||
       activityText.startsWith("Improve Production"))
      return;

    if(activityText.startsWith("Improved") ||
       activityText.startsWith("Just built"))
      g.setColor(Color.green);
    else
      g.setColor(Color.red);

    int x     = getLocation().getX();
    int y     = getLocation().getY();
    int scale = g.getScaleFactor();

    g.setFont(textFont);
    g.drawString(activityText, x+35, y-25);
  }


  void setMaterials(MaterialBundle mb) {
    materials=mb;
  }

  void setProduction(MaterialBundle mb) {
    materialProduction=mb;
  }

  void setTechLevels(TechLevel t) {
    techLevels = t;
  }
  
  void setActivityType(int t) {
    activityType = t;
  }

  void setActivityText(String s) {
    activityText = s;
  }

  void setActivityDuration(int d) {
    activityDuration = d;
  }

  void setPlanetType(int t) {
    planetType=t;
  }

    void setCloneShipInfo(CloneShipInfo csi) {
	cloneShipInfo = csi;
    }

  public int getPlanetType() {
    return planetType;
  }

  public void startIdling() {
    WorldComponentMessage wcm = new StartIdlingWCM(getId());
    world.putComponentMessage(wcm);
    activityText="In idle mode";
    resetInfoText();
  }

  public void startShipCloning() {
    WorldComponentMessage wcm = new CloneShipWCM(getId());
    world.putComponentMessage(wcm);
    activityText="Cloning Ship";
    resetInfoText();
  }

    public void pointClicked(Location l) {
      Location atl = calculateAutotransferLocation(l);
      
      Enumeration e = world.getComponentsByLocation(atl);
      while (e.hasMoreElements()) {
	WorldComponent wc= (WorldComponent) e.nextElement();
	if(wc instanceof Planet) {
	  componentSelected(wc);
	}
      }
    }

  private Location calculateAutotransferLocation(Location l) {
    Location actualWaypoint = l;
    Enumeration e = world.getWorldComponents();
    WorldComponent wc;
    Location p;

    while(e.hasMoreElements()) {
      wc = (WorldComponent) e.nextElement();
      if(wc instanceof Planet) {
	p=wc.getLocation();
	if(Math.abs(p.getX()-l.getX()) <= 20 &&
	   Math.abs(p.getY()-l.getY()) <= 20) {
	  actualWaypoint=(Location) p.clone();
	  break;
	}
      }
    }

    return actualWaypoint;
  }

  public void componentSelected(WorldComponent wc) {
    if(!(wc instanceof Planet))
	return;

    if(wc == this) 
	setAndSendAutotransferTarget(-1);
    else {
	if(wc instanceof Planet &&
	   wc.isOwnedByMe() &&
	   !wc.getName().equals("Dodechaedron")) {
	    setAndSendAutotransferTarget(wc.getId());
	}
    }
  }

  public int getAutotransferEfectivity() {
      if(autotransferTarget >= 0) {
	  return HostTransferer.getEfectivityPercentage(
	     getLocation().distance(world.getComponentById(autotransferTarget).getLocation()));
      } else {
	  return 0;
      }
  }

  public int getAutotransferTarget() {
    return autotransferTarget;
  }

  String getAutotransferTargetName() {
    if(autotransferTarget >= 0) {
	 return world.getComponentById(autotransferTarget).getName();
    } else {
	 return null;
    }
  }

  void setAutotransferTarget(int target) {
    autotransferTarget = target;
  }

  public void setAndSendAutotransferTarget(int target) {
    WorldComponentMessage wcm = new AutotransferWCM(getId(), target);
    world.putComponentMessage(wcm);

    autotransferTarget = target;
    resetInfoText();
    world.repaintMap();
  }

  public void startProductionImproving() {
    WorldComponentMessage wcm = new ImproveProductionWCM(getId());
    world.putComponentMessage(wcm);
    if(activityText == null || !activityText.startsWith("Improve Production"))
      activityText=
	"Improve Production ("+HostPlanet.PRODUCTION_IMPROVEMENT_START+"%)";
    setActivityType(HostPlanet.IMPROVE_PRODUCTION);
    resetInfoText();
  }

  public void startImproveTech(int techType) {
    WorldComponentMessage wcm = new ImproveTechWCM(getId(), techType);
    world.putComponentMessage(wcm);
    activityText="Started to improve " + TechLevel.getTechName(techType);
    setActivityType(HostPlanet.IMPROVE_TECH);
    resetInfoText();
  }

  public void startBuildComponent(int techType, int level) {
    WorldComponentMessage wcm = new BuildComponentWCM(getId(), techType,
						      level);
    world.putComponentMessage(wcm);
    activityText="Started to build " + TechLevel.getNounTechName(techType) +
      " (" + level + ")";
    setActivityType(HostPlanet.BUILD_COMPONENT);
    resetInfoText();
  }
  
  public void startBuildShip(int hullid, Vector compos) {
    WorldComponentMessage wcm = new BuildShipWCM(getId(), hullid, compos);
    world.putComponentMessage(wcm);
    activityText="Started to build ship";
    setActivityType(HostPlanet.BUILD_SHIP);
    resetInfoText();
  }

  public void startRecycleShip(int shipId) {
    WorldComponentMessage wcm = new RecycleShipWCM(getId(), shipId);
    world.putComponentMessage(wcm);
    setActivityType(HostPlanet.RECYCLE_SHIP);
    activityText="Started to recycle ship";
  }

  public TechLevel getTechLevels() {
    return techLevels;
  }

  public int getActivityDuration() {
    return activityDuration;
  }

  public int getActivityType() {
    return activityType;
  }

  public MaterialBundle getMaterials() {
    return materials;
  }

  public MaterialBundle getProduction() {
    return materialProduction;
  }

    void setDefensePower(int power) {
	defensePower = power;
    }

    public int getDefensePower() {
	return defensePower;
    }

  void setEDTCircleRadius(int r) {
    this.edtCircleRadius=r;
  }

  int getEDTCircleRadius() {
    return this.edtCircleRadius;
  }

  void redrawEDT() {
    if(!((PHWorld)world).isEDTCircleDrawingEnabled())
      return;

    world.repaintMap();
  }

    public void setTroops(int t) {
	troops = t;
    }

    public int getTroops() {
	if(materials != null)
	    return materials.getTroops();
	else
	    return troops;
    }

    public MaterialBundle getTotalProduction() {
	MaterialBundle total = new MaterialBundle(MaterialBundle.UNLIMITED);

	Enumeration e = getWorld().getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Planet) {
		Planet planet = (Planet) wc;

		if(getOwner() == planet.getOwner() &&
		   planet.getAutotransferTarget() == getId()) {
		    MaterialBundle prod =
			(MaterialBundle) planet.getProduction().clone();

		    prod.takeFuel(prod.getFuel());
		    prod.takeTroops(prod.getTroops());
		    prod.takeDodechadrium(prod.getDodechadrium());

		    prod.scaleBundle(planet.getAutotransferEfectivity());
		    total.addFullBundle(prod);
		}
	    }
	}
	
	total.addFullBundle(getProduction());
	return total;
    }


}
