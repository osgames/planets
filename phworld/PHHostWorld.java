/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.util.*;
import java.util.*;


public class PHHostWorld extends HostWorld {

    private int planetsPerPlayer;
    private int planetDensity;
    private int planetRichness;
  
    private int worldXSize=0;
    private int worldYSize=0;

    private static final int SIZE_CONSTANT = 80000;

    private CollectStatistics collectStatistics;
    private PHNameDatabase phNameDatabase;

    /* State saving variables for savegame */
    private int savegameTransferableComponentCounter;

    public PHHostWorld(HostController hc, 
		       HostScreen hw, 
		       WorldOptions wo) {
	super(hc,hw,wo);
	this.worldOptions=worldOptions;
	PHWorldOptions phWorldOptions = (PHWorldOptions) worldOptions;

	planetsPerPlayer=phWorldOptions.getPlanetsPerPlayer();
	planetDensity=phWorldOptions.getPlanetDensity();
	planetRichness=phWorldOptions.getRichness();

	phNameDatabase  = new PHNameDatabase();
	ShipHull.nameDatabase = phNameDatabase;

	if(phWorldOptions.isRandomWorld()) {
	    placePlanets(planetsPerPlayer);
	    allocatePlanets(phWorldOptions.
			    getPlanetAllocationPercent());
	}
	else {
	    loadMap(((PHWorldOptions) (worldOptions)).getMapFilename());
	}

	placeWormHoles();

	collectStatistics = new CollectStatistics(this, "TEST.stats");//XXX
    }

    protected PHNameDatabase getNameDatabase() {
	return phNameDatabase;
    }

    private void placeWormHoles() {
	HostWormhole hw1 = new HostWormhole(this);
	HostWormhole hw2 = new HostWormhole(this);
	HostWormhole hw3 = new HostWormhole(this);

	putComponent(hw1);
	putComponent(hw2);
	putComponent(hw3);

	hw1.setPairWormhole(hw2);
	hw2.setPairWormhole(hw3);
	hw3.setPairWormhole(hw1);

	hw1.setName("Wormhole-"+hw1.getId());
	hw2.setName("Wormhole-"+hw2.getId());
	hw3.setName("Wormhole-"+hw3.getId());

	PHWorldOptions options = (PHWorldOptions) this.worldOptions;
	double tharganActivity = ((double) options.getTharganActivity())/100.0;
	hw1.setTharganActivity(tharganActivity);
	hw2.setTharganActivity(tharganActivity);
	hw3.setTharganActivity(tharganActivity);
    }

    protected void initializeWorldStates() {
	worldState=new WorldState[GlobalDefines.maxPlayers];

	for(int i=0; i<GlobalDefines.maxPlayers; i++) {
	    worldState[i] = new PHWorldState();
	    ((PHWorldState) worldState[i]).
		putCredits(((PHWorldOptions) worldOptions).
			   getStartingCredits());
	}
    }

    public HostPlanet findNearestPlanet(Location l) {
	int shortest=9999999;
	HostPlanet hp = null;
    
	Enumeration e = getComponents();
    
	while (e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();

	    if(hwc instanceof HostPlanet) {
		int dis = hwc.getLocation().distance(l);
		if(dis<shortest) {
		    shortest=dis;
		    hp=(HostPlanet) hwc;
		}
	    }
	}

	return hp;
    }

    /**
     * Determines a random location for a planet in a slice of
     * the space. The slice is centered to the Dodechaedron and its middle
     * point is the player's home world.
     *
     * @param player The player number for which to generate a location
     * @param numberOfPlayers Total number of players in the game
     * @return Location Location for a planet
     */

    private Location getNewRandomLocation(int player, int numberOfPlayers) {
	int xc, yc;

	double angleSlice=2.0*Math.PI/((double)numberOfPlayers);
	double baseAngle=angleSlice*((double) player);
	double angle=baseAngle + Math.random()*angleSlice - angleSlice/2.0;
	
	double mapRadius=((double)getXSize())/2.0-50.0;
	double radius=Math.pow(Math.random(), 0.5)*mapRadius;
	
	xc = getXSize()/2 + (int) (Math.cos(angle)*radius);
	yc = getYSize()/2 + (int) (Math.sin(angle)*radius);

	return new Location(xc,yc);
    }

    private Location getNewLocation(int player, int numberOfPlayers) {

	Location test=null;
	Location temp;
	Enumeration e;
	boolean ok;

	do {
	    ok=true;
	    test=getNewRandomLocation(player, numberOfPlayers);
	    e=getComponents();
	    while(e.hasMoreElements()) {
		temp=((HostWorldComponent) (e.nextElement())).getLocation();
		if(test.distance(temp)<80) {
		    ok=false;
		    break;
		}
	    }

	} while(!ok);

	return test;
    }

    private void placeDodechaedron(int x, int y) {
	HostPlanet hp;
	MaterialBundle initial = 
	    PlanetType.getInitialMaterials(PlanetType.DODECHAEDRON);
	MaterialBundle prod = 
	    PlanetType.getProductionMaterials(PlanetType.DODECHAEDRON);

	hp=new HostPlanet(this, -1, new Location(x,y), prod, initial,
			  PlanetType.DODECHAEDRON);
	hp.setName("Dodechaedron");
	hp.setOwnershipVisiblity(true);
	putComponent(hp);
    }

    private void placeHomePlanet(int x, int y, int player, String name) {
	HostPlanet hp = PlanetType.getHomePlanet(this, player, new Location(x,y));
	hp.setName(name);
	putComponent(hp);    
    }

    private HostPlanet findHomePlanet(int player) {
	Enumeration e = getComponents();

	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();

	    if(hwc instanceof HostPlanet ) {
		HostPlanet hp = (HostPlanet) hwc;
		if(hp.getOwner() == player && 
		   hp.getPlanetType() == PlanetType.HOME) {
		    return hp;
		}
	    }
	}

	return null;
    }

    private HostPlanet findNearestFreePlanet(Location l) {
	HostPlanet nearestFree = null;
	int nearestDistance = Integer.MAX_VALUE;
	Enumeration e = getComponents();

	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if(hwc instanceof HostPlanet) {
		HostPlanet hp = (HostPlanet) hwc;
		if(hp.getOwner() == -1 &&
		   hp.getPlanetType() != PlanetType.DODECHAEDRON) {
		    int d = hp.getLocation().distance(l);
		    if(d < nearestDistance) {
			nearestDistance = d;
			nearestFree     = hp;
		    }
		}
	    }
	}

	return nearestFree;
    }

    private void allocatePlanets(int allocationPercentage) {
	int allocationAmount = (planetsPerPlayer-1) 
	    * allocationPercentage / 100;

	for(int p=0; p<allocationAmount; p++) {
	    Enumeration e = getAllPlayers();
	    while(e.hasMoreElements()) {
		int n=((Integer) e.nextElement()).intValue();
		
		HostPlanet home = findHomePlanet(n);
		if(home == null) {
		    System.err.println("Internal error: can't find home planet");
		    break;
		}
		
		HostPlanet hp = 
		    findNearestFreePlanet(home.getLocation());
		if(hp != null) {
		    hp.setName(phNameDatabase.getNewPlanetName(n));
		    hp.setOwner(n);
		}
	    }
	}
    }

    private void placePlanets(int planetsPerPlayer) {
        long worldArea = (((long)100)* 
			    ((long)SIZE_CONSTANT)*(((long)planetsPerPlayer) *
						   ((long) getNumberOfPlayers())+ ((long)1)))/
	  ((long) (planetDensity+100));

	worldXSize= ((int) Math.sqrt(worldArea));
	worldYSize= worldXSize;

	int xs = getXSize();
	int ys = getYSize();
    
	HostPlanet hp;
	Enumeration e=getAllPlayers();
	int numberOfPlayers=getNumberOfPlayers();
	int homeWorldRadius=(xs+ys)/6;

	double angle=0;
	double angleAdd=2.0*Math.PI/numberOfPlayers;

	placeDodechaedron(xs/2, ys/2);

	while(e.hasMoreElements()) {
	    int n=((Integer) e.nextElement()).intValue();
	    angle+=angleAdd;

	    placeHomePlanet((int) (Math.cos(angle)*homeWorldRadius+xs/2),   
			    (int) (Math.sin(angle)*homeWorldRadius+ys/2), n, 
			    phNameDatabase.getNewPlanetName(n));

	}

	planetsPerPlayer--;

	for(int p=0; p<planetsPerPlayer; p++) 
	    for(int n=0; n<numberOfPlayers; n++) {
		hp=new HostPlanet(this,-1, getNewLocation(n, numberOfPlayers), 
				  PlanetType.getRandomType());
		hp.setName(phNameDatabase.getNewPlanetName(-1));
		hp.scaleRichness(planetRichness);
		putComponent(hp);
	    }
	
    }

    private void loadMap(String mapFile) {
	MapLoader mapLoader = new MapLoader(this, mapFile, getNumberOfPlayers());
	if(!mapLoader.isMapValid()) {
	    System.err.println("Unable to load map. Giving up.\n");
	    System.exit(1);
	}

	worldXSize=mapLoader.getMapXSize();
	worldYSize=mapLoader.getMapYSize();

	Enumeration e = mapLoader.getComponents();

	while (e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    putComponent(hwc);
	}
    }
  
    public int getXSize() {
	return worldXSize;
    }

    public int getYSize() {
	return worldYSize;
    }
  
    private void dragShips(Location oldLocation, Location newLocation) {
	Enumeration e = getComponentsByLocation(oldLocation);

	while (e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();

	    if(hwc instanceof HostShip) {
		((HostShip) hwc).moveToLocation(newLocation);
	    } if(hwc instanceof HostFleet) {
		((HostFleet) hwc).moveToLocation(newLocation);
	    }
	}
    }

    protected void rotatePlanet(HostPlanet hp) {
	PHWorldOptions phwo = (PHWorldOptions) worldOptions;
	int orbitalVelocity = phwo.getOrbitalVelocity();

	if(orbitalVelocity > 0) {
	    orbitalVelocity *= hp.getBaseOrbitalVelocity() * getCurrentRound();
	    Location center  = new Location(getXSize()/2, getYSize()/2);
	    Location origL   = hp.getOriginalLocation();
	    Location oldL    = hp.getLocation();
	    Location newL    = origL.rotateAroundCenter(center, 
							orbitalVelocity);

	    hp.setLocation(newL);
	    dragShips(oldL, newL);
	}
    }

    protected void runPlanetRounds() {
	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostPlanet) {
		hwc.doRound();
		rotatePlanet((HostPlanet) hwc);
	    }
	}
    }

    protected void runFleetRounds() {
	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostFleet)
		hwc.doRound();
	}
    }

    private MovingObject findWorstETA(Vector movers) {
	MovingObject worst = null;
	int worstETA = -1;

	Enumeration e = movers.elements();

	while (e.hasMoreElements()) {
	    MovingObject mo = (MovingObject) e.nextElement();
	    int eta = mo.getETA();

	    if(eta > worstETA) {
		worstETA = eta;
		worst    = mo;
	    }
	}

	return worst;
    }

    private void moveObjects(Vector movers) {
	Vector remainingMovers = new Vector();
	boolean someoneMoved   = false;

	//System.err.println("moveObjects: " + movers.size() + " movers.");

	if(movers.size() == 0)
	    return;

	for(int i=0 ; i<movers.size() ; i++) {
	    MovingObject mo = (MovingObject) movers.elementAt(i);
	    HostWorldComponent trackTarget =
		mo.getEffectiveTrackTarget();
	    
	    if(trackTarget != null &&
	       trackTarget instanceof HostShip) {
		HostShip tt = (HostShip) trackTarget;
		HostFleet fleet = tt.getFleet();

		if(fleet != null)
		    trackTarget = fleet;
	    }

	    if(trackTarget == null || movers.indexOf(trackTarget) == -1) {
		mo.doMovement();
		//System.err.println("Moving one object");
		someoneMoved = true;
	    } else {
		remainingMovers.addElement(mo);
	    }
	}

	if(someoneMoved)
	    moveObjects(remainingMovers);
	else {
	    MovingObject mo = findWorstETA(remainingMovers);
	    mo.doMovement();
	    //System.err.println("Moved by worst ETA");
	    remainingMovers.removeElement(mo);
	    moveObjects(remainingMovers);
	}
    }

    protected void runShipRounds() {

	// Run normal ship rounds
	Enumeration e = getComponents();
	Vector movers = new Vector();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostShip) {
		hwc.doRound();
	    }

	    if(hwc instanceof MovingObject) {
		if(hwc instanceof HostShip &&
		   ((HostShip) hwc).isInFleet()) {
		    // nothing
		} else {
		    movers.addElement((MovingObject) hwc);
		}
	    }
	}

	moveObjects(movers);
	// Check for surrenders and superbombs

	e = getComponents();
	while (e.hasMoreElements()) {
	    Object hs = e.nextElement();
	    if(hs instanceof HostShip) {
		((HostShip) hs).checkSurrender();
		((HostShip) hs).checkSuperBombs();
	    }
	}
    }

    protected void runTransferEntitys() {

	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostTransferer) {
		hwc.doRound();
	    }
	}
    }

    protected void runMiscEntitys() {

	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostMiscComponent) {
		hwc.doRound();
	    }
	}
    }

    protected void runWormholes() {

	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostWormhole) {
		hwc.doRound();
	    }
	}
    }


    protected void runTharganRounds() {

	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostShip &&
	       hwc.getOwner() == GlobalDefines.maxPlayers-1) {
		tharganShip((HostShip) hwc);
	    }
	}
    }
    
    protected void tharganShip(HostShip ship) {
	Enumeration e = getComponents();
	HostWorldComponent hwc;
	HostPlanet best = null;
	int dist = 100000;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostPlanet &&
	       hwc.getOwner() != GlobalDefines.maxPlayers-1) {
		int d = (int) ((double) hwc.getLocation().distance(ship.getLocation()) * ((HostPlanet) hwc).getThargonDistanceMultiplier());
		if(d<dist) {
		    best = (HostPlanet) hwc;
		    dist = d;
		}
	    }
	}

	if(best != null) {
	    ship.setWaypoint(best.getLocation());
	}
    }


    protected void runComponentRounds() {
	runPlanetRounds();
	runFleetRounds();
	runTharganRounds();
	runShipRounds();
	runTransferEntitys();
	runMiscEntitys();
	runWormholes();
    }

    protected void checkPlanetOwners() {
	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostPlanet)
		((HostPlanet) hwc).ownershipCheck();
	}
    }

    protected void runBombardments() {
	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostShip)
		((HostShip) hwc).runOrbitalBombard();
	}
    }

  
    public int getTotalNumberOfCriticalPlanets() {
	Enumeration e = getComponents();
	int maxnro=0;

	while(e.hasMoreElements()) {
	    HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	    if(hwc instanceof HostPlanet) {
		HostPlanet hp = (HostPlanet) hwc;
		if(hp.getOwnershipVisibility()) {
		    maxnro++;
		}
	    }
	}

	return maxnro;
    }

    private void resetMaterialStatistics() {
	for(int i=0; i<GlobalDefines.maxPlayers; i++) {
	    Integer ii = new Integer(i);
	    getPHWorldState(i).resetResourceUsage();
	}
    }

    void addToMaterialUsageStatistics(int player, MaterialBundle mat) {
	getPHWorldState(player).addMaterialUsage(mat);
    }

    void addCredits(int player, int amount) {
	getPHWorldState(player).putCredits(amount);
    }

    boolean takeCredits(int player, int amount) {
	return getPHWorldState(player).takeCredits(amount);
    }

    int getCredits(int player) {
	return getPHWorldState(player).getCredits();
    }

    private PHWorldState getPHWorldState(int player) {
	return (PHWorldState) worldState[player];
    }

    private void setBattleReports(Hashtable battleOutcomes) {

	for(int i=0; i<GlobalDefines.maxPlayers; i++) {
	    Integer ii = new Integer(i);
      
	    BattleOutcomes bo = (BattleOutcomes) battleOutcomes.get(ii);
	    getPHWorldState(i).setBattleOutcomes(bo);
	}
    }

    protected void handleWorldStateMessage(WorldStateMessage wsm) {
	
    }

    public void runWorldRound() {
	resetMaterialStatistics();

	runComponentRounds();

	ShipBattle sb = new ShipBattle(this);
	Hashtable battleOutcomes = sb.shipBattle();
	setBattleReports(battleOutcomes);

	runBombardments();
	checkPlanetOwners();
    }

    public void runWorldPostRound() {
	collectStatistics.newRoundToStatistics();
    }

    protected void prepareForSaveGame() {
	savegameTransferableComponentCounter = 
	    TransferableComponent.getCurrentSubcomponentID();
    }

    protected void restoreAfterLoadGame() {
	ShipHull.nameDatabase = phNameDatabase;
	TransferableComponent.setCurrentSubcomponentID(savegameTransferableComponentCounter);
    }

    protected boolean isPlayerAlive(int player) {
	Enumeration e = getComponents();
	HostWorldComponent hwc;

	while(e.hasMoreElements()) {
	    hwc=(HostWorldComponent) e.nextElement();
      
	    if(hwc instanceof HostPlanet) {
		if(hwc.getOwner() == player)
		    return true;
	    }
	}
	
	return false;
    }
}
