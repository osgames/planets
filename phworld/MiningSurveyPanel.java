/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import dsc.awt.*;
import dsc.netgame.*;

/**
 * Mining Survey display for Plantes.
 *
 *@author Dodekaedron Software Creations, Inc. -- Wraith
 */

class MiningSurveyPanel extends PlanetSidePanel {

  TextArea infoText;

  MiningSurveyPanel() {
    super("data/graph/still/panel/planet.mining.gif");

    contentsPanel.setLayout(new VerticalFlowLayout());

    infoText=new TextArea("",20,22,TextArea.SCROLLBARS_NONE);
    infoText.setEditable(false);
    infoText.setText("---");

    contentsPanel.add(infoText);
  }

  void initializeContents(Planet p) {
    infoText.setText("Production:\n\n" + p.getProduction() + "\n\n");
    infoText.append("Prod. + import:\n\n" + p.getTotalProduction() + "\n");
  }

  boolean OKAction() {    
    return true;
  }

  void initiateHelp() {
    HelpWindow.callForHelp("data" + java.io.File.separator + "help",
			   "Playing",
			   "Mining Survey");
  }

  public void initiateTechGallery() {
      TechGallery.showTechGallery();      
      return;
  }
}
