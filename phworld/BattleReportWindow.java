/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import dsc.awt.*;
import dsc.netgame.*;


public class BattleReportWindow extends LayoutableFrame implements ItemListener, ActionListener {

  private Button previousButton,nextButton,closeButton,helpButton;
  private Choice battleChoice;
  private ScrollPane pane;
  private Panel panel;

  private Enumeration battleReports;
  private World world;
  private Vector reports; 

  private Frame targetFrame;

  // Custom mutex to avoid conflicts with AWT synchronized methods 
  private Integer mutex = new Integer(0);  

  public BattleReportWindow(BattleOutcomes bo, World world) {
    

    setTitle("Battle Reports");
    setSize(320,450);

    targetFrame=this;
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) { targetFrame.setVisible(false); } 
    });


    this.world=world;

    previousButton=createButton("<<<<", this);
    nextButton=createButton(">>>>", this);
    closeButton=createButton("Close", this);
    helpButton=createButton("Help",this);

    pane = new ScrollPane();
    panel = new Panel();
    GridBagLayout gblpanel = new GridBagLayout();
    panel.setLayout(gblpanel);
    pane.add(panel);

    battleChoice = new Choice();
    battleChoice.addItemListener(this);

    
    GridBagLayout gbl = new GridBagLayout();
    setLayout(gbl);


    
    gbAdd(battleChoice,100,0,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
	  0,0,8,1);
    gbAdd(previousButton,100,0,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
	  9,0,3,1);
    gbAdd(nextButton,100,0,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
	  12,0,3,1);
    gbAdd(helpButton,100,0,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
	  9,20,3,1);
    gbAdd(closeButton,100,0,GridBagConstraints.WEST,GridBagConstraints.HORIZONTAL,
	  12,20,3,1);
    gbAdd(pane,100,100,GridBagConstraints.WEST,GridBagConstraints.BOTH,
	  0,1,16,19);

    initialize(bo);
    
  }


  public void actionPerformed(ActionEvent evt) {
      synchronized (mutex) {
	  String arg = evt.getActionCommand();

	  if(arg.equals("Close")) {
	      this.setVisible(false);
	  } else if(arg.equals("Help")) {
	      HelpWindow.callForHelp("data" + java.io.File.separator + "help",
				     "Playing",
				     "Battle Report");
	  } else if(arg.equals("<<<<")) {
	      int n=battleChoice.getSelectedIndex();
	      if(n>0) {
		  battleChoice.select(n-1);
		  updateReportPanel();
	      }
	  } else if(arg.equals(">>>>")) {
	      int n=battleChoice.getSelectedIndex();
	      if(n<(battleChoice.getItemCount()-1)) {
		  battleChoice.select(n+1);
		  updateReportPanel();
	      }
	  }
      }
  }

  public void itemStateChanged(ItemEvent evt) {
      synchronized (mutex) {
	  if(evt.getStateChange()==ItemEvent.DESELECTED) return;

	  updateReportPanel();
      }
  }

    private void updateReportPanel() {
	if (battleChoice.getSelectedItem().equals("N/A")!=true)  
	    updateReportPanel((BattleReport) reports.
			      elementAt(battleChoice.getSelectedIndex()));
    }

  private void updateReportPanel(BattleReport br) {
    Vector participants;
    
    panel.removeAll();
    GridBagConstraints gbc = new GridBagConstraints();


    participants = br.getParticipants();
    for(int i=0;i<participants.size();i++) {
      Label nameLabel = 
	new Label(getPlayerName(((Integer) participants.
				 elementAt(i)).intValue()));
      Label shipsLabel = 
	shipsDestroyed(((Integer)participants.elementAt(i)).intValue(),br);


      panel.add(nameLabel,
		gbcs(0,0,GridBagConstraints.NORTHWEST,GridBagConstraints.HORIZONTAL,
		0,i,6,1));
      
      panel.add(shipsLabel, 
		gbcs(0,0,GridBagConstraints.NORTHWEST,GridBagConstraints.HORIZONTAL,
		     6,i,4,1));
      
    }

    panel.add(new Label(" "),
	      gbcs(100,100,GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
		   0,br.getParticipants().size(),10,2));

    int yPosition=updateShipInformation(panel, br);

    panel.add(new Label(" "),
	      gbcs(100,100,GridBagConstraints.SOUTH, GridBagConstraints.BOTH,
		   0,yPosition,10,1));

    validate();
  }

  private Label shipsDestroyed(int player,BattleReport br) {
    Label l;

    int ship = 0;
    int alive = 0;

    Vector attacks = br.getAttacks(player);
    Vector ships = br.getShips();

    for (int i=0;i<ships.size();i++) {
      if (((ShipReport)ships.elementAt(i)).getOwner() == player) {
	ship++;
	if (((ShipReport)ships.elementAt(i)).isAlive()) alive++;
      }
    }

    if (alive == 0) {
      l = new Label("Destroyed 0/"+ship);
      l.setForeground(Color.red);
    }
    else {
      if (attacks.isEmpty()) { 
	l = new Label("Observed "+alive+"/"+ship);
	l.setForeground(Color.yellow);
      }
      
      else {
	l = new Label("Survived "+alive+"/"+ship);
	l.setForeground(Color.green);
      }
    }
    
    return l;
  }

  private int updateShipInformation(Panel panel, BattleReport br) {
    Vector participants;
    participants = br.getParticipants();
    int yPosition=participants.size()+2;

    for(int i=0;i<participants.size();i++) {
      int player = ((Integer) participants.elementAt(i)).intValue();

      insertShipOwnerLabel(panel, br, player, yPosition);
      yPosition++;

      Vector ships = br.getShips();
      Enumeration e = ships.elements();

      while (e.hasMoreElements()) {
	ShipReport sr = (ShipReport) e.nextElement();

	if(sr.getOwner() == player) {
	  panel.add(getShipImageComponent(sr.getTech()),
		    gbcs(0,0,GridBagConstraints.WEST,GridBagConstraints.NONE,
			 0, yPosition, 2, 2));

	  Label nameLabel = new Label(sr.getName() + " - H: " +
				      sr.getHullHpLeft() + "/" +
				      sr.getHullMaxHp() + " ("+
				      ((sr.getHullHpLeft()*100)/
				      sr.getHullMaxHp()) +
				      "%) S: " +
				      sr.getShielsLeft());
	  nameLabel.setForeground(Color.green);
	  if(sr.isDamaged())
	    nameLabel.setForeground(Color.yellow);
	  if(!sr.isAlive())
	    nameLabel.setForeground(Color.red);

	  panel.add(nameLabel,
		    gbcs(0,0,GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
			 2, yPosition, 8, 1));

	  Label attackLabel = new Label("Didn't attack");
	  if(sr.getHullAttack()>0 || sr.getShieldAttack()>0) {
	    attackLabel.setText("H "+sr.getHullAttack() + " / " +
				"S "+sr.getShieldAttack());
	  }

	  panel.add(attackLabel,
		    gbcs(0,0,GridBagConstraints.WEST, GridBagConstraints.NONE,
			 2, yPosition+1, 4, 1));


	  Label damageLabel = new Label("S "+sr.getShieldDamage()+" / "+
					"H "+sr.getHullDamage()+" / "+
					"C "+sr.getComponentDamage());
	 
	  panel.add(damageLabel,
		    gbcs(0,0,GridBagConstraints.WEST, GridBagConstraints.NONE,
			 6, yPosition+1, 4, 1));


	  yPosition+=2;
	} 
      }
    }

    return yPosition;
  }

  private void insertShipOwnerLabel(Panel panel, 
				    BattleReport br, 
				    int player, 
				    int yPosition) {
    
    Label starLabel=new Label("*");
    Label nameLabel=new Label(getPlayerName(player));

    starLabel.setForeground(GlobalDefines.getSecondaryColor(player));
    //starLabel.setBackground(Color.black);
    nameLabel.setForeground(GlobalDefines.getPrimaryColor(player));
    //nameLabel.setBackground(Color.black);
        
    String attackText = "attacked ";

    Vector attacks = br.getAttacks(player);
    if(attacks.size()==0) {
      attackText+="#no-one#";
    } else {
      for (int i = 0; i <attacks.size() ; i++) {
	String name = 
	  getPlayerName(((Integer) attacks.elementAt(i)).intValue());

	attackText+=name + " ";
      }
    }

    Label attackLabel=new Label(attackText);

    panel.add(starLabel,
	      gbcs(0,0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
		   0, yPosition, 1, 1));

    panel.add(nameLabel,
	      gbcs(0,0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
		   1, yPosition, 3, 1));

    panel.add(attackLabel,
	      gbcs(0,0,GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL,
		   4, yPosition, 6, 1));
  }


  public GridBagConstraints gbcs(int gbcWeightx, int gbcWeighty, int anchor, int fill, int x, int y, int w, int h) {

    GridBagConstraints gbc = new GridBagConstraints();

    gbc.weightx=gbcWeightx;
    gbc.weighty=gbcWeighty;
    gbc.anchor=anchor;
    gbc.fill=fill;
    gbc.gridx=x;
    gbc.gridy=y;
    gbc.gridwidth=w;
    gbc.gridheight=h;
  
   return gbc; 
  }

  public void initialize(BattleOutcomes bo) {
      synchronized (mutex) {
	  battleReports = bo.getReports();
	  reports = new Vector();
	  battleChoice.removeAll();

	  if (battleReports.hasMoreElements()==false) { 
	      battleChoice.add("N/A");
	      panel.setEnabled(false);
	  } else {
	      panel.setEnabled(true);
	      int temp = 0;
	      while (battleReports.hasMoreElements()) {

		  BattleReport battleReport = (BattleReport) battleReports.nextElement();

		  battleChoice.insert(battleReport.getName(),temp);
		  reports.insertElementAt(battleReport,temp);

		  temp++;
	      }

	  }
	  battleChoice.select(0);

	  if (battleChoice.getSelectedItem().equals("N/A")!=true)  
	      updateReportPanel((BattleReport) reports.
				elementAt(battleChoice.getSelectedIndex()));

      }    
    return;
  }

  private ImageComponent getShipImageComponent(int tech) {
    return new ImageComponent(world.
			      getImage(ShipHull.getMapImageName(tech)));
  }


  private String getPlayerName(int playerNumber) {
    String name = 
      world.getController().playersInfo.getPlayerName(playerNumber);
    
    return name;
  }
} 

