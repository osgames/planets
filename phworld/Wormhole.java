/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;


public class Wormhole extends WorldComponent {
    private static Panel controlPanel = null;  
    private static GridBagConstraints controlPanelConstraints = null;
    private static Panel generalPanel = null;
    private static Label nameLabel = null;
    private static TextArea infoText = null;

    private int bearing;

    public Wormhole (int id,
		     String name, 
		     Location location ) {
	super(id, name, -1, location);
    }

    protected void changeControlPanel(Panel newPanel) {
	if(controlPanelConstraints == null) {
	    controlPanelConstraints = new GridBagConstraints();

	    controlPanelConstraints.fill=GridBagConstraints.BOTH;
	    controlPanelConstraints.anchor=GridBagConstraints.NORTH;
	    controlPanelConstraints.weightx=100;
	    controlPanelConstraints.weighty=1;
	    controlPanelConstraints.gridwidth=1; 
	    controlPanelConstraints.gridheight=1;
	    controlPanelConstraints.gridx=0;
	    controlPanelConstraints.gridy=0;
	}

	controlPanel.removeAll();
	controlPanel.add(newPanel, controlPanelConstraints);
	controlPanel.validate();
    }

    void createControlPanel() {
	controlPanel=new Panel();
	controlPanel.setLayout(new GridBagLayout());

	generalPanel=new Panel();
	changeControlPanel(generalPanel);

	generalPanel.setLayout(new VerticalFlowLayout());
	ImageComponent imageComponent = new ImageComponent(
						world.getImage(getPanelImageName()));
	generalPanel.add(imageComponent);
    
	nameLabel=new Label("Wormhole");
	generalPanel.add(nameLabel);

	infoText=new TextArea("",15,22,TextArea.SCROLLBARS_NONE);
	infoText.setEditable(false);

	generalPanel.add(infoText);
    }


    public Panel getControlPanel() {
	if(controlPanel==null) {
	    createControlPanel();
	}
    
	return controlPanel;
    }

  
    public String getMapImage() {
	return "data/graph/still/components/world/wormhole.gif";
    }

    public String getPanelImageName() {
	return "data/graph/still/panel/wormhole.gif";
    }

    public boolean isVisible() {
	return true;
    }

    public int getBearing() {
	return bearing;
    }

    void setBearing(int b) {
	bearing = b;
    }
}
