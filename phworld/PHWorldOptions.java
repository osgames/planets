/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2002 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;
import dsc.util.*;

public class PHWorldOptions extends WorldOptions {
    /* round settings */
    private int firstRoundLength;
    private int roundLength;
    private double roundIncrement;
    private int maxRoundLength;
    boolean waitOfflinePlayers = true;

    /* world settings */

    private int creditMultiplier = 100; /* percents */
    private int startingCredits = 10000;   

    private String mapName = null;
    private String mapFilename = null;
    private int planetsPerPlayer;
    private int materialRichness;
    private int mapDensity;
    private int mapRotation;
    private int mapPreAllocation;
    private int tharganActivity;
  
    /* scoring options*/

    private int turnLimit  = 250;
    private int scoreLimit = 0;
    private int staticDodeScore = 0;
    private int staticHomeScore = 0;
    private int staticOtherScore = 0;
    private int staticSpacePortScore = 0;
    private int cumDodeScore = 5;
    private int cumHomeScore = 0;
    private int cumOtherScore = 1;
    private int cumSpacePortScore = 3;

    public PHWorldOptions(CmdLine cmdLine) {
	firstRoundLength = cmdLine.getIntOption("-Cfirstrnd", 
						300,
						10,
						31536000);

	roundLength = cmdLine.getIntOption("-Crndtime",
					   35,
					   10,
					   31536000);
	
	roundIncrement = cmdLine.getDoubleOption("-Ctimeincrement",
						 0.05,
						 0.0,
						 1000000.0);

	maxRoundLength = cmdLine.getIntOption("-Cmaxrndtime",
					      45,
					      10,
					      31536000);


	mapName = cmdLine.getStringOption("-Cmap", null);

	if(mapName != null) {
	    Hashtable maps = MapLoader.getLoadableMaps();
	    if(maps.get(mapName) != null) {
		mapFilename = (String) maps.get(mapName);
	    } else {
		System.err.println("Map not found, using random world");
		mapName = null;
	    }
	}
    
	planetsPerPlayer = cmdLine.getIntOption("-Cppl",
						12,
						4,
						250);

	
	materialRichness = cmdLine.getIntOption("-Crichness",
						   100,
						   25,
						   250);


	mapDensity = cmdLine.getIntOption("-Cdensity",
					  100,
					  50,
					  200);


	mapRotation = cmdLine.getIntOption("-Covelocity",
					   0,
					   0,
					   40);

	mapPreAllocation = cmdLine.getIntOption("-Cprealloc",
						0,
						0,
						100);

	tharganActivity  = cmdLine.getIntOption("-Cthargon",
						100,
						0,
						5000);
    }

    public boolean isRandomWorld() {
	return (mapName==null);
    }

    public boolean isCustomWorld() {
	return (!isRandomWorld());
    }
    
    public String getMapName() {
	return mapName;
    }

    public String getMapFilename() {
	return mapFilename;
    }

    void setMapFilename(String mapName, String mapFilename) {
	this.mapName = mapName;
	this.mapFilename = mapFilename;
    };

    public int getPlanetsPerPlayer() {
	return planetsPerPlayer;
    }

    void setPlanetsPerPlayer(int n) {
	if(n<1 || n>1000) {
	    System.err.println("Number of planets per player out of range, defaulting to 12");
	    planetsPerPlayer = 12;
	} else {
	    planetsPerPlayer = n;
	}
    }

    public int getRichness() {
	return materialRichness;
    }

    void setRichness(int n) {
	if(n<5 || n>1000) {
	    System.err.println("Material richness percentage out of range, defaulting to 100");
	    materialRichness = 100;
	} else {
	    materialRichness = n;
	}
    }

    public int getOrbitalVelocity() {
	if(isCustomWorld()) {
	    return 0;
	}

	return mapRotation;
    }

    void setOrbitalVelocity(int n) {
	if(n<0 || n>50) {
	    System.err.println("Map velocity out of range, defaulting to 0");
	    mapRotation = 0;
	} else {
	    mapRotation = n;
	}
    }

    public int getPlanetAllocationPercent() {
	return mapPreAllocation;
    }

    void setPlanetAllocationPercent(int n) {
	if(n<0 || n>100) {
	    System.err.println("Preallocation out of range, defaulting to 0");
	    mapPreAllocation = 0;
	} else {
	    mapPreAllocation = n;
	}
    }

    public int getPlanetDensity() {
	return mapDensity;
    }

    void setPlanetDensity(int n) {
	if(n<50 || n>200) {
	    System.err.println("Map density out of range, using 100%");
	    mapDensity = 100;
	} else {
	    mapDensity = n;
	}
    }

    public int getTharganActivity() {
	return tharganActivity;
    }

    void setTharganActivity(int n) {
	if(n<0 || n>5000) {
	    System.err.println("Thargan activity out of range, using 100%");
	    tharganActivity = 100;
	} else {
	    tharganActivity = n;
	}
    }

    int getFirstRoundTime() {
	return firstRoundLength;
    }

    void setFirstRoundTime(int n) {
	firstRoundLength = n;
    }

    int getBaseRoundTime() {
	return roundLength;
    }

    void setBaseRoundTime(int n) {
	roundLength = n;
    }

    double getRoundTimeIncrement() {
	return roundIncrement;
    }

    void setRoundTimeIncrement(double i) {
	roundIncrement = i;
    }

    int getMaxRoundTime() {
	return maxRoundLength;
    }

    void setMaxRoundTime(int n) {
	maxRoundLength = n;
    }

    public int getRoundTime(int round) {
	int time;

	if(round<=1) {
	    time = getFirstRoundTime();
	}
	else {
	    time = getBaseRoundTime() + 
		(int) ((double) round * getRoundTimeIncrement());

	    int max = getMaxRoundTime();
	    if (time>max) time = max;
	}

	return (time*1000);
    }

    public int getCreditMultiplier() {
	return creditMultiplier;
    }

    public int getStartingCredits() {
	return startingCredits;
    }

    public String getSaveGameSuffix() {
	return ".phs";
    }

    public boolean endTurnWhenAway() {
	return !waitOfflinePlayers;
    }

    void setEndTurnWhenAway(boolean endWhenAway) {
	waitOfflinePlayers = !endWhenAway;
    }

    public String toString() {
	String s;

	if(isRandomWorld()) {
	    s = "Random world with " + getPlanetsPerPlayer() + 
		" planets/player\n";
	    s+= "Density: " + getPlanetDensity() + "%\n";
	    s+= "Orbital velocity: " + getOrbitalVelocity() + "\n";
	    s+= "Richness: " + getRichness() + "%\n";
	    s+= "Planet preallocation: " 
		+ getPlanetAllocationPercent() + "%\n";

	} else {
	    s = "Using map: " + getMapName() + "\n";
	}
	s+= "Thargon activity: " + getTharganActivity() + "%\n";
	s+= "\n";

	s+="First round time: "+ getFirstRoundTime() +"s\n";
	s+="Rest of the rounds: "+getBaseRoundTime()+"s + "
	    +getRoundTimeIncrement()
	    +"s / rnd\n";
	s+="with maximum of "+getMaxRoundTime()+"s / rnd\n";

	if(endTurnWhenAway()) {
	    s+= "Offline players are ignored\n";
	} else {
	    s+= "Host will wait for offline players\n";
	}

	s+="\n";
	s+="Scoring:\n";
	s+="Static scores:\n";
	s+="  Dodechaedron  " + getStaticDodeScore() + "\n";
	s+="  Home Worlds   " + getStaticHomeScore() + "\n";
	s+="  Other Planets " + getStaticOtherScore() + "\n";
	s+="  Space Ports   " + getStaticSpacePortScore() + "\n";
	s+="Cumulative scores:\n";
	s+="  Dodechaedron  " + getCumDodeScore() + "/round\n";
	s+="  Home Worlds   " + getCumHomeScore() + "/round\n";
	s+="  Other Planets " + getCumOtherScore() + "/round\n";
	s+="  Space Ports   " + getCumSpacePortScore() + "/round\n";
	  
	s+="\n";
	s+="Game End Conditions:\n";

	if(getTurnLimit() > 0) {
	    s+=" Game ends at turn " + getTurnLimit() + "\n";
	}

	if(getScoreLimit() > 0) {
	    s+=" Game ends at score " + getScoreLimit() + "\n";
	}
	
	return s;
    }

    int getTurnLimit() {
	return turnLimit;
    }

    int getScoreLimit() {
	return scoreLimit;
    }

    int getStaticDodeScore() {
	return staticDodeScore;
    }

    int getStaticHomeScore() {
	return staticHomeScore;
    }

    int getStaticOtherScore() {
	return staticOtherScore;
    }

    int getStaticSpacePortScore() {
	return staticSpacePortScore;
    }

    int getCumDodeScore() {
	return cumDodeScore;
    }

    int getCumHomeScore() {
	return cumHomeScore;
    }

    int getCumOtherScore() {
	return cumOtherScore;
    }

    int getCumSpacePortScore() {
	return cumSpacePortScore;
    }

    void setTurnLimit(int turnLimit) {
	if(turnLimit < 0) {
	    System.err.println("Turn limit below zero, defaulting to zero");
	    turnLimit = 0;
	}
	this.turnLimit = turnLimit;
    }

    void setScoreLimit(int scoreLimit) {
	if(turnLimit < 0) {
	    System.err.println("Score limit below zero, defaulting to zero");
	    scoreLimit = 0;
	}

	this.scoreLimit = scoreLimit;
    }

    void setStaticDodeScore(int score) {
	staticDodeScore = score;
    }

    void setStaticHomeScore(int score) {
	staticHomeScore = score;
    }

    void setStaticOtherScore(int score) {
	staticOtherScore = score;
    }

    void setStaticSpacePortScore(int score) {
	staticSpacePortScore = score;
    }

    void setCumDodeScore(int score) {
	cumDodeScore = score;
    }

    void setCumHomeScore(int score) {
	cumHomeScore = score;
    }

    void setCumOtherScore(int score) {
	cumOtherScore = score;
    }

    void setCumSpacePortScore(int score) {
	cumSpacePortScore = score;
    }
}
