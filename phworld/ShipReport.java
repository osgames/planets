/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.util.*;
import java.io.Serializable;


public class ShipReport implements Serializable {

  private String name;
  private int owner;
  private int techLevel;

  private int hullAttack;
  private int shieldAttack;

  private int shieldDamage;
  private int hullDamage;
  private int componentDamage;

  private int hullDamageTotal;
  private int shieldsLeft;

  private boolean isAlive   = true;

  public ShipReport(String name, int owner, int techLevel) {
    this.name=name;
    this.owner=owner;
    this.techLevel=techLevel;
  }

  public String getName() {
    return name;
  }

  public int getTech() {
    return techLevel;
  }

  public int getOwner() {
    return owner;
  }

  public int getHullAttack() {
      return hullAttack;
  }

  public int getShieldAttack() {
      return shieldAttack;
  }

  public boolean isAlive() {
    return isAlive;
  }

  public int getShieldDamage() {
    return shieldDamage;
  }

  public int getHullDamage() {
    return hullDamage;
  }

    public int getHullHpLeft() {
	return ShipHull.getMaxDamage(techLevel) - hullDamageTotal;
    }

    public int getHullMaxHp() {
	return ShipHull.getMaxDamage(techLevel);
    }

  public int getComponentDamage() {
    return componentDamage;
  }

  public void setShieldDamage(int d) {
    shieldDamage=d;
  }

  public void setHullDamage(int d) {
    hullDamage=d;
  }

  public void setComponentDamage(int d) {
    componentDamage=d;
  }

  public void setHullDamageTotal(int d) {
      hullDamageTotal=d;
  }

    public void setShieldsLeft(int d) {
	shieldsLeft=d;
  }
  
    public int getHullDamageTotal() {
	return hullDamageTotal;
    }

    public int getShielsLeft() {
	return shieldsLeft;
    }

  public boolean isDamaged() {
    return (hullDamage > 0);
  }

  void addHullAttack(int attackAmount) {
    hullAttack+=attackAmount;
  }

  void addShieldAttack(int attackAmount) {
    shieldAttack+=attackAmount;
  }

  void setAliveness(boolean a) {
    isAlive=a;
  }

}
