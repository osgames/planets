/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.util.*;
import dsc.netgame.*;


/** ShipMath contains various functions that are used in calculations
 * regarding planets in the game. Functions are intended to be same in both
 * HostPlanet and Planet classes which both already inherit class from
 * dsc.netgame. All methods are static.
 * 
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public final class PlanetMath {
    static int calcNumberOfTechPlanets(int player,
				       Enumeration planets,
				       int techType,
				       int techLevel) {
	int n=0;

	while(planets.hasMoreElements()) {
	    Object p = planets.nextElement();
	    TechLevel tl = null;

	    if(p instanceof Planet) {
		Planet planet = (Planet) p;
		if(planet.getOwner() == player)
		    tl = planet.getTechLevels();
	    }

	    if(p instanceof HostPlanet) {
		HostPlanet planet = (HostPlanet) p;
		if(planet.getOwner() == player)
		    tl = planet.getTechLevels();
	    }

	    if(tl!=null) {
		if(tl.getTech(techType) >= techLevel)
		    n++;
	    }
	}
	
	return n;
    }
}


