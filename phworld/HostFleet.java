/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;
import dsc.util.*;


public class HostFleet extends HostWorldComponent implements MovingObject {

  private Location waypoint;
  private int trackTarget;
  private TargetValuation targetValuation;

  public HostFleet(HostWorld hw, 
		   int owner, 
		   Location location, 
		   Location waypoint,
		   int trackTarget) {
    super(hw,owner,location);
    
    this.waypoint = waypoint;
    scanRange=0;
    this.trackTarget=trackTarget;
    this.targetValuation = null;
  }
    
  public void putMessage(WorldComponentMessage m) {
    if(m.getSource()!=getOwner()) {
      return;
    }

    if(m instanceof SetWaypointWCM) {
      Location l;
      l=((SetWaypointWCM) m).getLocation();
      if(l!=null) 
	waypoint=l;
      trackTarget=-1;
    }

    if(m instanceof SetTrackingWCM) {
      trackTarget=((SetTrackingWCM) m).getTarget();
      if(trackTarget<-1)
	trackTarget=-1;
    }

    if(m instanceof SetTargetValuationWCM) {
	SetTargetValuationWCM stvw = (SetTargetValuationWCM) m;
	// null as value is OK, it is checked at get*
	this.targetValuation = stvw.getTargetValuation();
    }
  }

    public void doRound() {

	if(isEmpty()) {
	    hostWorld.removeComponent(getId());
	    return;
	}

	doFuelBalansing();
	//doMovement();
    }
    
    private void doFuelBalansing() {
	Enumeration ships = getShips();
	int nroOfShips    = 0;
	int totalFuel     = 0;
	int totalOneRoundConsumption = 0;

	while(ships.hasMoreElements()) {
	    HostShip hs = (HostShip) ships.nextElement();
	    nroOfShips++;

	    int fuel = hs.getMaterials().getFuel();
	    totalFuel += fuel;
	    hs.getMaterials().takeFuel(fuel);

	    totalOneRoundConsumption += hs.getFuelConsumptionForOneRound();
	}

	int maxMovementRounds = 1;

	if(totalOneRoundConsumption > 0)
	    maxMovementRounds = totalFuel / totalOneRoundConsumption;

	ships = getShips();
	while(ships.hasMoreElements()) {
	    HostShip hs = (HostShip) ships.nextElement();
	    totalFuel -= hs.putMaxFuel(maxMovementRounds * 
				       hs.getFuelConsumptionForOneRound());
	}

	ships = getShips();
	while(ships.hasMoreElements()) {
	    HostShip hs = (HostShip) ships.nextElement();
	    if(totalFuel>0 && nroOfShips>0) {
		int oneFuel=totalFuel/nroOfShips;
		nroOfShips--;

		totalFuel -= hs.putMaxFuel(oneFuel);
	    }
	}

	ships = getShips();
	while(ships.hasMoreElements()) {
	    HostShip hs = (HostShip) ships.nextElement();
	    if(totalFuel>0) {
		totalFuel -= hs.putMaxFuel(totalFuel);
	    }
	}


	ships = getShips();
	while(ships.hasMoreElements()) {
	    HostShip hs = (HostShip) ships.nextElement();
	    if(totalFuel>0) {
		totalFuel -= hs.forcePutFuel(totalFuel);
	    }
	}
    }


  public void doMovement() {
    Enumeration e = getShips();

    if(trackTarget != -1) {
	HostWorldComponent hwc = hostWorld.getComponentById(trackTarget);

      if(hostWorld.isVisibleByPlayer(getOwner(), hwc) ||
	 hwc instanceof HostPlanet) {
	waypoint=hwc.getLocation();
      }
    }

    if(!waypoint.equals(location)) {
	location = location.moveTowards(waypoint, getSpeed());
    }

    while(e.hasMoreElements()) {
	HostShip hs = (HostShip) e.nextElement();
	hs.doMovement();
    }
  }

    public int getSpeed() {
	Enumeration ships = getShips();
	int minSpeed = 99999;
	
	while(ships.hasMoreElements()) {
	    HostShip hs = (HostShip) ships.nextElement();
	    
	    int speed = hs.getNextMovementAmount();
	    if(speed < minSpeed) {
		minSpeed = speed;
	    }
	}

	if(minSpeed == 99999) {
	    return 0;
	} else
	    return minSpeed;
    }
    
    private int getMaxHullTech() {
	Enumeration ships = getShips();
	int max = 1;

	while(ships.hasMoreElements()) {
	    HostShip hs = (HostShip) ships.nextElement();

	    if(hs.getHullTech() > max)
		max=hs.getHullTech();
	}

	return max;
    }

    protected int getBearing() {
	return (location.getBearingTo(waypoint));
    }

  public WorldComponent getWorldComponent(int player) {

      if(getOwner() == player) {
	  Fleet f = new Fleet(id, name, owner, location);
	  
	  /*****/
	  f.setHullTech(getMaxHullTech());
	  f.setBearing(getBearing());
	  f.setTrackTarget(trackTarget);
	  f.setWaypoint(waypoint);
	  f.setTargetValuation(targetValuation);

	  return f;
      } 

      return null;
  }

  public boolean isVisible() {
    return true;
  }

  public boolean orbitingFriendlyPlanet() {
    Enumeration e = hostWorld.getComponentsByLocation(location);

    while (e.hasMoreElements()) {
      HostWorldComponent hwc = (HostWorldComponent) e.nextElement();

      if(hwc instanceof HostPlanet) {
	if(hwc.getOwner() == owner)
	  return true;
	else
	  return false;
      }
    }

    return false;
  }

  public String toString() {
    return getName();
  }

  public boolean isTrackingSomeone() {
      if(trackTarget < 0)
	  return false;
      else
	  return true;
  }

    public boolean isEmpty() {
	Enumeration e = getShips();
	if(e.hasMoreElements())
	    return false;
	else
	    return true;
    }

    public Enumeration getShips() {
	Vector v = new Vector();

	Enumeration e = hostWorld.getComponentsByLocation(getLocation());

	while(e.hasMoreElements()) {
	    HostWorldComponent wc = (HostWorldComponent) e.nextElement();
	    if(wc instanceof HostShip) {
		if(wc.getOwner() == getOwner() &&
		   ((HostShip)wc).getTrackTarget() == getId())
		    v.addElement(wc);
	    }
	}

	return v.elements();	    
    }

    public int getMass() {
	int m=0;
	Enumeration e = getShips();

	while(e.hasMoreElements()) {
	    HostShip hs = (HostShip) e.nextElement();
	    m += hs.getMass();
	}

	return m;
    }

    public HostWorldComponent getEffectiveTrackTarget() {
	if(getSpeed() == 0)
	    return null;

	HostWorldComponent target = 
	    hostWorld.getComponentById(trackTarget);


	if(target instanceof MovingObject)
	    return target;   
	else
	    return null;
    }

    private Location getEffectiveWaypoint() {
	HostWorldComponent hwc = getEffectiveTrackTarget();

	if(hwc == null)
	    return waypoint;

	return hwc.getLocation();
    }

    public int getETA() {
	int speed = getSpeed();

	if(speed == 0)
	    return Integer.MAX_VALUE;

	return (100*getEffectiveWaypoint().distance(getLocation()))/speed;
    }

    protected void moveToLocation(Location newLocation) {
	    Enumeration ships = getShips();

	    while(ships.hasMoreElements()) {
		HostShip hs = (HostShip) ships.nextElement();
		
		hs.moveToLocation(newLocation);
	    }

	    setLocation(newLocation);
	    waypoint = (Location) newLocation.clone();
    }

    protected TargetValuation getTargetValuation() {
	if(targetValuation != null)
	    return targetValuation;

	// Should never be null, but if is, fall back to default case
	return new TargetValuation();
    }
}
