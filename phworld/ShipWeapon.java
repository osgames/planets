/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;


public class ShipWeapon extends ShipComponent {

  private static int mass[] = {0,35,35,65,65,95,95,95,95};

  private static int fuel[] = {0,0,0,0,0,0,0,0,0};
  private static int dodechadrium[] = {0,0,0,0,0,0,0,0,2};
  private static int metals[] =   {0,12, 8,25,20,45,30, 67, 95};
  private static int minerals[] = {0,29,25,50,45,90,80, 95,110};
  private static int oil[] =      {0, 7,15,15,40,23,80, 80,120};
  private static int troops[] = {0,0,0,0,0,0,0,0,0}; 

  private static int techFuel[] = {0,0,0,0,0,0,0,0,0};
  private static int techDodechadrium[] = {0,0,0,0,0,0,0,200,400};
  private static int techMetals[]   = highTechRequirement;
  private static int techMinerals[] = smallTechRequirement;
  private static int techOil[]      = mediumTechRequirement;
  private static int techTroops[] = {0,0,0,0,0,0,0,0,0};


  private static int powerConsumption[] = {0,0,0,0,0,0,0,0,0}; 

  private static int damageResistance[] = {0,45,45,85,85,
					   140,140,200,350}; 

  private static int maxInflictableHullDamage[] 
      = {0, 100,  75, 160,  90, 215,  105, 190,  380};

  private static int maxInflictableShieldDamage[] 
      = {0,  65, 120,  75, 230, 120, 440, 365,  730};

  private static int maxInflictableTroopDamage[] 
      = {0,  75, 100,  85, 150, 95, 200, 205,  410};

  private static String[] gadgetName =
  {
	"0",
	"Laser",
	"Mass Driver",
	"Blaster",
	"Torpedo",
	"Photon",
	"Missile",
	"Plasma",
	"Disruptor"
  };

  /** Deducted from the randomised damage to allow non-positive results, 
   * which then are converted into 0 damages.
   */
  private static final int MISFIRE = 50;


//CONSTRUCTOR
  public ShipWeapon(int tech) {
    super(tech);
  }
 
  /** Returns an integer number which represents the damage inflicted
   * by the weapon, if the damage is 0, no damage inflicted.
   * 
   * @return Damage inflicted by the weapon
   */
  public int getInflictedHullDamage() {
    int damage = 0;

    if(getDamagePercentage() == 100)
      return 0;

    if(!isActive())
      return 0;

    damage=( (int) (Math.random() * maxInflictableHullDamage[techLevel]) -
	     MISFIRE);

    if(damage<0) damage=0;
    damage = (damage * (100-this.getDamagePercentage())) / 100;

    return  damage+1;
  }

  public int getInflictedShieldDamage() {
    int damage = 0;

    if(getDamagePercentage() == 100)
      return 0;

    if(!isActive())
      return 0;

    damage=( (int) (Math.random() * maxInflictableShieldDamage[techLevel]) -
	     MISFIRE);

    if(damage<0) damage=0;
    damage = (damage * (100-this.getDamagePercentage())) / 100;

    return  damage+1;
  }

  public int getInflictedTroopDamage() {
    int damage = 0;

    if(getDamagePercentage() == 100)
      return 0;

    if(!isActive())
      return 0;

    damage=( (int) (Math.random() * maxInflictableTroopDamage[techLevel]) -
	     MISFIRE);

    if(damage<0) damage=0;
    damage = (damage * (100-this.getDamagePercentage())) / 100;

    return  damage+1;
  }

  /** Returns an integer number which represents the maximum damage 
   * a weapon of of tech level 'tech' can inflict.
   * 
   * @return Maximum inflictable damage
   */

    static public int getMaxInflictableHullDamage (int tech) {
	if (tech<1 || tech>8) throw new IllegalArgumentException
	    ("Non-existent tech level not between (1-8)");
	return(maxInflictableHullDamage[tech]-MISFIRE);
    }

    static public int getMaxInflictableShieldDamage(int tech) {
	if (tech<1 || tech>8) throw new IllegalArgumentException
	    ("Non-existent tech level not between (1-8)");
	return(maxInflictableShieldDamage[tech]-MISFIRE);
    }

    static public int getMaxInflictableTroopDamage(int tech) {
	if (tech<1 || tech>8) throw new IllegalArgumentException
	    ("Non-existent tech level not between (1-8)");
	return(maxInflictableTroopDamage[tech]-MISFIRE);
    }


  /** Returns an integer number which represents the maximum damage 
   * this weapon can inflict.
   * 
   * @return Maximum inflictable damage
   */


  public int getMaxInflictableHullDamage() {
    return getMaxInflictableHullDamage(techLevel);
  }

  public int getMaxInflictableShieldDamage() {
    return getMaxInflictableShieldDamage(techLevel);
  }

  public int getMaxInflictableTroopDamage() {
    return getMaxInflictableTroopDamage(techLevel);
  }

  /** Returns an integer number which represents the maximum damage 
   * this weapon can currently inflict.
   * 
   * @return Maximum inflictable damage at the moment.
   */
  
  public int getInflictableHullDamage() {
    if(isActive())
      return (getMaxInflictableHullDamage(techLevel) *
	      (100 - getDamagePercentage())) / 100; 
    else
      return 0;
  }

  public int getInflictableShieldDamage() {
    if(isActive())
      return (getMaxInflictableShieldDamage(techLevel) *
	      (100 - getDamagePercentage())) / 100; 
    else
      return 0;
  }

  public int getInflictableTroopDamage() {
    if(isActive())
      return (getMaxInflictableTroopDamage(techLevel) *
	      (100 - getDamagePercentage())) / 100; 
    else
      return 0;
  }

  public int getPowerOutput() {
    return getInflictableHullDamage() + getInflictableShieldDamage();
  }

  /** Returns materials required to build the ShipWeapon. 
   * getBuildMaterials() returns a MaterialBundle that has all
   * the materials required to build the ShipShield.
   *
   * @param tech Tech level
   * @return Materials required to build a shield of the spec. tech level
   */
  public static MaterialBundle getBuildMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(fuel[tech]);
    mb.putDodechadrium(dodechadrium[tech]);
    mb.putMetals(metals[tech]);
    mb.putMinerals(minerals[tech]);
    mb.putOil(oil[tech]);
    mb.putTroops(troops[tech]);

    return mb;
  }

  /** Returns the time taken to build the ShipWeapon.
   * Weapons of different tech level have varying building times. Presently
   * the building time is directly proportional to the tech level of the
   * shield.
   *
   * @param tech Tech level
   * @return Time taken in building the ShipShield of the spec. tech level
   */
  public static int getBuildTime(int tech) {
      return 1;
  }

  /** Returns the materials required in upgrading the ShipWeapon tech level 
   * of a planet.
   *
   * @param tech Tech level
   * @return Materials required in upgrading one tech to given techlevel
   */
  public static MaterialBundle getTechMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(techFuel[tech]);
    mb.putDodechadrium(techDodechadrium[tech]);
    mb.putMetals(techMetals[tech]);
    mb.putMinerals(techMinerals[tech]);
    mb.putOil(techOil[tech]);
    mb.putTroops(techTroops[tech]);

    return mb;
  }

  /** Returns the power consumption of the Shield.
   * ShipShields consume presently no at all...
   *
   * @return Power consumption of the ShipShield
   */ 
  public int getPowerConsumption() {
    if (this.isActive()) return powerConsumption[techLevel];
    else return 0;
  }


  /** Returns the maximum damgage that the ShipWeapon can receive without 
   * being totally destroyed.
   * 
   * @return Maximum obtainable damage
   */
  public int getMaxDamage() {
    return damageResistance[techLevel];
  }

  /** Returns the mass of the ShipShield.
   *
   * @return Mass of the ShipShield
   */
  public int getMass() {
    return mass[techLevel];
  }

  public String toString() {
    return "W"+techLevel+" " + gadgetName[techLevel];
  }

    public int getTechType() {
	return ShipComponent.WEAPON;
    }
}
