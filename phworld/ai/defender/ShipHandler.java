/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2001 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld.ai.defender;

import dsc.netgame.*;
import phworld.*;
import java.util.*;

class ShipHandler {

    /** Ship which has weapons, but no engines */
    public static int DEFENDER = 1;

    /** Ship which has weapons and can move */
    public static int ATTACK   = 2;

    /** Ship which has lots of free cargo space and can move */
    public static int CARGO    = 3;

    /** Tech 1 scout ship. */
    public static int SCOUT    = 4;


    private AI parentAI;
    private PHWorld world;

    private Hashtable planetOwners = null;

    public ShipHandler(AI parentAI) {
	this.parentAI = parentAI;
    }

    void debugPrint(String s) {
	parentAI.debugPrint("(SH)" + s);
    }

    void doShipActions(PHWorld world) {
	this.world = world;
	
	if(planetOwners == null) {
	    planetOwners = new Hashtable();
	}

	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(world.getController().playerNumber==wc.getOwner() &&
	       wc instanceof Ship) {
		handleShip((Ship) wc);

	    }

	    if(wc instanceof Planet &&
	       parentAI.canSeeAt(wc.getLocation())) {
		planetOwners.put(wc.getName(), new Integer(wc.getOwner()));
	    }
	}

	e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(world.getController().playerNumber==wc.getOwner() &&
	       wc instanceof Fleet) {
		handleFleet((Fleet) wc);

	    }
	}
    }

    void handleShip(Ship s) {
	if(s.isInFleet()) {
	    debugPrint("+ Ship " + s.getName() + " is in fleet -> ignored");
	    return;
	}

	debugPrint("+ Handling Ship " + s.getName());

	if(s.getHullTech() == 1) {
	    handleScout(s);
	} else if(s.getHullTech() == 3 || s.getHullTech() == 6) {
	    handleCargoShip(s);
	} else if(s.getSpeed() > 0) {
	    handleAttackShip(s);
	}
    }

    void handleAttackShip(Ship s) {
	Enumeration fleets = parentAI.allOwnFleetsAt(s.getLocation());

	if(fleets.hasMoreElements()) {
	    Fleet f = (Fleet) fleets.nextElement();
	    s.startTracking(f);
	} else {
	    s.initiateFormFleet();
	}
    }

    private int preferredFleetEDT(Fleet f) {
	/* Note: This should really be a function calculated from
	   techlevels and planet distances of the current universum */

	return 650;
    }

    boolean tryJoinFleet(Ship s) {
	Enumeration fleets = parentAI.allOwnFleetsAt(s.getLocation());

	while(fleets.hasMoreElements()) {
	    Fleet f = (Fleet) fleets.nextElement();

	    if(f.getMaxEDT() < preferredFleetEDT(f)) {
		s.startTracking(f);
		return true;
	    }
	}

	return false;
    }

    private int calcEnemyShieldPowerAt(Location l) {
	Enumeration e = world.getComponentsByLocation(l);
	int total = 0;

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship) {
		Ship s = (Ship) wc;
		if(s.getOwner() != world.getController().playerNumber &&
		   s.getLocation().equals(l)) {
		    total += s.getShieldPower();
		}
	    }
	}

	return total;
    }

    private int getBestVisibleEnemyShieldPower() {
	return 9000; ///////
    }

    private int getTargetGoodness(WorldComponent target, Fleet f) {
	if(target == null)
	    return -1;

	int edt = f.getEDT();

	if(target.getLocation().distance(f.getLocation()) > edt)
	    return -1;

	int goodness = 0;

	int enemyShieldPower = calcEnemyShieldPowerAt(target.getLocation());
	
	if(enemyShieldPower > 0) {
	    goodness -= enemyShieldPower;
	    goodness += f.getShieldPower();
	}

	Planet overPlanet = f.overOwnPlanet();
	if(overPlanet != null && 
	   parentAI.getPlanetHandler().isBuildingPlanet(overPlanet))
	    goodness -= target.getLocation().distance(f.getLocation())*2;

	if(target instanceof Planet) {
	    Planet p = (Planet) target;
	    if(target.getOwner() == world.getController().playerNumber) {
		int fuel = p.getMaterials().getFuel();
		int cons = f.getConsumption();

		if(fuel > cons*8) {
		    int fg = fuel - cons*8;

		    if(fg > 1010)
			fg = 1000+((int) Math.sqrt((double)fg));
		    goodness += fg;
		    goodness += p.getProduction().getDodechadrium()*250;
		}
	    } else {
		int type = p.getPlanetType();
		goodness += 1400;

		if(type >= 0 && type <= 12) {
		    MaterialBundle probableProduction =
			PlanetType.getProductionMaterials(p.getPlanetType());
		    goodness += probableProduction.getFuel()*20 +
			probableProduction.getDodechadrium()*250 +
			probableProduction.getMetals()*18 +
			probableProduction.getMinerals()*18 +
			probableProduction.getOil()*18 -
			probableProduction.getTroops()*10;
		} else if(type == PlanetType.HOME) {
		    goodness += 35*4*5*2;
		}
	    }
	}

	return goodness;
    }

    private WorldComponent calcFleetTarget(Fleet f) {
	WorldComponent target = null;
	int best = 0;
	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Planet) {
		int goodness = getTargetGoodness(wc, f);
		if(goodness > best) {
		    best = goodness;
		    target = wc;
		}
	    }
	}

	return target;
    }

    private WorldComponent getGoodTargetForFleet(Fleet f) {
	int shield_power = f.getShieldPower();

	WorldComponent target = calcFleetTarget(f);

	if(shield_power < getBestVisibleEnemyShieldPower())
	    target = null;

	if(target != null) {
	    if(target.getLocation().distance(f.getLocation()) >= f.getEDT())
		target = null;
	}

	return target;
    }

    void joinFleets(Fleet to, Fleet from) {
	debugPrint("++ Joining fleet " + from.getName() + 
		   " to fleet " + to.getName());

	Enumeration ships = from.getShips();
	
	while(ships.hasMoreElements()) {
	    Ship s = (Ship) ships.nextElement();

	    s.startTracking(to);
	}

	handleFleet(to);
    }

    void handleFleet(Fleet f) {
	debugPrint("+ Handling Fleet " + f.getName());

	Enumeration fleets = parentAI.allOwnFleetsAt(f.getLocation());

	while(fleets.hasMoreElements()) {
	    Fleet fl = (Fleet) fleets.nextElement();

	    if(fl != f) {
		if(fl.getShips().hasMoreElements()) {
		    joinFleets(fl, f);
		    return;
		}
	    }
	}

	f.initiateLoadFuel();
	WorldComponent target = getGoodTargetForFleet(f);
	if(target != null)
	    f.startTracking(target);
	else {
	    WorldComponent ctt = world.getComponentById(f.getTrackTarget());
	    if((ctt != null) && (f.overAnyPlanet()!=null) ) {
		f.startTracking(null);
	    }
	}
	
	debugPrint("++ Fleet handled");
    }

    private boolean isOwnShipGoingTo(Location l, Ship exclude) {
	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship) {
		Ship s = (Ship) wc;

		if(!(s == exclude) &&
		   s.getOwner() == world.getController().playerNumber &&
		   s.getWaypoint().equals(l)) {
		    return true;
		}
	    }
	}

	return false;
    }

    private boolean isOwnCargoShipGoingTo(Location l, Ship exclude) {
	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship) {
		Ship s = (Ship) wc;

		if(!(s == exclude) &&
		   s.getOwner() == world.getController().playerNumber &&
		   s.getWaypoint().equals(l) &&
		   (s.getHullTech() == 3 || s.getHullTech() == 6)) {
		    return true;
		}
	    }
	}

	return false;
    }


    private boolean canScoutGoTo(Ship s, Planet p) {
	if(p.getOwner() != -1)
	    return false;
	
	if(isOwnShipGoingTo(p.getLocation(), s))
	    return false;
	
	Integer owner = (Integer) planetOwners.get(p.getName());

	if(owner != null) {
	    int o = owner.intValue();

	    if(o == -1)
		return true;
	    else
		return false;
	}

	return true;
    }

    private Planet findFreePlanetInRange(Ship forWho, int maxRange) {
	Planet  target = null;
	int  nearest   = maxRange+1;
	Location start = forWho.getLocation();

	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Planet) {
		Planet p = (Planet) wc;
		Location l = p.getLocation();
	    
		if(!l.equals(start) &&
		   l.distance(start) < nearest &&
		   canScoutGoTo(forWho, p)) {
		    nearest = l.distance(start);
		    target = p;
		}
	    }
	}

	return target;
    }

    private Location calcWaypoint(Location start, 
				  Location target, 
				  int maxHop) {
	if(start.distance(target) <= maxHop)
	    return target;

	///////

	return start;
    } 

    private void handleScout(Ship s) {
	s.initiateLoadFuel();
	
	Planet target = findFreePlanetInRange(s, s.getEDT());

	if(target == null)
	    return;

	s.setNewWaypoint(target.getLocation());
    }

    private void handleCargoShip(Ship s) {
	int cargoMass = s.getMaterials().getMinerals() +
	    s.getMaterials().getMetals() +
	    s.getMaterials().getOil() +
	    s.getMaterials().getTroops() +
	    s.getMaterials().getDodechadrium();
	    
	if(cargoMass > 0)
	    handleCargoDelivery(s);
	else {
	    if(tryJoinFleet(s))
		return;

	    handleCargoPickup(s);
	}
    }

    private boolean isEnemyShipAt(Location l) {
	Enumeration e = world.getWorldComponents();

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship &&
	       wc.getOwner() != world.getController().playerNumber  &&
	       wc.getLocation().equals(l)) {
		return true;
	    }
	}

	return false;
    }

    private int getNumberOfOwnFightersAt(Location l) {
	Enumeration e = world.getWorldComponents();
	int n=0;

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship &&
	       wc.getOwner() == world.getController().playerNumber  &&
	       wc.getLocation().equals(l)) {
		if(((Ship) wc).getWeaponHullPower() > 0) {
		    n++;
		}
	    }
	}

	return n;
    }

    private int getNumberOfOwnFighters() {
	Enumeration e = world.getWorldComponents();
	int n=0;

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship &&
	       wc.getOwner() == world.getController().playerNumber) {
		if(((Ship) wc).getWeaponHullPower() > 0) {
		    n++;
		}
	    }
	}

	return n;
    }


    private int getOwnWeaponPower() {
	Enumeration e = world.getWorldComponents();
	int n=0;

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship &&
	       wc.getOwner() == world.getController().playerNumber) {
		n += ((Ship) wc).getWeaponHullPower(); 
	    }
	}

	return n;
    }


    private Planet getCargoPickupPlanet(Ship s) {
	Planet target = null;
	int best = -1;

	Enumeration e = parentAI.allOwnPlanets();
	
	while(e.hasMoreElements()) {
	    Planet p = (Planet) e.nextElement();

	    if(!parentAI.getPlanetHandler().isBuildingPlanet(p)) {
		int goodness = 10000;

		goodness -= s.getLocation().distance(p.getLocation())*2;

		MaterialBundle m = p.getMaterials();
		int f = p.getMaterials().getFuel()-500;
		if(f>1000)
		    f=1000;

		goodness += m.getMinerals() + m.getMetals() + m.getOil() +
		    m.getDodechadrium()*50 + f/2;

		if(goodness > best &&
		   !isOwnCargoShipGoingTo(p.getLocation(), s) &&
		   !isEnemyShipAt(p.getLocation())) {
		    best = goodness;
		    target = p;
		}
	    }
	}

	return target;
    }

    private void handleCargoPickup(Ship s) {
	s.initiateLoadFuel(s.getTotalConsumption()*8);

        Planet target = getCargoPickupPlanet(s);
	if(target == null)
	    return;

	if(s.getLocation().equals(target.getLocation())) {
	    pickCargo(s, target);
	    handleCargoDelivery(s);
	    return;
	}

	s.initiateLoadFuel(s.getTotalConsumption()*8);
	s.setNewWaypoint(calcWaypoint(s.getLocation(),
				      target.getLocation(),
				      s.getEDT()-150));
    }

    private void pickCargo(Ship s, Planet p) {
	dropCargo(s, p);
	debugPrint("++ Picking up cargo at " + p.getName());

	int space = s.getMaterials().getFreeSpace();
	int fuelTarget = s.getTotalConsumption()*8;
	space -= fuelTarget;

	if(space < 0)
	    return;

	MaterialBundle fromBundle =
	    new MaterialBundle(MaterialBundle.UNLIMITED);
	
	int d = p.getMaterials().getDodechadrium();
	if(d>space)
	    d=space;
	fromBundle.putDodechadrium(d);
	space-=d;
	
	MaterialBundle onPlanet = new MaterialBundle(MaterialBundle.UNLIMITED);
	onPlanet.putMinerals(p.getMaterials().getMinerals());
	onPlanet.putMetals(p.getMaterials().getMetals());
	onPlanet.putOil(p.getMaterials().getOil());

	int totalOnPlanet = onPlanet.getMass();
	if(totalOnPlanet!=0) {
	    int factor = (space*100)/totalOnPlanet;
	    if(factor > 100)
		factor=100;
	    if(factor <0)
		factor=0;

	    onPlanet.scaleBundle(factor);
	    fromBundle.addFullBundle(onPlanet);
	}

	s.initiateMaterialTransfer(p.getId(),
				   new MaterialBundle(MaterialBundle.UNLIMITED),
				   fromBundle,
				   new Vector(),
				   new Vector());
				 
	
    }

    private void handleCargoDelivery(Ship s) {
	s.initiateLoadFuel();

	Planet target = 
	    parentAI.getPlanetHandler().
	    getPreferredTransferTarget(s.getMaterials(), 
				       s.getLocation(),
				       s.getEDT());

	if(target == null)
	    return;

	if(target.getLocation().equals(s.getLocation())) {
	    dropCargo(s, target);
	    if(tryJoinFleet(s))
		return;
	    handleCargoPickup(s);
	    return;
	}

	s.setNewWaypoint(calcWaypoint(s.getLocation(),
				      target.getLocation(),
				      s.getEDT()-5));
    }

    private void dropCargo(Ship s, Planet p) {
	debugPrint("++ Dropping cargo at " + p.getName());

	MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
	mb.addFullBundle(s.getMaterials());

	s.initiateMaterialTransfer(p.getId(),
				   mb,
				   new MaterialBundle(MaterialBundle.UNLIMITED),
				   new Vector(),
				   new Vector());
    }

    int getNumberOfScouts() {
	Enumeration e = parentAI.allOwnShips();
	int n=0;

	while(e.hasMoreElements()) {
	    Ship s = (Ship) e.nextElement();

	    if(s.getHullTech() == 1)
		n++;
	}

	return n;
    }

    int getCargoShipCapacity() {
	Enumeration e = parentAI.allOwnShips();
	int n=0;

	while(e.hasMoreElements()) {
	    Ship s = (Ship) e.nextElement();

	    if((s.getHullTech() == 3 || s.getHullTech() == 6) &&
	       !s.isInFleet())
		n += s.getMaterials().getCapacity();
	}

	return n;
    }

    int getPreferredNewShipType(Planet p) {
	boolean enemyPresent   = isEnemyShipAt(p.getLocation());
	int     nroOwnFighters = getNumberOfOwnFightersAt(p.getLocation());

	/*
	if(getNumberOfScouts() < 1)
	    return SCOUT;
	*/

	if(enemyPresent ||
	   nroOwnFighters < 1) {
	    return DEFENDER;
	}

	if(getCargoShipCapacity() <
	   parentAI.getPlanetHandler().getTotalProduction().getMass()*2) {
	    return CARGO;
	}

	if(Math.random() < 0.25) {
	    return DEFENDER;
	} else {
	    return ATTACK;
	}
    }

    int getPreferredMaterialStock(Planet p) {
	if(getCargoShipCapacity() < 700)
	    return 0;

	int n = p.getMaterials().getTroops();
	if(n>600)
	    n=600;

	return n;
    }
}
