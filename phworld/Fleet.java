/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import dsc.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.awt.image.*;


public class Fleet extends BaseShip implements SidePanelOwner {

    private static Panel controlPanel = null;
    private static GridBagConstraints controlPanelConstraints = null;

    private static ImageComponent shipImageComponent;
    private static Panel generalPanel = null;
    private static Label nameLabel = null;
    private static Button waypointButton = null;
    private static Button trackShipButton = null;
    private static Button loadFuelButton = null;
    private static Fleet buttonListener = null;
    private static BattleOrderPanel ordersPanel = null;

    private static TextArea infoText = null;
    private int edt = 0;

    public Fleet (int id, String name, int owner, Location location) {
	super(id,name,owner,location);
    }

    protected void changeControlPanel(Panel newPanel) {
	if(controlPanelConstraints == null) {
	    controlPanelConstraints = new GridBagConstraints();

	    controlPanelConstraints.fill=GridBagConstraints.BOTH;
	    controlPanelConstraints.anchor=GridBagConstraints.NORTH;
	    controlPanelConstraints.weightx=100;
	    controlPanelConstraints.weighty=1;
	    controlPanelConstraints.gridwidth=1; 
	    controlPanelConstraints.gridheight=1;
	    controlPanelConstraints.gridx=0;
	    controlPanelConstraints.gridy=0;
	}

	controlPanel.removeAll();
	controlPanel.add(newPanel, controlPanelConstraints);
	controlPanel.validate();
    }

    void createControlPanel() {
	controlPanel=new Panel();
	controlPanel.setLayout(new GridBagLayout());

	generalPanel=new Panel();
	changeControlPanel(generalPanel);

	generalPanel.setLayout(new VerticalFlowLayout());

	shipImageComponent = new ImageComponent(
						world.getImage("data/graph/still/panel/ship1.gif"));
	generalPanel.add(shipImageComponent);
    
	nameLabel=new Label("Name");
	generalPanel.add(nameLabel);

	infoText=new TextArea("",15,22,TextArea.SCROLLBARS_NONE);
	infoText.setEditable(false);

	generalPanel.add(infoText);

	waypointButton=new Button("Set Waypoint");
	trackShipButton=new Button("Track");
	loadFuelButton=new Button("Load fuel");

	generalPanel.add(waypointButton);
	generalPanel.add(trackShipButton);
	generalPanel.add(loadFuelButton);

	ordersPanel = new BattleOrderPanel();
    }

    public int getConsumption() {
	Enumeration ships = getShips();
	int consumption = 0;

	while(ships.hasMoreElements()) {
	    Ship s = (Ship) ships.nextElement();
	    consumption+=s.getTotalConsumption();
	}

	return consumption;
    }

    public int getMinSpeed() {
	Enumeration ships = getShips();
	int min_speed = 99999;

	while(ships.hasMoreElements()) {
	    Ship s = (Ship) ships.nextElement();

	    int speed = s.getSpeed();
	    if(speed<min_speed)
		min_speed=speed;
	}

	return min_speed;
    }

    public int getTotalFuel() {
	int total_fuel=0;
	Enumeration ships = getShips();

	while(ships.hasMoreElements()) {
	    Ship s = (Ship) ships.nextElement();
	    total_fuel+=s.getMaterials().getFuel();
	}

	return total_fuel;
    }

    public int getMaxTotalFuel() {
	int total_fuel=0;
	Enumeration ships = getShips();

	while(ships.hasMoreElements()) {
	    Ship s = (Ship) ships.nextElement();
	    total_fuel+=s.getMaterials().getFuel();
	    total_fuel+=s.getMaterials().getFreeSpace();
	}

	return total_fuel;
    }

    public int getShieldPower() {
	int shield_power=0;
	Enumeration ships = getShips();

	while(ships.hasMoreElements()) {
	    Ship s = (Ship) ships.nextElement();
	    shield_power+=s.getShieldPower();
	}

	return shield_power;
    }

    private void setInfoTextOwn() {
	int total_mass=0;
	int mass_max=0;
	int consumption=0;
	int total_fuel=0;
	int fuel_max=0;
	int min_speed=999999;

	int hullhp=0;
	int maxhullhp=0;
	int engine_power=0;
	int max_engine_power=0;
	int weapon_hull_power=0;
	int max_weapon_hull_power=0;
	int weapon_shield_power=0;
	int max_weapon_shield_power=0;
	int shield_power=0;
	int max_shield_power=0;

	Enumeration ships = getShips();

	while(ships.hasMoreElements()) {
	    Ship s = (Ship) ships.nextElement();
	    total_mass+=s.getMass() + s.getMaterials().getMass();
	    mass_max+=s.getMass() + s.getMaterials().getCapacity();
	    total_fuel+=s.getMaterials().getFuel();
	    fuel_max  +=s.getMaterials().getFreeSpace() +
		s.getMaterials().getFuel();
	    consumption+=s.getTotalConsumption();

	    int speed = s.getSpeed();
	    if(speed<min_speed)
		min_speed=speed;

	    hullhp+=s.getHullHPLeft();
	    maxhullhp+=s.getMaxHullHP();
	    engine_power+=s.getEnginePower();
	    max_engine_power+=s.getMaxEnginePower();
	    weapon_hull_power+=s.getWeaponHullPower();
	    max_weapon_hull_power+=s.getMaxWeaponHullPower();
	    weapon_shield_power+=s.getWeaponShieldPower();
	    max_weapon_shield_power+=s.getMaxWeaponShieldPower();
	    shield_power+=s.getShieldPower();
	    max_shield_power+=s.getMaxShieldPower();
	}

	infoText.setText("");
      
	infoText.append("Mass: " + total_mass + "/" + mass_max + "\n");
	infoText.append("Fuel: " + total_fuel + "/" + fuel_max + "\n");
	infoText.append("FC/round: " + consumption + "\n");

	infoText.append("\n");
	infoText.append("Hull: " + hullhp
			+ "/" +
			maxhullhp + " (" +
			ShipMath.getPercentage(hullhp, maxhullhp) + "%)\n");
	infoText.append("Engines: " + engine_power +
			"/" + max_engine_power + " (" +
			ShipMath.getPercentage(engine_power,
					       max_engine_power) + "%)\n");
	infoText.append("Weapons H: " + 
			weapon_hull_power + 
			"/" + max_weapon_hull_power + 
			" (" +
			ShipMath.getPercentage(weapon_hull_power,
					       max_weapon_hull_power) + "%)\n");
	infoText.append("Weapons S: " + 
			weapon_shield_power + 
			"/" + max_weapon_shield_power + 
			" (" +
			ShipMath.getPercentage(weapon_shield_power,
					       max_weapon_shield_power) + "%)\n");

	infoText.append("Shields: " + shield_power + 
			"/" + max_shield_power + " (" +
			ShipMath.getPercentage(shield_power, 
					       max_shield_power) + "%)\n");

	infoText.append("\n");

	String wspeed = (((double) min_speed) / ShipMath.SPEED_CONST) + "";
	if(wspeed.length() > 4)
	    wspeed=wspeed.substring(0, 4);
	infoText.append("Warp speed: " + wspeed + "\n");

	if(consumption>0)
	    edt = min_speed*(total_fuel/consumption);
	else
	    edt = 0;

	if(trackTarget==-1) {
	    infoText.append("\nWaypoint: " + getWaypointText() + "\n");
	} else {
	    WorldComponent ttarget = world.getComponentById(trackTarget);
	    if(ttarget==null)
		infoText.append("\nLost track\n");
	    else
		infoText.append("\nTracking " + ttarget.getName() + "\n");
	}

	infoText.append("Distance: " + 
			ShipMath.speedToWarpString(location.distance(waypoint)) 
			+ "\n");

	updateWarningCircle();
    }

    public void setInfoText() {
	if(infoText == null)
	    return;
	
	if(world.getController().playerNumber==getOwner()) {
	    setInfoTextOwn();
	}
    }

    protected void resetControlPanel() {
	nameLabel.setText(getName());

	shipImageComponent.setImage(
				    world.getImage("data/graph/still/panel/ship"+hullTechLevel+".gif"));

	if(buttonListener!=null) {
	    waypointButton.removeActionListener(buttonListener);
	    trackShipButton.removeActionListener(buttonListener);
	    loadFuelButton.removeActionListener(buttonListener);

	    buttonListener=null;
	}

	if(world.getController().playerNumber==getOwner()) {
	    waypointButton.addActionListener(this);
	    trackShipButton.addActionListener(this);
	    loadFuelButton.addActionListener(this);

	    buttonListener=this;

	    waypointButton.setEnabled(true);
	    trackShipButton.setEnabled(true);

	    if(overOwnPlanet() != null) {
		loadFuelButton.setEnabled(true);
	    }
	    else {
		loadFuelButton.setEnabled(false);
	    }
	}

	setInfoText();
	changeControlPanel(generalPanel);
    }

    public String getMapImage() {
	return ShipHull.getMapImageName(hullTechLevel);
    }

    public Panel getControlPanel() {
    
	if(controlPanel==null) {
	    createControlPanel();
	}
    
	resetControlPanel();

	return controlPanel;
    }


    public void actionPerformed(ActionEvent evt) {
	String arg = evt.getActionCommand();

	if(arg == null) {
	    MenuItem mi = (MenuItem) evt.getSource();
	    arg=mi.getLabel();
	}

	if(arg.equals("Set Waypoint")) 
	    world.requestLocation();
	else if(arg.equals("Track"))
	    world.requestComponent();
	else if(arg.equals("Load fuel"))
	    initiateLoadFuel();
	else if(arg.equals("Split Fleet"))
	    initiateFleetSplit();
	else if(arg.equals("Drop Slowest"))
	    initiateDropSlowest();
	else if(arg.equals("Join Fleets"))
	    initiateJoinFleets();
	else if(arg.equals("Battle Orders")) {
	    ordersPanel.initialize(this);
	    changeControlPanel(ordersPanel);
	}
    }

    public void initiateLoadFuel() {
	Enumeration e = getShips();

	while(e.hasMoreElements()) {
	    Ship s = (Ship) e.nextElement();

	    s.initiateLoadFuel();
	}

	setInfoText();
    }

    public void drawEDTCircle(ScaledGraphics g) {
	if(getOwner() != world.getController().playerNumber)
	    return;

	if(!((PHWorld)world).isEDTCircleDrawingEnabled())
	    return;

	if(edt<=0)
	    return;

	g.setColor(Color.green);
	g.drawArc(getLocation().getX()-edt,
		  getLocation().getY()-edt,
		  edt*2,
		  edt*2,
		  0,
		  360);
    }


    public void updateWarningCircle() {
	if(getOwner() != world.getController().playerNumber)
	    return;
    
	warningCircleColor=null;
    }

    public int getScanRange() {
	int maxRange=0;

	Enumeration e = getShips();

	while(e.hasMoreElements()) {
	    Ship s = (Ship) e.nextElement();
	    if(s.getScanRange() > maxRange)
		maxRange=s.getScanRange();
	}

	return maxRange;
    }

    public Enumeration getShips() {
	Vector v = new Vector();

	Enumeration e = world.getComponentsByLocation(getLocation());

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();
	    if(wc instanceof Ship) {
		if(wc.getOwner() == getOwner() &&
		   ((Ship)wc).getTrackTarget() == getId())
		    v.addElement(wc);
	    }
	}

	return v.elements();	    
    }

    public Menu getMenu() {
	Menu m = new Menu("Fleet");

	Enumeration e = getShips();

	while(e.hasMoreElements()) {
	    Ship s = (Ship) e.nextElement();
	    
	    MenuItem mi = new MenuItem(s.getName());
	    mi.addActionListener(world.getController().getGameWindow());
	    mi.setActionCommand(s.getIdString());
	    m.add(mi);
	}

	m.addSeparator();

	MenuItem di = new MenuItem("Split Fleet", 
				   new MenuShortcut(KeyEvent.VK_B));
	di.addActionListener(this);
	m.add(di);

	di = new MenuItem("Drop Slowest", 
			  new MenuShortcut(KeyEvent.VK_D));
	di.addActionListener(this);
	m.add(di);

	di = new MenuItem("Join Fleets", 
			  new MenuShortcut(KeyEvent.VK_J));
	di.addActionListener(this);
	m.add(di);

	di = new MenuItem("Battle Orders",
			  new MenuShortcut(KeyEvent.VK_O));
	di.addActionListener(this);
	m.add(di);

	return m;
    }

    protected ImageObserver getImageObserver() {
	return generalPanel;
    }

    public String getName() {
	Ship best = null;
	Enumeration e = getShips();

	while(e.hasMoreElements()) {
	    Ship hs = (Ship) e.nextElement();
	    
	    if(best == null)
		best = hs;
	    else {
		if(best.getHullTech() < hs.getHullTech())
		    best = hs;
		else if(best.getHullTech() == hs.getHullTech() &&
			best.getId()       >  hs.getId())
		    best = hs;
	    }
	}

	if(best == null)
	    return super.getName();
	else
	    return best.getTrueName() + "'s fleet"; 
    }

    public void initiateFleetSplit() {
	Enumeration e = getShips();

	while(e.hasMoreElements()) {
	    Ship s = (Ship) e.nextElement();

	    s.resetTrack();
	}

	setInfoText();
    }

    public void initiateJoinFleets() {
	Enumeration e = world.getComponentsByLocation(getLocation());

	while(e.hasMoreElements()) {
	    WorldComponent wc = (WorldComponent) e.nextElement();

	    if(wc instanceof Ship) {
		Ship s = (Ship) wc;
		Fleet f = s.getFleet();

		if(f != null && f != this) {
		    s.startTracking(this);
		}
	    }
	}
    }

    public void initiateDropSlowest() {
	Enumeration e = getShips();

	int  worstSpeed = Integer.MAX_VALUE;
	Ship worstShip  = null;

	while(e.hasMoreElements()) {
	    Ship s = (Ship) e.nextElement();

	    if(s.getSpeed() < worstSpeed) {
		worstSpeed = s.getSpeed();
		worstShip  = s;
	    }
	}

	if(worstShip != null) {
	    worstShip.resetTrack();
	}

	setInfoText();
    }

    public int getEDT() {
	return (getMinSpeed()*(getTotalFuel()/(getConsumption()+1)));
    }

    public int getMaxEDT() {
	return (getMinSpeed()*(getMaxTotalFuel()/(getConsumption()+1)));
    }

    public void sidePanelOut() {
	changeControlPanel(generalPanel);
    }
}
