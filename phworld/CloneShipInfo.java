/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import dsc.netgame.*;
import java.util.*;


public class CloneShipInfo implements java.io.Serializable {

    private int hullTech;
    private Vector components;

    private int nBuilds = 0;
    private String name;

    protected CloneShipInfo(int hullTech, String name) {
	this.name       = name;
	this.hullTech   = hullTech;
	this.components = new Vector();
    }

    public String getName() {
	return name;
    }

    public void incBuildCount() {
	nBuilds++;
    }

    public int getBuildCount() {
	return nBuilds;
    }

    public int getHullTech() {
	return hullTech;
    }

    private void addToVector(Vector v, Enumeration e) {
	while (e.hasMoreElements()) {
	    Object o = (Object) e.nextElement();
	    v.addElement(o);
	}
    }

    public Enumeration getComponents() {
	Enumeration e = components.elements();
	Vector v = new Vector();

	while(e.hasMoreElements()) {
	    ShipComponent sc = (ShipComponent) e.nextElement();
	    v.addElement(TechLevel.getComponentInstance(sc.getTechType(),
							sc.getTech()));
	}

	return v.elements();
    }

    protected void addComponent(int techType, int techLevel) {
	components.addElement(
             TechLevel.getComponentInstance(techType, techLevel));
    }

    public MaterialBundle getBuildMaterials() {
	MaterialBundle buildMaterials = 
	    new MaterialBundle(MaterialBundle.UNLIMITED);

	buildMaterials.addFullBundle(ShipHull.getBuildMaterials(hullTech));
	
	Enumeration e;

	e = getComponents();
	while (e.hasMoreElements()) {
	    ShipComponent se = (ShipComponent) e.nextElement();
	    buildMaterials.addFullBundle(
	        TechLevel.getBuildMaterials(se.getTechType(), se.getTech()));
	}

	return buildMaterials;
    }

    public int getBuildTime() {
	int time = 0;
	int nCompos = 0;

	time += ShipHull.getBuildTime(hullTech);

	Enumeration e;

	e = getComponents();
	while (e.hasMoreElements()) {
	    ShipComponent se = (ShipComponent) e.nextElement();
	    time += TechLevel.getBuildTime(se.getTechType(), se.getTech());
	    nCompos++;
	}

	int assemblyTime = HostShip.getBuildTimeByComponents(nCompos);
	assemblyTime -= nBuilds;
	if(assemblyTime < 0) 
	    assemblyTime = 0;

	time += assemblyTime;

	return time;
    }

    public MaterialBundle getFirstRoundBuildMaterials() {
	MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
	MaterialBundle buildm = getBuildMaterials();
	int time = getBuildTime();

	mb.putFuel(buildm.getFuel()/time + buildm.getFuel()%time);
	mb.putDodechadrium(buildm.getDodechadrium()/time + 
			   buildm.getDodechadrium() %time);
	mb.putMetals(buildm.getMetals()/time + buildm.getMetals() %time);
	mb.putMinerals(buildm.getMinerals()/time + buildm.getMinerals ()%time);
	mb.putOil(buildm.getOil()/time + buildm.getOil()%time);
	mb.putTroops(buildm.getTroops()/time + buildm.getTroops()%time);

	return mb;
    }

    public MaterialBundle getRestRoundsBuildMaterials() {
	MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
	MaterialBundle buildm = getBuildMaterials();
	int time = getBuildTime();

	mb.putFuel(buildm.getFuel()/time);
	mb.putDodechadrium(buildm.getDodechadrium()/time);
	mb.putMetals(buildm.getMetals()/time);
	mb.putMinerals(buildm.getMinerals()/time);
	mb.putOil(buildm.getOil()/time);
	mb.putTroops(buildm.getTroops()/time);

	return mb;
    }

    protected HostShip getNewCloneShip(HostWorld hostWorld, 
			      int owner,
			      Location location) {

	HostShip ship = new HostShip(hostWorld, owner, location);

	ShipHull sh = new ShipHull(owner, hullTech);
	ship.setComponents(sh, getComponents());

	return ship;
    }

    protected boolean canBeBuilt(TechLevel planetTechs) {
	if(planetTechs.getHullTech() < hullTech)
	    return false;

	Enumeration e = getComponents();

	while (e.hasMoreElements()) {
	    ShipComponent sc = (ShipComponent) e.nextElement();
	    if(planetTechs.getTech(sc.getTechType()) < sc.getTech())
		return false;
	}

	return true;
    }
}
