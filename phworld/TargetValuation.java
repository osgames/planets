/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;

import java.io.*;


public class TargetValuation implements Serializable, Cloneable {

    public static final int MIN_VALUE = -10;
    public static final int MAX_VALUE = 10;
    public static final int NOT_EFECTIVE = 0;

    int hullValue;
    
    int shieldValue;
    int persistentShieldValue;
    
    int hullWeaponValue;
    int shieldWeaponValue;

    int massValue;
    int volumeValue;

    boolean bombardPlanets;
    
    public TargetValuation() {
	hullValue             = -5;
	shieldValue           = -5;
	persistentShieldValue = -5;
	
	hullWeaponValue       =  5;
	shieldWeaponValue     =  3;
	
	massValue             =  0;
	volumeValue           =  0;

	bombardPlanets        = true;
    }

    int getHullValue() {
	return hullValue;
    }

    int getShieldValue() {
	return shieldValue;
    }

    int getPersistentshieldValue() {
	return persistentShieldValue;
    }

    int getHullweaponValue() {
	return hullWeaponValue;
    }

    int getShieldweaponValue() {
	return shieldWeaponValue;
    }

    int getMassValue() {
	return massValue;
    }

    int getVolumeValue() {
	return volumeValue;
    }

    boolean isBombardingEnabled() {
	return bombardPlanets;
    }

    void setHullValue(int v) {
	hullValue = v;
    }

    void setShieldValue(int v) {
	shieldValue = v;
    }

    void setPersistentShieldValue(int v) {
	persistentShieldValue = v;
    }

    void setHullWeaponValue(int v) {
	hullWeaponValue = v;
    }

    void setShieldWeaponValue(int v) {
	shieldWeaponValue = v;
    }

    void setMassValue(int v) {
	massValue = v;
    }

    void setVolumeValue(int v) {
	volumeValue = v;
    }

    void setBombarding(boolean enabled) {
	bombardPlanets = enabled;
    }

    public String toString() {
	String s = "";

	s+="Target Selection:\n";
	s+="Hull: " +  hullValue + "\n";
	s+="Shield: " + shieldValue + "\n";
	s+="Persistent Shield: " + persistentShieldValue + "\n";
	s+="Hull Weapons: " + hullWeaponValue + "\n";
	s+="Shield Weapons: " + shieldWeaponValue + "\n";
	s+="Mass: " + massValue + "\n";
	s+="Volume: " + volumeValue + "\n";

	if(bombardPlanets) {
	    s+="Bombard Planets\n";
	} else {
	    s+="Don't Bombard\n";
	}

	return s;
    }

    public int getTargetValue(int hullPointsLeft,
			      int shields,
			      int persistentShields,
			      int hullAttacks,
			      int shieldAttacks,
			      int mass,
			      int volume) {

	long div  = 0;
	long base    = 500;

	if(hullPointsLeft <= 0)
	    return 0;

	long hullV    = ((long) hullPointsLeft) * ((long) hullValue); 
	long shieldV  = (((long) shields) * ((long) shieldValue)) / 7;
	long pShieldV = (((long) persistentShields) * 
			((long) persistentShieldValue)*4) / 5;
	long massV    = ((long) mass) * ((long) massValue);
	long volumeV  = ((long) volume *2) * ((long) volumeValue);
	long hullAV   = ((long) hullAttacks) * ((long) hullWeaponValue);
	long shieldAV = ((long) shieldAttacks/4) * ((long) shieldWeaponValue);

	if(hullV >= 0)
	    base += hullV; 
	else
	    div  -= hullV;

	if(shieldV >= 0)
	    base += shieldV;
	else
	    div  -= shieldV;

	if(pShieldV >= 0)
	    base += pShieldV;
	else
	    div  -= pShieldV;

	if(massV   >= 0)
	    base += massV;
	else
	    div  -= massV;

	if(volumeV >= 0)
	    base += volumeV;
	else
	    div  -= volumeV;

	if(hullAV  >= 0)
	    base += hullAV;
	else
	    div  -= hullAV;

	if(shieldAV >= 0)
	    base += shieldAV;
	else
	    div  -= shieldAV;

	if(base == 0) {
	    base = 1;
	}

	if(div == 0) {
	    div = 1;
	}

	long rating = (base * 1000) / div;

	return (int) rating;
  }

    void validateValues() {
	if(hullValue > MAX_VALUE)
	    hullValue = MAX_VALUE;

	if(hullValue < MIN_VALUE)
	    hullValue = MIN_VALUE;

	if(shieldValue > MAX_VALUE)
	    shieldValue = MAX_VALUE;

	if(shieldValue < MIN_VALUE)
	    shieldValue = MIN_VALUE;

	if(persistentShieldValue > MAX_VALUE)
	    persistentShieldValue = MAX_VALUE;

	if(persistentShieldValue < MIN_VALUE)
	    persistentShieldValue = MIN_VALUE;

	if(hullWeaponValue > MAX_VALUE)
	    hullWeaponValue = MAX_VALUE;

	if(hullWeaponValue < MIN_VALUE)
	    hullWeaponValue = MIN_VALUE;

	if(shieldWeaponValue > MAX_VALUE)
	    shieldWeaponValue = MAX_VALUE;

	if(shieldWeaponValue < MIN_VALUE)
	    shieldWeaponValue = MIN_VALUE;

	if(massValue > MAX_VALUE)
	    massValue = MAX_VALUE;

	if(massValue < MIN_VALUE)
	    massValue = MIN_VALUE;

	if(volumeValue > MAX_VALUE)
	    volumeValue = MAX_VALUE;

	if(volumeValue < MIN_VALUE)
	    volumeValue = MIN_VALUE;
    }

    public Object clone() {
	try {
	    return super.clone();
	} catch (CloneNotSupportedException e) {
	    System.err.println("Internal error: " + e);
	}
	
	// In error case, return default valuator
	return new TargetValuation();
    }
}
