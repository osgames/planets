/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package phworld;


public class ShipEngine extends ShipComponent {

  private static int mass[] = {0,20,25,30,35,40,45,45,45};

  private static int fuel[] = {0,0,0,0,0,0,0,0,0};
  private static int dodechadrium[] = {0,0,0,0,0,0,0,0,4};
  private static int metals[] =   {0,20,25,25,25,30, 35, 40, 40};
  private static int minerals[] = {0,20,30,40,65,95,130,180,200};
  private static int oil[] =      {0,10,25,35,45,60, 75,120,120};
  private static int troops[] = {0,0,0,0,0,0,0,0,0}; 

  private static int techFuel[] = {0,0,0,0,0,0,0,0,0};
  private static int techDodechadrium[] = {0,0,0,0,0,0,0,150,300};
  private static int techMetals[]   = smallTechRequirement; 
  private static int techMinerals[] = mediumTechRequirement;
  private static int techOil[]      = highTechRequirement;
  private static int techTroops[] = {0,0,0,0,0,0,0,0,0};

  private static int powerConsumption[] = {0,11,19,22,28,36,44,35,25}; 
  private static int powerProduction[] = {0,400,700,900,1250,1750,2400,4500,9000};
  private static int damageResistance[] = {0,30,45,70,80,
					   115,145,170,300}; 

    private static String [] gadgetName = {
	"0",
	"Space",
	"Ionic",
	"Flux",
	"Gravitonic",
	"Warp",
	"Super",
	"Hyper",
	"Ultra"
    };

//CONSTRUCTOR
  public ShipEngine(int tech) {
    super(tech);
  }
 
  /** Returns the power production of the engine.
   *
   * @return Power production of the engine
   */
  public int getPowerProduction() {
    if (this.isActive()) 
      return ((getMaxPowerProduction() * 
	       (100-this.getDamagePercentage())) / 100);
    else return 0;
  } 

  public int getPowerOutput() {
    return getPowerProduction();
  }

  /** Returns the maxinum power production of this engine.
   *
   * @return Maxinum power production of the engine.
   */

  public int getMaxPowerProduction() {
    return powerProduction[techLevel];
  }

  /** Returns materials required to build the ShipEngine. 
   * getBuildMaterials() returns a MaterialBundle that has all 
   * the materials required to build the ShipEngine.
   *
   * @param tech Tech level
   * @return Materials required to build an engine of spec. tech level
   */
  public static MaterialBundle getBuildMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(fuel[tech]);
    mb.putDodechadrium(dodechadrium[tech]);
    mb.putMetals(metals[tech]);
    mb.putMinerals(minerals[tech]);
    mb.putOil(oil[tech]);
    mb.putTroops(troops[tech]);

    return mb;
  }

  /** Returns the time taken to build the ShipEngine.
   * Enignes of different tech level have varying building times. Presently,
   * the building time is directly proportional to the tech level of the
   * engine.
   *
   * @param tech Tech level
   * @return Time taken in building the ShipEngine of the spec. tech level
   */
  public static int getBuildTime(int tech) {
      return 1;
  }


  /** Returns the materials required in upgrading the ShipEngine tech level 
   * of a planet.
   *
   * @param tech Tech level
   * @return Materials required in upgrading one tech to given techlevel
   */
  public static MaterialBundle getTechMaterials(int tech) {
    MaterialBundle mb = new MaterialBundle(MaterialBundle.UNLIMITED);
    if (tech<0 || tech>8) throw new IllegalArgumentException
			    ("Non-existent tech level not between (0-8)");
    mb.putFuel(techFuel[tech]);
    mb.putDodechadrium(techDodechadrium[tech]);
    mb.putMetals(techMetals[tech]);
    mb.putMinerals(techMinerals[tech]);
    mb.putOil(techOil[tech]);
    mb.putTroops(techTroops[tech]);

    return mb;
  }

  /** Returns the power consumption of the Engine.
   * ShipEngines consume power only if the ship is moving and the engine is active.
   *
   * @return Power consumption of the ShipEngine
   */ 
  public int getPowerConsumption() {
    if (this.isActive()) return powerConsumption[techLevel];
    else return 0;
  }


  /** Returns the maximum damgage that the ShipEngine can receive without 
   * being totally destroyed.
   * 
   * @return Maximum obtainable damage
   */
  public int getMaxDamage() {
    return damageResistance[techLevel];
  }

  /** Returns the mass of the ShipEngine.
   *
   * @return Mass of the ShipEngine
   */
  public int getMass() {
    return mass[techLevel];
  }

  public String toString() {
    return "E"+techLevel+" " + gadgetName[techLevel];
  }

    public int getTechType() {
	return ShipComponent.ENGINE;
    }
}

