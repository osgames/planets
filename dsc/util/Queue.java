/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.util; 

/** Queue is a thread safe FIFO object buffer.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class Queue {

  private LinkedListNode start;
  private LinkedListNode end;
  
  /** Create empty queue.
   */

  public Queue() {
    
  }
  
  /** Add object into Queue.
   *
   * @param o Object to add as last item in queue
   */

  public synchronized void add(Object o) {
    if (isEmpty()) {
      start=end=new LinkedListNode(o);

    }
    else {
      LinkedListNode temp = new LinkedListNode(o);
      end.setNext(temp);
      end=temp;
    }      

    notify();
  }

  /** Remove oldest item from the queue.
   *
   * @return Oldest object. Null if Queue is empty.
   */

  public synchronized Object remove() {
    if (isEmpty()) {
      return (null);
    }
    else {
      LinkedListNode temp = start;
      if(start==end) {
	start=end=null;
      } else
	start=start.getNext();
      return(temp.getData());
    }
  }


  /** Waits until there is an object in the queue. Gets the oldest one
   * out. 
   *
   * @param timeoutMillis Time in milliseconds to wait for object to
   * appear, if the queue is empty. If this value is set to 0, no
   * waiting will occur.
   * @return Oldest object, null if Queue is empty after timeout. 
   */

  public synchronized Object remove(int timeoutMillis) {
    if (isEmpty()) {
      if(timeoutMillis==0)
	return (null);
      else {
	try {
	  wait(timeoutMillis);
	} catch(InterruptedException e) {
	  return (null);
	}

	if(isEmpty())
	  return (null);
      }
    }

    LinkedListNode temp = start;
    if(start==end) {
      start=end=null;
    } else
      start=start.getNext();
    return(temp.getData());
  }


  /** Check if there are more objects in the queue.
   *
   * @return True if queue is empty.
   */

  public synchronized boolean isEmpty() {
    return (start == null);
  }

}
