/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.util;

import java.util.*;

/** Insertionsort for Sortable objects.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 *
 * @see Sortable
 */

public class InsertionSort {

  /** Sorts vector into ascending order. Items in the vector must
   * implement Sortable interface.
   *
   * @param v Vector of Sortable items.
   * @see Sortable
   */

  public static void insertionSort(Vector v) {
    v.trimToSize();
    doInsertionSort(v, 0, v.size()-1);
  }

  static void doInsertionSort(Vector v, int l, int r) {
    int j, P;
    Sortable tmp;
    int N=r-l+1;

    for (P=1; P<N; P++)
      {
	tmp=(Sortable) v.elementAt(P+l);

	/* "Roll" item to left until it's final place is found */
	for (j=P; j>0 && 
	       ((Sortable) v.elementAt(j-1+l)).greaterThan(tmp) >0 ; j--)
	  {
	    v.setElementAt(v.elementAt(j-1+l), j+l);
	  }
	v.setElementAt(tmp, j+l);
      }
  }
}
