/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.util;

import java.util.*;

/** Selection sort for Sortable objects.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 *
 * @see Sortable
 */

public class SelectionSort {

  /** Sorts vector into ascending order. Items in the vector must
   * implement Sortable interface.
   *
   * @param v Vector of Sortable items.
   * @see Sortable
   */

  public static void selectionSort(Vector v) {
    Object temp;
    int i,j,s;

    v.trimToSize();
    int N=v.capacity();

    for (i=0; i<N; i++) {

      /* Scan all elements that are not in order and search the smallest */
      for(s=i, j=i+1; j<N; j++) {
	//	if( ((Sortable) v.elementAt(j)).getKey() <
	//    ((Sortable) v.elementAt(s)).getKey())
	if(((Sortable) v.elementAt(s)).greaterThan((Sortable) v.elementAt(j))
	   >0)
	  s=j;
      }
    
      /* Place the found element at it's place */
      temp=v.elementAt(i);
      v.setElementAt(v.elementAt(s), i);
      v.setElementAt(temp, s);
    }
  }
}
