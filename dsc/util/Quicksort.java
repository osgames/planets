/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.util;

import java.util.*;

/** Quicksort for Sortable objects.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 * @see Sortable
 */

public class Quicksort {

  /* Note that CUTOFF must be at least 3 (because of median-3 pivoting) */
  public static final int CUTOFF = 20;

  /** Sorts vector into ascending order. Items in the vector must
   * implement Sortable interface.
   *
   * @param v Vector of Sortable items.
   * @see Sortable
   */

  public static void quicksort(Vector v) {
    v.trimToSize();
    if(v.size()<2)
      return;

    doQuicksort(v, 0, v.size()-1);
  }  

  /* Find the median element of the leftmost, rightmost and center
   * elements of given range. Returns that element. Also sorts these
   * three elements checked in order and places the chosen pivot to the
   * right side of the area [r-1]. 
   */

  protected static Sortable median3(Vector v, int l, int r) {
    int center = (l+r) /2;

    /* Sort three elements */
    Object temp;

    if(((Sortable) v.elementAt(l)).
       greaterThan(((Sortable) v.elementAt(center))) > 0) {
      temp=v.elementAt(l);
      v.setElementAt(v.elementAt(center), l);
      v.setElementAt(temp, center);
    }

    if(((Sortable) v.elementAt(l)).
       greaterThan(((Sortable) v.elementAt(r))) > 0) {
      temp=v.elementAt(l);
      v.setElementAt(v.elementAt(r), l);
      v.setElementAt(temp, r);
    }

    if(((Sortable) v.elementAt(center)).
       greaterThan(((Sortable) v.elementAt(r))) > 0) {
      temp=v.elementAt(center);
      v.setElementAt(v.elementAt(r), center);
      v.setElementAt(temp, r);
    }
      
    /* Place pivot in the right side of the area (only the "biggest"
       of the three elements is more to the right) */

    temp=v.elementAt(center);
    v.setElementAt(v.elementAt(r-1), center);
    v.setElementAt(temp, r-1);
    
    return ((Sortable) v.elementAt(r-1));
  }

  private static void doQuicksort(Vector v, int l, int r) {
    int i, j;
    Sortable pivot;

    if ( l + CUTOFF < r) {
      pivot = median3(v, l, r);

      /* the side elements are already in order and pivot is at right*/
      i=l; j=r-1; 

      for(;;) {
	/* Search for element that is at wrong side (both directions) */
	while(((Sortable)v.elementAt(++i)).greaterThan(pivot) < 0) {}
	while(((Sortable)v.elementAt(--j)).greaterThan(pivot) > 0) {}

	/* if we haven't scanned the whole area, swap the two elements found */
	if(i<j) { 
	  Object temp=v.elementAt(i);
	  v.setElementAt(v.elementAt(j), i);
	  v.setElementAt(temp, j);
	}
	else
	  break;
      }

      /* restore pivot */
      Object temp=v.elementAt(i);
      v.setElementAt(v.elementAt(r-1), i);
      v.setElementAt(temp, r-1);

      /* Sort left and right partitions (Pivot is not included in either) */
      doQuicksort(v, l, i-1);
      doQuicksort(v, i+1, r);
    }
    else 
      {
	InsertionSort.doInsertionSort(v, l, r);
      }
  }
}
