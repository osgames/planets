/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.util; 

/** CmdLine implements methods for manipulating commmand line arguments.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class CmdLine {

    private String [] cmdlineArgs;

    public CmdLine(String [] cmdlineArgs) {
	this.cmdlineArgs = cmdlineArgs;
    }

    public int getIntOption(String option, 
			     int defaultV,
			     int min,
			    int max) {
	try {
	    String s = getStringOption(option, null);
	    if(s == null)
		return defaultV;

	    int v = Integer.parseInt(s);

	    if(v<min || v>max) {
		System.err.println("Error: value of '" + option + "' is " +
				   "out of range [" + min + "," + max + "]");
		System.exit(1);
	    }
	    
	    return v;

	} catch(NumberFormatException e) {
	    System.err.println("Error: invalid command line setting for '" +
			       option + "'");
	    System.exit(1);
	}

	return defaultV;
    }

    public double getDoubleOption(String option, 
				  double defaultV,
				  double min,
				  double max) {
	try {
	    String s = getStringOption(option, null);
	    if(s == null)
		return defaultV;

	    double v = Double.valueOf(s).doubleValue();

	    if(v<min || v>max) {
		System.err.println("Error: value of '" + option + "' is " +
				   "out of range [" + min + "," + max + "]");
		System.exit(1);
	    }
	    
	    return v;

	} catch(NumberFormatException e) {
	    System.err.println("Error: invalid command line setting for '" +
			       option + "'");
	    System.exit(1);
	}

	return defaultV;
    }

    public String getStringOption(String option,
				  String defaultV) {
	if(cmdlineArgs == null)
	    return defaultV;

	/* note that we don't scan the last option,
	   because we need an argument option */

	for(int i=0; i<cmdlineArgs.length-1; i++) {
	    if(cmdlineArgs[i].equals(option)) {
		return cmdlineArgs[i+1];
	    }
	}

	return defaultV;
    }


    public boolean getBooleanOption(String option) {
	if(cmdlineArgs == null)
	    return false;

	for(int i=0; i<cmdlineArgs.length; i++) {
	    if(cmdlineArgs[i].equals(option)) {
		return true;
	    }
	}

	return false;
    }
}
