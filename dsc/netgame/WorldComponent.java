/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.Serializable;
import java.awt.*;
import dsc.awt.*;


public abstract class WorldComponent implements Serializable {

  protected Location location;
  protected String   name;
  protected int      owner;
  protected int      id;
  private   int      scanRange;
  private   int      round;
  protected World    world;

  public WorldComponent(int id, String name, int owner, Location location) {
    this.location=(Location) location.clone();
    this.name=name;
    this.owner=owner;
    this.id=id;
  }

  public Panel getControlPanel() {
    return new Panel();
  }

  public Menu getControlMenu() {
    return (null);
  }
     
  public String getMapImage() {
    return (null);
  }

  public Location getLocation() {
    return location;
  }

  public String getName() {
    return name;
  }

  public int getOwner() {
    return owner;
  }

  public int getScanRange() {
    return scanRange;
  }

  public void setScanRange(int r) {
    scanRange=r;
  }

  public boolean isVisible() {
    return false;
  }

  public int getId() {
    return id;
  }

  public String getIdString() {
    return "WC-ID-" + getId();
  }

  public void setRound(int r) {
    round=r;
  }

  public int getRound() {
    return round;
  }

  public void setWorld(World w) {
    world=w;
  }

  public World getWorld() {
    return world;
  }

  public boolean paintToForeground() {
    return true;
  }

  public void locationSelected(Location l) {}

  public void locationShiftSelected(Location l) {}

  public void componentSelected(WorldComponent wc) {}

  public int getBearing() {
    return 0;
  }

  public void pointClicked(Location l) {}

  public void pointShiftClicked(Location l) {}

  public void drawAdditionalGraphs(ScaledGraphics g) {}

  public void drawAdditionalPreGraphs(ScaledGraphics g) {}

  public void clientsideInitialization() {}

  public Menu getMenu() { return null; }

  public boolean isOwnedByMe() {
      return (world.getController().playerNumber == getOwner());
  }

  public String getTrueName() {
      return getName();
  }
}
