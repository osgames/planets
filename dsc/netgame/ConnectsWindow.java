/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.io.*;

public class ConnectsWindow extends CloseableLayoutableFrame 
    implements ItemListener,ActionListener {

  private TextField ip, port, name, password;
  private Choice game;
  private WorldCreator worldCreator;
  GridBagLayout gb;
  Button okButton, quitButton, helpButton;
  Checkbox hostBox, clientBox, aiBox;

  private Checkbox addCheckbox(GridBagConstraints gbc,int x,int y, int w, int h, 
			  String name,CheckboxGroup g,boolean v) {
    Checkbox c=new Checkbox(name,g,v);
    c.addItemListener(this);
    add(c,gbc,x,y,w,h);
    return c;
  }

  public ConnectsWindow(WorldCreator worldCreator) {

    this.worldCreator=worldCreator;

    setTitle(worldCreator.getWorldProperties().getName("ConnectsWindow"));
    setSize(320,300);    
    
    gb = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();
    setLayout(gb);

    gbc.fill=GridBagConstraints.NONE;
    gbc.weightx=100;
    gbc.weighty=100;

    Image i=GraphicsLoader.getTransparentImage("data/graph/still/planet1.gif");
    ImageComponent ic= new ImageComponent(i);
    add(ic,gbc,0,0,3,3);

    i=GraphicsLoader.getImage("data/graph/still/icons/connections.gif");
    setIconImage(i);
    

    ip=new TextField("127.0.0.1",16);
    port=new TextField("9090",6);
    name=new TextField(16);
    password=new TextField(16);
    game=new Choice();

    getLoadableGames();
    game.setEnabled(false);

    Label nameL = new Label("Name");
    Label passL = new Label("Password");
    Label ipL = new Label("Host");
    Label portL = new Label("Port");
    Label gameL = new Label("Game");

    add(nameL,gbc,4,1,2,2);
    add(passL,gbc,4,3,2,2);
    add(ipL,gbc,4,5,2,2);
    add(portL,gbc,4,7,2,2);
    add(gameL,gbc,4,9,2,2);

    gbc.anchor=GridBagConstraints.WEST;
    add(name,gbc,6,1,3,2);
    add(password, gbc, 6,3,3,2);
    add(ip,gbc,6,5,3,2);
    add(port,gbc,6,7,3,2);
    add(game,gbc,6,9,3,2);
    gbc.anchor=GridBagConstraints.CENTER;

    okButton=createButton(" OK ",this);
    helpButton=createButton("Help",this);
    quitButton=createButton("Quit",this);
    
    add(quitButton,gbc,0,11,3,2);
    add(helpButton,gbc,3,11,3,2);
    add(okButton,gbc,6,11,3,2);

    CheckboxGroup g=new CheckboxGroup();
    hostBox=addCheckbox(gbc,0,3,4,2,"Host",g,false);    
    clientBox=addCheckbox(gbc,0,5,4,2,"Client",g,true);
    aiBox=addCheckbox(gbc,0,7,4,2,"AI",g,false);

    loadDefaults();
  }

  public void getLoadableGames() 
  {
    game.add("<-New game->");
    File d = new File(".");

    String s [] =d.list();

    for (int i = 0; i < s.length; i++){

	if (s[i].endsWith(worldCreator.getWorldOptionsUI().
			  getOptions().getSaveGameSuffix()))
      game.add(s[i]);
    }
    
  }

    
  public void itemStateChanged(ItemEvent evt) {
    if(evt.getStateChange()==ItemEvent.DESELECTED) return;
 
    if(evt.getItem().equals("Host")){
      ip.setEnabled(false);
      port.setEnabled(false);
      password.setEnabled(false);
      game.setEnabled(true);
    }
    else if (evt.getItem().equals("Client") ||
	     evt.getItem().equals("AI")) {
      ip.setEnabled(true);
      port.setEnabled(true);
      password.setEnabled(true);
      game.setEnabled(false);
    }
    
    gb.layoutContainer(this);
    repaint();
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();
    
    if(arg.equals("Quit")) {
      QuitQueryWindow w = new QuitQueryWindow(this);
      w.show();
    } 
    else if(arg.equals(" OK ")) {
      if(clientBox.getState() || aiBox.getState()) {
	ClientController c = new ClientController(ip.getText().trim(),
		    Integer.valueOf(port.getText().trim()).intValue(),
						  name.getText(), 
						  password.getText(), 
						  worldCreator);

	if(aiBox.getState()) {
	    c.setAI(new phworld.ai.defender.AI());
	}
	c.start();
      } else {
	String saveGameFile = game.getSelectedItem();
	if(saveGameFile.equals("<-New game->"))
	  saveGameFile = null;

	HostController c = new HostController(name.getText(),
					      worldCreator, 
					      true,
					      saveGameFile);
	c.start();
      }
      setVisible(false);
      dispose();
    } 
    else if (arg.equals("Help")) {
      HelpWindow.callForHelp("data/help", "Window", "Connections");
    }
    
  }

  private void loadDefaults() {
  } 
}
