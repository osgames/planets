/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Message from Host's chatter to Client. Contains information 
 * of message's original source.
 *
 * @author Dodekaedron Software Creations, Inc. -- Lexa
 */

public class ChatMessage extends Message {
  
  /**
   * @param to Player ID of message's receiver.
   * @param from Player ID of message's sender. (-1 is host)
   * @param str Text of the message.
   */

  ChatMessage(int to,int from,String str) {
    to_player=to;
    from_player=from;
    msg=str;
  }

  /** Gets message's receiver.
   *
   * @return Player ID of message's receiver.
   */
  public int whoTo() {return(to_player);}

  /** Gets message's sender.
   *
   * @return Player ID of message's sender.
   */

  public int whoFrom() {return(from_player);}
  
  /** Gets message's text.
   *
   * @return Message's text.
   */
   
  public String getMessage() {return(msg);}

  /** Message's text */
  protected String msg;
  
  /** Sender */
  protected int from_player;
  
  /** Receiver */
  protected int to_player;

}


