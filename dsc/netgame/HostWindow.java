/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;


public class HostWindow extends LayoutableFrame implements ActionListener,
               HostScreen {

  private TextArea  outputText;
  private Button helpButton, startGameButton, quitButton;
  private GridBagLayout gb;
  public static HostController controller;

  // Custom mutex to avoid conflicts with AWT synchronized methods
  private Integer mutex = new Integer(0);

  public void putMessage(String s) {
      synchronized (mutex ) {
	  outputText.append(s + "\n");
	  try {
	      outputText.setCaretPosition(outputText.getText().length());
	  } catch (IllegalComponentStateException e) {}
      }
  }

  public HostWindow(HostController controller, String winName, 
		    boolean newGame) {
    this.controller=controller;
    setTitle(winName);
    setSize(300,400);

    gb = new GridBagLayout();
    GridBagConstraints gbc = new GridBagConstraints();
    setLayout(gb);

    gbc.fill=GridBagConstraints.BOTH;
    gbc.weightx=100;
    gbc.weighty=100;
    
    Image i=GraphicsLoader.getImage("data/graph/still/icons/host.gif");
    setIconImage(i);
    
    outputText = new TextArea("Host started\n\n",20,50,TextArea.SCROLLBARS_VERTICAL_ONLY);
    outputText.setEditable(false);

    add(outputText,gbc,0,0,9,20);

    quitButton=createButton("Quit",this);
    helpButton=createButton("Help",this);
    startGameButton=createButton("Start Game",this);

    if(!newGame)
	startGameButton.setLabel("Continue");
    
    gbc.weighty=0; gbc.fill=GridBagConstraints.HORIZONTAL;
    add(startGameButton,gbc,6,21,2,1);
    add(helpButton,gbc,3,21,2,1);
    add(quitButton,gbc,0,21,2,1);
  }

  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();

    if(arg.equals("Quit")) {
      QuitQueryWindow w = new QuitQueryWindow(this);
      w.show();
    } 
    else if (arg.equals("Start Game")) {
      startGameButton.setLabel("Pause Game");
      controller.startGame();
    }
    else if (arg.equals("Help")) {
      HelpWindow.callForHelp("data/help", "Window", "Host");
    } else if (arg.equals("Pause Game")) {
      pauseGame();
      startGameButton.setLabel("Continue");
    } else if (arg.equals("Continue")) {
      continueGame();
      startGameButton.setLabel("Pause Game");
    }
  }

  private void pauseGame() {
    HostWorld world = controller.getWorld();
    world.pauseGame();
  }

  private void continueGame() {
    HostWorld world = controller.getWorld();
    world.continueGame();
  }
}



