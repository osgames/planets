/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;
import java.util.*;


/**
 * Alliance information handling class. Stores alliance settings for
 * one player.
 *
 *@author Dodekaedron Software Creations, Inc. -- Wraith 
 */

public class Alliance implements Serializable, Cloneable {
 
  private BitSet allies;

  /**
   * Creates new alliance setting which defaults to no alliances.
   *
   * @param owner Whose settings these are.
   */

  public Alliance(int owner) {
    allies = new BitSet(GlobalDefines.maxPlayers);
    allies.set(owner);
  }


  private Alliance(BitSet allies) {
    this.allies = allies;
  }

  protected void validate(int owner) {
      if(allies == null ||
	 allies.size() < GlobalDefines.maxPlayers ||
	 !allies.get(owner)) {
	  System.err.println(
	    "Warning: Got invalid alliance message from player " + owner);
	  allies = new BitSet(GlobalDefines.maxPlayers);
	  allies.set(owner);
      }
  }
  
  /**
   * Checks whether given player is an ally according to the owner
   * of this alliance setting.
   *
   * @param player Player to check for alliance.
   */

  public boolean isAlly(int player) {
    if(player == -1)
      return false;

    return allies.get(player);
  }

  /**
   * Sets alliance status for given player.
   *
   * @param isAlly Whether given player is considered as an ally.
   * @param player The player whose setting is to be set.
   */

  public void setAlly(int player, boolean isAlly) {
    if(isAlly) {
      allies.set(player);
    } else {
      allies.clear(player);
    }
  }

  public Object clone() {
    return new Alliance((BitSet) allies.clone());
  }
}
