/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Message from hostside world to clientside world. 
 * All world messages contain information about current round.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public abstract class HostWorldMessage extends Message {

  private int destination;
  private int round;

  /** New message to player whose id is given. Note that setRound
   * should be called to set the round number before the message is sent.
   *
   * @param dest Receiver's player ID
   * @see #setRound
   */

  public HostWorldMessage(int dest) {
    destination=dest;
  }

  /** Gets message's receiver.
   *
   * @return Receiver's player ID
   */

  public int whoTo() {
    return destination;
  }

  /** Sets round information in the message. Round must be set to keep
   * host and client in sync.
   *
   * @param r Current round.
   */

  public void setRound(int r) {
    round=r;
  }

  /** Gets round when this message was created.
   *
   * @return Round when the message was created.
   */

  public int getRound() {
    return round;
  }

}
