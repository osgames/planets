/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.Serializable;
import dsc.util.*;


/** Location represents map coordinates. This class can 
 * be used to specify component locations or waypoints.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class Location implements Serializable, Cloneable {
  
    private int x;
    private int y;	

    /** Creates location object with (0,0) coordinates.
     */

    public Location() {}

    /** Creates new location with given coordinates.
     *
     * @param x Horizontal coordinate (positive right)
     * @param y Vertical coordinate (positive down)
     */

    public Location(int x, int y) {
	this.x=x;
	this.y=y;
    }

    /** Get x-coordinate.
     *
     * @return x-coordinate
     */

    public int getX() { return x; }

    /** Get y-coordinate.
     *
     * @return y-coordinate
     */

    public int getY() { return y; }

    /** Set x-coordinate. 
     *
     * @param x New X-coordiante
     */

    public void setX(int x) { this.x=x; }

    /** Set y-coordinate. 
     *
     * @param y New Y-coordiante
     */

    public void setY(int y) { this.y=y; }

    /** Checks whether this and given Location represent same
     * location.
     *
     * @param l Location to compare this Location.
     * @return True if coordinates are same
     */

    public boolean equals(Location l) {
	if(l == null)
	    return false;

	return(x==l.getX() && y==l.getY());
    }

    /** Calculates distance between locations.
     *
     * @param l Location to calculate distance from this one.
     * @return Distance between locations.
     */

    public int distance(Location l) {
	return ((int) Math.sqrt((x-l.getX())*(x-l.getX()) +
				(y-l.getY())*(y-l.getY())));
    }

    public Object clone() {
	return (new Location(x,y));
    }

    /** Calculates bearing to given location. 0 is upwards and 
     * bearing increases clockwise. Bearings are between [0,359].
     *
     * @param l Location into which heading is calculated.
     * @return Bearing from this component to l.
     */

    public int getBearingTo(Location l) {
	int yd, xd;
	yd=getY()-l.getY();
	xd=l.getX()-getX();

	if(xd==0) {
	    if(yd < 0)
		return 180;
	    else
		return 0;
	}
    
	if(yd==0) {
	    if(xd > 0)
		return 90;
	    else
		return 270;
	}

	int b=0;
	double t;

	// Upper halfplane
	if(yd>0) {
	    t= (Math.atan(((double) xd)/ ((double)yd))/Math.PI*180.0);
	    b=(int) t;
	    if(b<0) b+=360;
	    if(b>360) b-=360;
	    if(b==360) b=0;
      
	} else {
	    t= (180.0 + Math.atan(((double) xd)/ ((double)yd))/Math.PI*180.0);
	    b=(int) t;
	    if(b<0) b+=360;
	    if(b>360) b-=360;
	    if(b==360) b=0;
	}

	return b;
    }

    /** Moves location at most given amount towards given location.
     * If distance between locations is lesser than given movement,
     * move stops at given location. Note that method returns new
     * Location object representing new coordinates and <BOLD>doesn't</BOLD>
     * change itself. 
     *
     * @param towards Location towards movement is wanted to happen.
     * @param amountToMove Maxinum amount of movement.
     * @return New Location object that represent the new location.
     */

    public Location moveTowards(Location towards, int amountToMove) {
	int d=distance(towards);

	if(d <= amountToMove)
	    return ((Location) towards.clone());
    
	int nx, ny;

	nx=getX()+(((towards.getX()-getX())*amountToMove)/d);
	ny=getY()+(((towards.getY()-getY())*amountToMove)/d);
    
	return new Location(nx,ny);
    }

    /** Moves location at given amount towards given bearing. Note that
     * method returns new Location object representing new coordinates
     * and <BOLD>doesn't</BOLD> change itself.
     *
     * @param bearing Bearing towards movement is wanted to happen (0 up).
     * @param amountToMove Amount of movement.
     * @return New Location object that represent the new location. */


    public Location moveTowards(int bearing, int amountToMove) {
	int nx, ny;

	double radianBearing = ((double)(bearing+90))*Math.PI/180.0;

	nx=getX()+((int) (Math.cos(radianBearing)*(double) amountToMove));
	ny=getY()-((int) (Math.sin(radianBearing)*(double) amountToMove));
    
	return new Location(nx,ny);
    }

    /** Rotates this location around the given centerpoint.
     *
     * @param  center    Centerpoint of rotation
     * @param  speed     Speed of rotation in tenths of pixexs
     * @return New Location object that represent the new location. 
     */

    public Location rotateAroundCenter(Location center, int speed) {
	int nlx, nly;

	int r = distance(center);
	if(r < 20) {
	    return (Location) clone();
	} else {
	    r*=10.0;
	    double theta = (double) speed / (double) r;

	    nlx = (int) 
		(((double) (getX() - center.getX())) * Math.cos(theta) -
		 ((double) (getY() - center.getY())) * Math.sin(theta) +
		 ((double) center.getX()));

	    nly = (int) 
		(((double) (getX() - center.getX())) * Math.sin(theta) +
		 ((double) (getY() - center.getY())) * Math.cos(theta) +
		 ((double) center.getY()));

	    return new Location(nlx, nly);
	}
    }
    
    public String toString() {
	return "(" + x + "," + y + ")";
    }
}
