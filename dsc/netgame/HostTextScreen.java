/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.util.*;
import java.io.*;


public class HostTextScreen extends Thread implements HostScreen {

  public static HostController controller;
  private String name;
  private boolean newGame;

  public void putMessage(String s) {
    System.out.println(s);
  }

  public HostTextScreen(HostController controller, String winName,
			boolean newGame) {
    this.controller=controller;
    name=winName;
    this.newGame=newGame;
  }

  public void run() {
    BufferedReader stdin = new BufferedReader(new
    	InputStreamReader(System.in));

    try {
	if(newGame) {
	    stdin.readLine();
	    controller.startGame();
	} else {
	    stdin.readLine();
	    continueGame();
	}

      while(true) {
	stdin.readLine();
	pauseGame();
	stdin.readLine();
	continueGame();
      }
    } catch (Exception e) {System.err.println(e); System.exit(1);}
  }

  private void pauseGame() {
    HostWorld world = controller.getWorld();
    world.pauseGame();
  }

  private void continueGame() {
    HostWorld world = controller.getWorld();
    world.continueGame();
  }
}



