/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Message from host telling that new round will start. 
 * Contains all components that player is aware of.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class StartRoundMessage extends HostWorldMessage {

  private WorldComponent[] playerComponents;
  private Alliance alliance;
  private int roundTime;
  private WorldState worldState;

  /** Create message with components. Remember to set round
   * information.
   *
   * @param dest Player ID of the receiver.
   * @param wct Table of WorldComponents visible to this player.
   * @param rt Lenght of next round in milliseconds.
   * @see HostWorldMessage#setRound
   */

  public StartRoundMessage(int dest, 
			   WorldComponent[] wct, 
			   Alliance alliance,
			   int rt,
			   WorldState ws) {

    super(dest);
    
    this.alliance=alliance;
    playerComponents=wct;
    roundTime=rt;
    worldState=ws;
  }

  /** Gets component table.
   *
   * @return Table of all components visible to this player.
   */

  public WorldComponent[] getComponents() {
    return playerComponents;
  }

  /** Gets lenght of current round.
   *
   * @return Lenght of current round in milliseconds.
   */

  public int getRoundTime() {
    return roundTime;
  }

  /**
   * Sets the length of the current round
   *
   * @time Length of current round in milliseconds.
   */

  protected void setRoundTime(int time) {
    roundTime = time;
  }

  /**
   * Gets alliance information for this player.
   *
   * @return alliance information
   */

  public Alliance getAlliance() {
    return alliance;
  }

  /**
   * Gets current world state.
   *
   * @return World state for this round.
   */

  public WorldState getWorldState() {
    return worldState;
  }
}
