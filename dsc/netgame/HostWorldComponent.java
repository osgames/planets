/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


public abstract class HostWorldComponent implements java.io.Serializable {

  protected HostWorld hostWorld;
  protected Location location;
  protected int id;
  protected int owner;
  protected int scanRange;
  protected String name;

  public HostWorldComponent(HostWorld hw, int owner, Location location) {
    hostWorld=hw;
    this.location=(Location) location.clone();
    this.owner=owner;
  }

  public void doRound() {}

  public abstract void putMessage(WorldComponentMessage m);
  public abstract WorldComponent getWorldComponent(int player);

  public int getScanRange() {
    return scanRange;
  }

  public double getScanRangeDivisor() {
	return (double) 1.0;
  }

  public Location getLocation() {
    return (Location) location.clone();
  }

  public void setLocation(Location l) {
    location=(Location) l.clone();
  }

  public int getId() {
    return id;
  }

  public int getOwner() {
    return owner;
  }

    public void setOwner(int player) {
	owner = player;
    }

  void setId(int i) {
    id=i;
  }

  public void setName(String n) {
    name=n;
  }

  public boolean isVisible() {
    return false;
  }

  public String getName() {
    return name;
  }

  public int getRadius() {
    return 0;
  }
}
