/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.net.*;
import dsc.util.Queue;


class ControllerSocket extends Thread {

  private ServerSocket serverSocket;
  private Queue socketQueue;
  private HostScreen hostWindow;

  ControllerSocket(ServerSocket serverSocket, Queue socketQueue, HostScreen hostWindow) {
    this.serverSocket=serverSocket;
    this.socketQueue=socketQueue;
    this.hostWindow=hostWindow;
  }

  public void run() {
    Socket s;
    InetAddress i;

    hostWindow.putMessage("Accepting new connections");
    while(true) {

      try {
	s = serverSocket.accept();
	i=s.getInetAddress();
      } catch (Exception e) {continue;}
      hostWindow.putMessage("New connection from " + i);
      socketQueue.add(s);

      try {
	sleep(300);
      } catch (Exception e) {}
    }
  }

}
