/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.util.BitSet;


/** Message from Client to Hosts's chatter.
 *
 * @author Dodekaedron Software Creations, Inc. -- Lexa
 */

public class ChatClientMessage extends ChatMessage {

  /** Create new message with given text. Message receivers bits
   * must be set in the BitSet.
   *
   * @param to Message's receivers.
   * @param str Message text.
   */

  public ChatClientMessage(BitSet to,String str, String preChar) {
    super(0,0,str);
    to_players=to;
    this.preChar=preChar;
  }

  /** Gets the ID of the sender of the message.
   *
   * @return Player ID of the sender.
   */

  public int playerFrom() {return(getSource());}

  /** Gets receiver BitSet.
   *
   * @return Representation of the receivers.
   */

  public BitSet playersTo() {return(to_players);}
  
  public String getPreChar() {
    if(preChar!=null)
      return preChar;
    else
      return "*";
  }

    public boolean validatePreChar() {
	if(getPreChar().startsWith("[")) {
	    return false;
	}
	return true;
    }

  /** BitSet that contains information who is going to get this 
   * message from Host chatter.
   */
  protected BitSet to_players;

  /** PreChar is added before the actual message in the host.
   */
  private String preChar;
}









