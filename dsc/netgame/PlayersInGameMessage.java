/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Message that contains information about all players in the game.
 * Host should send this message to all players when someone joins or
 * quits the game or when someone changes name. Receiver of this message
 * should update it's player information.
 *
 * @see PlayersInfo
 */

public class PlayersInGameMessage extends ControlMessage {

  /** Information of all players in the game. */
  public PlayersInfo players;

  /** Create new player information message. 
   *
   * @param p Information of all players in the game.
   **/
  PlayersInGameMessage(PlayersInfo p) {
    players=p;
  }

  /** Gets information of all players in the game.
   *
   * @return All players in the game
   */

  public PlayersInfo getPlayers() {
    return players;
  }
}
