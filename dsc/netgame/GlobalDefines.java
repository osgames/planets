/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.Color;


public class GlobalDefines {

  /** Maxinum number of player in the game. Note that this number
   * is restricted by the amount of color combinations.
   */

  public static final int maxPlayers=78;

  /** Color used to indicate WorldComponent which does not (yet)
   * have an owner.
   */

  private static Color hostColor = Color.gray;

    private static int MAX_CHATLINE_LENGTH = 300;

  /** Colors used in player color combinations.
   */

  private static int NUMBER_OF_COLORS = 12;

  private static Color [] colors = {
    Color.blue,
    Color.red,
    Color.green,
    Color.cyan,
    Color.orange,
    Color.pink,
    Color.yellow,
    Color.magenta,
    Color.white,
    Color.green.darker().darker().darker(),
    Color.darkGray,
    Color.blue.darker().darker().darker()
  };

  /** Gets color for unowned components. 
   *
   * @return Color for components owned by host. (unowned)
   */

  public static Color getHostColor() {
    return hostColor;
  }

  /** Gets primary color for given player. Note that primary and
   * secondary colors can be same. Note also that no two players
   * should have same colors as primary or secondary colors. 
   * (Ie. (white/red), (red/white) is not allowed)
   *
   * @param player Player number for requester. Argument must be
   * between 0 and maxPlayers-1.
   * @return Primary color for given player.
   * @see #maxPlayers
   */

  public static Color getPrimaryColor(int player) {
    if(player==-1)
      return getHostColor();

    if(player<NUMBER_OF_COLORS)
      return colors[player % NUMBER_OF_COLORS];
    
    if(player<(NUMBER_OF_COLORS+
		(NUMBER_OF_COLORS/2)*(NUMBER_OF_COLORS/2)))
      return colors[player % (NUMBER_OF_COLORS/2)];

    int nro = player-NUMBER_OF_COLORS-
      (NUMBER_OF_COLORS/2)*(NUMBER_OF_COLORS/2);
    
    int p=nro%(NUMBER_OF_COLORS/2);
    int s=nro/(NUMBER_OF_COLORS/2);

    if(p>s) {
      p=(NUMBER_OF_COLORS-1)-p;
    }

    return colors[p];
  }

  /** Gets secondary color for given player. Note that primary and
   * secondary colors can be same. Note also that no two players
   * should have same colors as primary or secondary colors. 
   * (Ie. (white/red), (red/white) is not allowed)
   *
   * @param player Player number for requester. Argument must be
   * between 0 and maxPlayers-1.
   * @return Secondary color for given player.
   * @see #maxPlayers
   */

  public static Color getSecondaryColor(int player) {

    if(player<NUMBER_OF_COLORS)
      return getPrimaryColor(player);

    if(player<(NUMBER_OF_COLORS+
	       (NUMBER_OF_COLORS/2)*(NUMBER_OF_COLORS/2)))
      return colors[(player-NUMBER_OF_COLORS) / (NUMBER_OF_COLORS/2) + (NUMBER_OF_COLORS/2)];

    int nro = player-NUMBER_OF_COLORS-
      (NUMBER_OF_COLORS/2)*(NUMBER_OF_COLORS/2);

    int p=nro%(NUMBER_OF_COLORS/2);
    int s=nro/(NUMBER_OF_COLORS/2);

    if(p==s) {
      s=(NUMBER_OF_COLORS/2)-1;
    }    
    else if(p>s) {
      s=(NUMBER_OF_COLORS-1)-s;
    }

    return colors[s];
  }

  /**
   * Tells whether given string is a valid name to use in the
   * game.
   *
   * @param name The name that player is trying to use
   * @return True if the name is OK (not too long and no special characters)
   */

  public static boolean isNameValid(String name) {
    if(name==null)
      return false;

    if(name.length()>16)
      return false;

    if(name.length()<2)
      return false;

    for(int i=0; i<name.length() ; i++)
      if(!isCharValid(name.charAt(i)))
	return false;

    return true;
  }

  /**
   * Tells whether given character is valid for use in names.
   *
   * @param c Character to examine
   * @return True if character is ok.
   */

  private static boolean isCharValid(char c) {

    if(Character.isLetterOrDigit(c))
      return true;

    if(c == ' ')
      return true;

    return false;
  }

  public static boolean isAccaptableChatString(String s) {
      if(s.length() >= MAX_CHATLINE_LENGTH) {
	  return false;
      }

      for(int i=0;i<s.length();i++) {
	  if(Character.isISOControl(s.charAt(i))) {
	      return false;
	  }
      }

      return true;
  }
}
