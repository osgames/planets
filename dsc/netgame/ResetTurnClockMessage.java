/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** Message from host telling when a new round will start. 
 */

public class ResetTurnClockMessage extends HostWorldMessage {

  private int roundTime;

  /** Create message with components. Remember to set round
   * information.
   *
   * @param dest Player ID of the receiver.
   * @param rt Lenght of next round in milliseconds.
   * @see HostWorldMessage#setRound
   */

  public ResetTurnClockMessage(int dest, int rt) {
    super(dest);
    roundTime=rt;
  }

  /** Gets lenght of current round.
   *
   * @return Lenght of current round in milliseconds.
   */

  public int getRoundTime() {
    return roundTime;
  }
}
