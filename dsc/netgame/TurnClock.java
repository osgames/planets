/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.util.*;
import java.awt.*;


/** Clock that runs in the game window. Tells player amount
 *  of time left during this round.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

class TurnClock extends java.lang.Thread {

  private Label turnLabel;
  private Date lastTime;
  private Date startTime;
  private int turnTime;
  private boolean turnHasEnded;

  // Custom mutex to avoid conflicts with AWT synchronized methods
  private Integer mutex = new Integer(0);

  public TurnClock(Label turnLabel) {
    this.turnLabel=turnLabel;
    lastTime=new Date();
    startTime=new Date();
  }

  public void turnEndPressed() {
      synchronized (mutex) {
	  turnHasEnded=true;
      }
  }

  public void resetTime(int turnTime) {
      synchronized (mutex) {
	  this.turnTime=turnTime;
	  this.turnHasEnded=false;
	  lastTime.setTime(System.currentTimeMillis());
      }
  }

  public void incTime(int inc) {
      synchronized (mutex) {
	  this.turnTime+=inc;
      }
  }

  public long getTimeLeft() {
      synchronized (mutex) {
	  return (turnTime - (System.currentTimeMillis() - lastTime.getTime()));
      }
  }

  public void run () {

    while(true) {
      long timeLeft = getTimeLeft()/1000;
      String s;

      if(timeLeft<0)
	timeLeft=0;
      
      if(turnHasEnded)
	turnLabel.setForeground(Color.blue);
      else if(timeLeft<30)
	turnLabel.setForeground(Color.red);
      else
	turnLabel.setForeground(Color.black);

      long minutes = timeLeft / 60;
      long  seconds = timeLeft % 60;

      if(seconds < 10)
	s = "" + minutes + ":0" + seconds;
      else
	s = "" + minutes + ":" + seconds;

      long totalTime = (System.currentTimeMillis() - startTime.getTime())/1000;
      minutes = totalTime/60;
      seconds = totalTime%60;

      s+=" [";
      if(seconds < 10)
	s += "" + minutes + ":0" + seconds;
      else
	s += "" + minutes + ":" + seconds;
      s+="]";

      turnLabel.setText(s);

      try {
	sleep(1000);
      } catch (Exception e) {}
    }
  }
}
