/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;
import java.util.*;


/** NameDatabase creates unique names from list in a file.
 * Database file should have one name per line and shouldn't contain
 * blank lines. Database uses names in the file, and when it runs out of
 * names it starts to append numbers after the names. (Name-2, Name-3 and
 * so on.)
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class NameDatabase implements Serializable {

  private int index;
  private Vector names;
  private String name;
  private int repetitionCounter;

  /** Initializes database with names in a given file.
   *
   * @param fname Filename of the database (text) file.
   */

  public NameDatabase(String fname) {

    BufferedReader in=null;
    names = new Vector();
    name=fname;

    try {
      File f   = new File(fname);
      in=new BufferedReader(new FileReader(f));
    } catch (Exception e) {
      System.err.println("Unable to load name database: " +fname); System.exit(1);}

    String s;

    try {
      do {
	s = in.readLine();
	if(s!=null)
	  names.addElement(s);
      } while(s != null);
    } catch (IOException e) {}

    try {
      in.close();
    } catch(IOException e) {}

    index=0;
    mixVector();
    repetitionCounter=1;
  } 

  private int rand(int n) {
    int N = (int) (Math.random() * ((double) n));
    if(N>=n)
      N=n-1;

    return N;
  }

  private void swapTwo(int a, int b) {
    Object tmp;

    tmp=names.elementAt(a);
    names.setElementAt(names.elementAt(b) ,a);
    names.setElementAt(tmp, b);
  }

  private void mixVector() {
    for(int i=0; i<(names.size()-1); i++) {
      swapTwo(i, rand(names.size()-i)+i);
    }
  }

  private String formNewName(String base) {
    if(repetitionCounter!=1)
      return (base + "-" + repetitionCounter);
    else
      return base;
  }

  /** Returns unique name based on names in the database. 
   * Adds numbers after names if namespace is exhausted.
   *
   * @return Unique name
   */

    public String getNewName() {
	if(index < names.size()) {
	    return formNewName((String) names.elementAt(index++));
	} else {
	    index=0;
	    repetitionCounter++;
	    if(index < names.size())
		return formNewName((String) names.elementAt(index++));
	    else
		return formNewName("NoNamesInDB");
	}
    }
}
