/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.util.*;
import java.io.*;
import dsc.util.Queue;


public abstract class HostWorld implements java.lang.Runnable, 
					   java.io.Serializable {
  
    private static int TIMEOUT_MULTIPLIER = 10;
    private static int MAX_TIMEOUT        = 120000; /* milliseconds */

  private Queue inputQueue;
  private Queue outputQueue;
  private HostController hostController;
  protected HostScreen hostWindow;
  protected Hashtable hostWorldComponents;
  private int lastComponentId;
  private int currentRound;
  private Date lastRunTime;
  private boolean turnEnded[];
  private int timeoutTimes[];
  private Alliance alliances[];
  protected WorldState worldState[];
  protected WorldOptions worldOptions;
  private Scoreboard scoreboard;
  private int roundTime;
  private boolean gameIsPaused=false;
  private StartRoundMessage startRoundMessages[];

  /** For complete game status saving, not used except in game loading */ 
  private PlayersInfo oldPlayersInfo;

  /**
   * Checks whetger given player has alliance enabled to other player.
   *
   * @param whose Whose alliance settings are checked.
   * @param who   The target of the query.
   * @return True if who is whose's ally by whose's settings.
   */

  public boolean isAlly(int whose, int who) {
    if(whose==-1)
      return false;

    return alliances[whose].isAlly(who);
  }

  public Alliance [] getAlliancesClone() {
    Alliance [] na = new Alliance [alliances.length];
    
    for(int i=0; i<alliances.length; i++) {
      na[i]=(Alliance) alliances[i].clone();
    }

    return na;
  }

  public int getCurrentRound() {
    return currentRound;
  }

  public synchronized void pauseGame() {
    gameIsPaused=true;
    setAllModelineTexts("Game is paused (after this round)");
    hostWindow.putMessage("Game paused.");
    broadcastChatMessage("Game paused (after this round)");
  }

  public synchronized void continueGame() {
    gameIsPaused=false;
    setAllModelineTexts("Game continues");
    hostWindow.putMessage("Game continues");
    broadcastChatMessage("Game continues");
  }

  public synchronized boolean isGamePaused() {
    return gameIsPaused;
  }

  public void setAllModelineTexts(String text) {
    Enumeration e = hostController.getAllPlayers();

    while(e.hasMoreElements()) {
      Integer integer = (Integer) e.nextElement();

      int i = integer.intValue();
      HostWorldMessage hwm = new ModeLineTextMessage(i, text);
      sendMessage(hwm);
    }
  }

  public Enumeration getAllPlayers() {
    return hostController.getAllPlayers();
  }

  public String getPlayerName(int id) {
    return hostController.getPlayerName(id);
  }

  public int getNumberOfPlayers() {
    return hostController.getNumberOfPlayers();
  }

  public int getXSize() {
    return 5000;
  }

  public int getYSize() {
    return getXSize();
  }

  public HostWorld(HostController hc, HostScreen hw, WorldOptions wo) {
    worldOptions = wo;
    hostController = hc;
    hostWindow = hw;
    inputQueue = new Queue();
    outputQueue = new Queue();
    hostWorldComponents=new Hashtable();
    roundTime=worldOptions.getRoundTime(0);
    startRoundMessages = new StartRoundMessage[GlobalDefines.maxPlayers];

    initializeAlliances();
    initializeWorldStates();
  }

  protected void prepareForSaveGame() {}

  protected void restoreAfterLoadGame() {}

  private synchronized void saveState() {
      HostController hc = hostController;
      HostScreen     hs = hostWindow;
      Queue          iq = inputQueue;
      Queue          oq = outputQueue;
      StartRoundMessage sr[] = startRoundMessages;

      /* These are not Serializable */
      /* Note that access to these structures outside of run() method
	 must be synchronized (usually only problem with queues) */

      hostController     = null;
      hostWindow         = null;
      inputQueue         = null;
      outputQueue        = null;
      startRoundMessages = null;

      prepareForSaveGame();

      File oldSaveFile = new File(worldOptions.getSaveGameName());
      if(oldSaveFile.renameTo(new File(worldOptions.getSaveGameBackupName())) 
	 == false) {
	  System.err.println("Warning: Cound not make savegame backup.");
      }

      try {
	  FileOutputStream fos = 
	      new FileOutputStream(worldOptions.getSaveGameName());
	  ObjectOutputStream oos = new ObjectOutputStream(fos);
	  oos.writeObject(this);
	  oos.close();
      } catch (IOException e) {
	  System.err.println("Warning: Could not save game state. " + e);
      }
      
      /* Restore unserializable references */
      hostController     = hc;
      hostWindow         = hs;
      inputQueue         = iq;
      outputQueue        = oq;
      startRoundMessages = sr;
  }

    static HostWorld loadStatus(String filename, 
				HostController hc,
				HostScreen sc) {
	HostWorld loadedWorld = null;

	try {
	    FileInputStream fis = new FileInputStream(filename);
	    ObjectInputStream ois = new ObjectInputStream(fis);
	    loadedWorld = (HostWorld) ois.readObject();
	    ois.close();
	} catch (Exception e) {
	    System.err.println("Warning: Could not load game: " + e);
	}

	if(loadedWorld != null) {
	    loadedWorld.hostController     = hc;
	    loadedWorld.hostWindow         = sc;
	    loadedWorld.inputQueue         = new Queue();
	    loadedWorld.outputQueue        = new Queue();
	    loadedWorld.startRoundMessages =
	      new StartRoundMessage[GlobalDefines.maxPlayers];

	    loadedWorld.gameIsPaused=true;

	    for(int i = 0; i < GlobalDefines.maxPlayers; i++) {
		loadedWorld.oldPlayersInfo.setPlayerActive(i, false);
	    }

	    loadedWorld.restoreAfterLoadGame();
	}

	return loadedWorld;
    }

  PlayersInfo getSavedPlayersInfo() {
    return oldPlayersInfo;
  }

  public synchronized void updatePlayersInfo(PlayersInfo pi) {
    oldPlayersInfo = pi;
  }
    
  public void sendMessage(HostWorldMessage m) {
    m.setRound(currentRound);
    outputQueue.add(m);
  }

  public synchronized void putMessage(WorldMessage m) {
    inputQueue.add(m);
  }

  public synchronized HostWorldMessage getMessage() {
    return (HostWorldMessage) outputQueue.remove();

  }

  public synchronized boolean isMoreMessages() {
    return (!outputQueue.isEmpty());
  }

  public void sendChatMessage(int player, String text) {
    if(player!=-1)
       hostController.sendChatMessage(player,text);
  }

  public void sendChatMessageAs(int toPlayer, int asPlayer, String text) {
     if(toPlayer!=-1)
       hostController.sendChatMessageAs(toPlayer, asPlayer, text);
  }

  public void broadcastChatMessage(String text) {
    hostController.broadcastChatMessage(text);
  }

  public void broadcastChatMessageAs(int asPlayer, String text) {
    hostController.broadcastChatMessageAs(asPlayer, text);
  }

  public HostWorldComponent getComponentById(int id) {
    return (HostWorldComponent) hostWorldComponents.get(new Integer(id));
  }

  public int putComponent(HostWorldComponent hwc) {
    hostWorldComponents.put(new Integer(++lastComponentId),hwc);
    hwc.setId(lastComponentId);
    return lastComponentId;
  }

  public Enumeration getComponentsByLocation(Location l) {

    Enumeration e=hostWorldComponents.elements();
    Hashtable h=new Hashtable();
    HostWorldComponent hwc;

    while(e.hasMoreElements()) {
      hwc = (HostWorldComponent) e.nextElement();
      if(hwc.getLocation().equals(l))
	h.put(new Integer(hwc.getId()), hwc);
    }

    return h.elements();
  }

  public Enumeration getComponents() {
    return ((Hashtable) hostWorldComponents.clone()).elements();
  }

  public boolean removeComponent(int id) {
    if(hostWorldComponents.remove(new Integer(id)) == null)
      return false;
    else
      return true;
  }

  protected void runComponentRounds() {
    Enumeration e = getComponents();
    HostWorldComponent hwc;

    while(e.hasMoreElements()) {
      hwc=(HostWorldComponent) e.nextElement();
      
      hwc.doRound();
    }
  }

  public void runWorldRound() {
    runComponentRounds();
  }

  public boolean isVisibleByComponent(HostWorldComponent target,
				      HostWorldComponent scanner) {

    if(scanner.getScanRange() <= 0)
      return false;

    if(target.isVisible() &&
       (target.getLocation().distance(scanner.getLocation()) -
	target.getRadius()) <=
       (int)
       ((double)scanner.getScanRange()/target.getScanRangeDivisor()) ) 
      return true;
    else
      return false;
  }

  public boolean isVisibleByPlayer(int player, HostWorldComponent hwc) {
    if(hwc==null)
      return false;

    if(hwc.getOwner()==player) {
      return true;
    }

    Enumeration e = getComponents();
    HostWorldComponent scanner;

    while(e.hasMoreElements()) {
      scanner = (HostWorldComponent) e.nextElement();
      if(scanner.getOwner()==player && isVisibleByComponent(hwc, scanner))
	return true;
    }

    return false;
  }

  private void sendComponents() {
    for(int n=0; n<hostController.getMaxPlayer(); n++) {
      sendComponents(n);
    }
  }

  private void sendComponents(int player) {
    sendComponents(player, roundTime); 
  }

  private void sendComponents(int player, int currentRoundTime) {
    if(hostController.playerActive(player)) {
      worldState[player].setTimeoutPossibility(isTimeoutPossible(player));

      Hashtable h = new Hashtable();
      Enumeration e = getComponents();

      while(e.hasMoreElements()) {
	HostWorldComponent hwc = (HostWorldComponent) e.nextElement();
	WorldComponent wc = hwc.getWorldComponent(player);
	if(wc!=null) {
	  wc.setRound(currentRound);
	  h.put(new Integer(wc.getId()), wc); 
	}
      }

      int num=h.size();

      e=h.elements();
      WorldComponent playerComponents[] = new WorldComponent[num];
	
      for(int i=0; i<num; i++)
	playerComponents[i]=(WorldComponent) e.nextElement();

      ScoreEntry score = getScore(player);
      worldState[player].setScore(score.getScore());

      StartRoundMessage srm = new StartRoundMessage(player,
						    playerComponents,
						    alliances[player],
						    currentRoundTime,
						    worldState[player]);
      srm.setRound(currentRound);
      sendMessage(srm);

      startRoundMessages[player] = srm;
    }
  }

  public void gameOver() {
    hostWindow.putMessage("Game Over");
    broadcastChatMessage("\n*********************\n" +
                         "*********************\n" +
                         "\nGame Over\n\n" +
                         scoreboard +
                         "\n*********************\n" +
                         "*********************");
    
  }

  protected boolean isPlayerAlive(int player) {
      /* In some games, player can die. Override this method to
	 prevent "dead" players from stalling the game by not
	 playing their turns. */
      return true;
  }

  public void runWorldPostRound() {
  }

  public void runRound() {
    currentRound++;
    roundTime=worldOptions.getRoundTime(currentRound);

    hostController.resetObjectStreams();

    for(int n=0; n<GlobalDefines.maxPlayers; n++) {
      turnEnded[n]=false;
      startRoundMessages[n]=null;
      if(!isPlayerAlive(n)) {
	  sendChatMessage(n, 
			  "Warning: You are considered dead. " +
			  "Host will not wait for your actions!");
	  turnEnded[n]=true;
      }
    }

    for(int n=0; n<GlobalDefines.maxPlayers; n++) {

    }

    hostWindow.putMessage("Running round " + currentRound + "...");
    runWorldRound();
    scoreboard.changeRound(this, currentRound);
    if(scoreboard.isGameOver(currentRound)) {
	gameOver();
    }
    runWorldPostRound();
    sendComponents();
    hostWindow.putMessage("Round " + currentRound + " ready.");
    broadcastChatMessage("*** Round " + currentRound + " starts ***");
  }

  private void initializeAlliances() {
    alliances=new Alliance[GlobalDefines.maxPlayers];

    for(int i=0; i<GlobalDefines.maxPlayers; i++) {
      alliances[i] = new Alliance(i);
    }
  }

  protected void initializeWorldStates() {
    worldState=new WorldState[GlobalDefines.maxPlayers];

    for(int i=0; i<GlobalDefines.maxPlayers; i++) {
      worldState[i] = new WorldState();
    }
  }

    protected void setScoring(Scoreboard scoreboard) {
	this.scoreboard = scoreboard;
    }

    public ScoreEntry getScore(int player) {
	return scoreboard.getScore(player);
    }

    protected WorldOptions getWorldOptions() {
	return worldOptions;
    }

  protected void doTimeout(int requester) {
      int time = roundTime;

      timeoutTimes[requester] -= roundTime * TIMEOUT_MULTIPLIER;

      if(time > MAX_TIMEOUT)
	  time = MAX_TIMEOUT;

      lastRunTime=new Date(lastRunTime.getTime() + time); 
      String m = "Timeout (" + time/1000 + " seconds)";

      setAllModelineTexts(m);
      hostWindow.putMessage(m);
      broadcastChatMessage(m);

      TimeoutMessage tm = new TimeoutMessage(time/1000);
      hostController.sendTimeout(tm);
  }

  private boolean isTimeoutPossible(int player) {
      if(timeoutTimes[player] >= TIMEOUT_MULTIPLIER * roundTime)
	  return true;
      else
	  return false;
  }
  
  protected void handleWorldStateMessage(WorldStateMessage wsm) {}

  public void run() {
    
    turnEnded=new boolean[GlobalDefines.maxPlayers];
    timeoutTimes=new int[GlobalDefines.maxPlayers];

    for(int nn=0; nn<GlobalDefines.maxPlayers; nn++) {
	timeoutTimes[nn] = 0;
    }

    if(isGamePaused()) {
	lastRunTime=new Date(0);
    } else {
	runRound();
	lastRunTime=new Date(System.currentTimeMillis());
    }
    
    while (true) {

      WorldMessage wm = (WorldMessage) inputQueue.remove(1000);
      if(wm != null) {

	if(wm.getRound() == currentRound) {

	  if(wm instanceof EndTurnMessage) {
	    long timeLeft =lastRunTime.getTime()+roundTime - 
		System.currentTimeMillis() - 
		timeoutTimes[wm.getSource()]/100;
	    if(turnEnded[wm.getSource()])
		timeLeft = 0;

	    turnEnded[wm.getSource()]=true;
	    boolean allDone=true;

	    for(int n=0; n<GlobalDefines.maxPlayers; n++) {
		if(worldOptions.endTurnWhenAway()) {
		    if(hostController.playerActive(n)) {
			if(!turnEnded[n])
			    allDone=false;
		    }
		} else {
		    if(hostController.playerInGame(n)) {
			if(!turnEnded[n])
			    allDone=false;
		    }
		}
	    }
	    
	    if(allDone) {
	      lastRunTime.setTime(lastRunTime.getTime()-roundTime);
	    } else {
		if(timeLeft > 0) {
		    timeoutTimes[wm.getSource()] += timeLeft;
		}
	    }
	  }

	  if(wm instanceof WorldComponentMessage) {
	    HostWorldComponent hwc = 
	      (HostWorldComponent) hostWorldComponents.get(new Integer(((WorldComponentMessage) wm).getComponentId()));

	    if(hwc!=null) {
	      startRoundMessages[wm.getSource()]=null;
	      hwc.putMessage((WorldComponentMessage) wm);
	    }
	  }

	  if(wm instanceof WorldStateMessage) {
	      handleWorldStateMessage((WorldStateMessage) wm);
	  }

	  if(wm instanceof SetAllianceMessage) {
	    startRoundMessages[wm.getSource()]=null;

	    SetAllianceMessage sam = (SetAllianceMessage) wm;
	    Alliance a = sam.getAlliance();
	    a.validate(wm.getSource());
	    alliances[sam.getSource()] = a;
	  }

	  if(wm instanceof ResendTurnMessage) {
	    long turnTimeLostL = System.currentTimeMillis()-
	      lastRunTime.getTime();
	    int turnTimeLost = (int) turnTimeLostL;

	    if(startRoundMessages[wm.getSource()] == null) {
	      ResetTurnClockMessage rtcm = 
		new ResetTurnClockMessage(wm.getSource(), 
					  roundTime - turnTimeLost);
	      sendMessage(rtcm);
	      turnEnded[wm.getSource()]=true;
	    } else {
	      startRoundMessages[wm.getSource()].setRoundTime(roundTime-
							      turnTimeLost);
	      sendMessage(startRoundMessages[wm.getSource()]);	      
	      turnEnded[wm.getSource()]=false;
	    }
	  }

	  if(wm instanceof TimeoutRequestMessage) {
	      if(timeoutTimes[wm.getSource()] >= 
		 roundTime*TIMEOUT_MULTIPLIER) {
		  doTimeout(wm.getSource());
	      }
	  }
	}
      }

      if(lastRunTime.getTime()+roundTime <= System.currentTimeMillis() 
	 && !isGamePaused()) {
	runRound();
	saveState();
	lastRunTime.setTime(System.currentTimeMillis());
      }
    }
  }
}
