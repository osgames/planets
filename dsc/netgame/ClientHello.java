/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;

/**
 * The first message sent to host by client. Contains some handshaking
 * information. 
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class ClientHello implements Serializable {
  

  /**
   * Name of the player. This name will be validated by the host.
   */
  private String playerName;

  /**
  * Password for the player. This password must be valid when rejoining the game.
  */
    
  private String password;
  
  /**
   * Constructs new handshaking message with given playername.
   *
   * @param name Name requested for this player.
   */

  public ClientHello(String name, String password) {
    playerName=name;
    this.password=password;
  }

  /**
   * Gets name stored in this handshaking message.
   *
   * @return Player's name. This is never null.
   */

  public String getName() {
    if(playerName==null)
      return "NONAME";
    else
      return playerName;
  }

    public String getPassword() {
	if(password == null)
	    return "NOPASS";
	else
	    return password;
    }
}
