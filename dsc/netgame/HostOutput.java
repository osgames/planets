/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.io.*;


class HostOutput extends Thread {

  private ObjectInputStream inputStream;
  private ClientChatter chatter;
  private ClientController controller;

  HostOutput(ObjectInputStream inputStream, ClientChatter chatter, ClientController controller) {
    this.inputStream=inputStream;
    this.chatter=chatter;
    this.controller=controller;
  }

  public void run() {
    try {
      while(true) {
	  Object o = inputStream.readObject();
	  if(! (o instanceof Message)) {
	    System.err.println("Data stream corrupted");
	    System.exit(1);
	  }

	  if(o instanceof ControlMessage) {
	    if(o instanceof PlayersInGameMessage) {
	      controller.playersInfo=((PlayersInGameMessage) o).players;
	      chatter.playerNotify();
	    }
	    else if(o instanceof PlayerSetsName) {
	      PlayerSetsName psn = (PlayerSetsName) o;
	      controller.playersInfo.setPlayerName(psn.getPlayerNumber(),
						   psn.getPlayerName());
	      chatter.playerNotify();
	    } 
	    else if(o instanceof PlayerNumberMessage) {
	      controller.playerNumber=((PlayerNumberMessage) o).getMyPlayerNumber();
	    }
	    else if(o instanceof StartGameMessage) {
	      controller.startGame((StartGameMessage) o);
	    } else if(o instanceof TimeoutMessage) {
	      controller.timeout((TimeoutMessage) o);
	    }
	  }

	  if(o instanceof ChatMessage) {
	    chatter.putMessage((ChatMessage) o);
	  }
	  
	  if(o instanceof HostWorldMessage) {
	    controller.putWorldMessage((HostWorldMessage) o);
	  }

	  try {
	    sleep(30);
	  } catch (Exception e) {}
      }
    } catch(Exception e) {
      System.err.println("(" + e + ")");
      controller.hostConnectLost();
    }
  }
}
