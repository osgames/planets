/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;


/** WorldProperties contains customization information for dsc.netgame
 * framework. It has default values so there is no need to extend it
 * in the beginning of new project, but later it can be used to
 * set names of different windows and locations of images.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class WorldProperties {
  
  /** Maps property name into string. This version just returns
   * the property name. Property names are usually names of the 
   * classes that are used to display game windows and returned
   * strings are used in title bars.
   *
   * @param property Name of the property
   * @return String that should be shown to player
   */

  public String getName(String property) {
    return property;
  }
}
