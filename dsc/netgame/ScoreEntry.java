/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;
import dsc.util.*;
import java.util.*;

public abstract class ScoreEntry implements java.io.Serializable, Sortable{

    private String playerName;
    protected int playerNumber;

    public ScoreEntry(String name, int playerNumber) {
	this.playerName    = name;
	this.playerNumber  = playerNumber;
    }

    protected void update(Scoreboard scoreboard,
			  int newRoundNro,
			  HostWorld world) {

    }

    public int getScore() {
	return 0;
    }

    public String toString() {
	return playerName + ": " + getScore();
    }

    public int greaterThan(Sortable s) {
	if(getScore() > ((ScoreEntry) s).getScore()) {
	    return -1;
	}

	if(getScore() < ((ScoreEntry) s).getScore()) {
	    return 1;
	}
	
	return 0;
    }
}
