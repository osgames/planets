/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.netgame;

import java.awt.*;
import java.awt.event.*;
import dsc.awt.*;
import java.util.*;


public class ObjectSelectionWindow extends LayoutableFrame 
                                   implements MouseListener {

    private ClientController controller;
    private ScrollPane scrollPane;
    private Panel mainPanel;
    private GridBagConstraints gbc;

    public void addToPanel(Panel p,Component c, 
			   GridBagConstraints gbc, 
			   int x, int y, int w, int h) {
	gbc.gridx=x;
	gbc.gridy=y;
	gbc.gridwidth=w;
	gbc.gridheight=h;
	p.add(c,gbc);
    }

    private void allocateResources() {

    }

    private void reconstructPanel() {
	allocateResources();
	pack();
	repaint();
    }

    public void updateNotify() {
	reconstructPanel();
    }

    public ObjectSelectionWindow(ClientController controller,
				 ScreenLayout screenLayout) {
	this.controller=controller;
	setTitle(controller.getWorldProperties().
		 getName("ObjectSelectionWindow"));

	WindowInfo windowInfo = 
	    screenLayout.getWindowInfo("ObjectSelectionWindow");

	if(windowInfo == null) {
	    setSize(5*40,6*40);
	} else {
	    setSize(windowInfo.getXSize(), windowInfo.getYSize());
	    setLocation(windowInfo.getXCoord(), windowInfo.getYCoord());
	}

	Image i=GraphicsLoader.getImage("data/graph/still/icons/receiver.gif");
	setIconImage(i);

	gbc = new GridBagConstraints();
	gbc.fill=GridBagConstraints.BOTH;
	gbc.weightx=100;
	gbc.weighty=100;

	reconstructPanel();

	scrollPane = new ScrollPane();
	scrollPane.add(mainPanel);
	scrollPane.setSize(5*40,6*40);
	add(scrollPane);

	pack();
    }

    public void mouseClicked(MouseEvent e) {
	
    }

    public void mousePressed(MouseEvent e)  {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e)  {}
    public void mouseExited(MouseEvent e)   {}

}
