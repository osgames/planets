/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;
import java.awt.event.*;


/** 
 * Complete dialog window for "Really quit?" questions. 
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class QuitQueryWindow extends Dialog implements ActionListener
{

  private Button okButton, cancelButton;
  private boolean ready;
  private Frame parentWindow;
  private QuitQueryCallback okCallBack=null;
  private QuitQueryCallback cancelCallBack=null;
  
  private static Image backgroundImage = null;

  private Button createButton(String name) {
    Button b = new Button(name);
    b.addActionListener(this);
    return b;
  }

  public void setOKQuitCallback(QuitQueryCallback callBack) {
    okCallBack=callBack;
  }

  public void setCancelQuitCallback(QuitQueryCallback callBack) {
    cancelCallBack=callBack;
  }

  public void update(Graphics g) {
    paint(g);
  }

  public void paint(Graphics g) {
    Insets in = getInsets();
    g.drawImage(backgroundImage,in.left,in.top,this);
  }

  public QuitQueryWindow(Frame w) { 
    super(w, "Really Quit?", true);

    int xSize, ySize;

    ready=false;
    parentWindow=w;

    if(backgroundImage == null)
      backgroundImage = GraphicsLoader.getImage("data/graph/still/background/quit.gif");

    Insets in = getInsets();
    xSize=backgroundImage.getWidth(null)+in.right+in.left;
    ySize=backgroundImage.getHeight(null)+in.bottom+in.top;

    Dimension parentSize = w.getSize();

    Point parentP = w.getLocation();
    setLocation(parentP.x+(parentSize.width-xSize)/2, parentP.y+(parentSize.height-ySize)/2);
    setSize(xSize,ySize);
    setResizable(false);

    setLayout(null);

    okButton=createButton("  OK  ");
    cancelButton=createButton("Cancel");

    setLayout(new BorderLayout());
    Panel p = new Panel();
    p.setLayout(new FlowLayout());

    p.add(okButton);
    p.add(cancelButton);
    
    p.setBackground(new Color(100,100,100));
    p.setForeground(new Color(255,255,255));
    p.setBackground(new Color(0));

    add(p,"South");
  }
    
  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();
    
    if(arg.equals("  OK  ")) {
      if(okCallBack==null)
	System.exit(0);
      else {
	okCallBack.quitOkPressed();
      }
    }
    else if(arg.equals("Cancel")) {
      if(cancelCallBack!=null)
	cancelCallBack.quitCancelPressed();
      dispose();
    }
  }
}






