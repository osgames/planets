/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;
import java.util.Hashtable;


/** ImageLoadBuffer acts as a buffer that contains loaded images.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class ImageLoadBuffer {

  private Hashtable images;

  /** Creates new empty buffer.
   */

  public ImageLoadBuffer() {
    images=new Hashtable();
  }

  /** Gets image which name is given. If the image is in the buffer it is 
   * not re-loaded. Loads image when needed and stores it in the buffer.
   *
   * @param filename Filename of the image to be returned.
   * @return Loaded or buffered image.
   */

  public Image getImage(String filename) {
    Image i;

    if(filename==null)
      return null;

    if(!images.containsKey(filename)) {
      i=GraphicsLoader.getImage(filename);
      images.put(filename,i);
    } else {
      i=(Image) images.get(filename);
    }

    return i;
  }
}
