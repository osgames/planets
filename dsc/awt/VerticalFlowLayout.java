/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/


package dsc.awt;

import java.awt.*;


public class VerticalFlowLayout implements LayoutManager {
  public void addLayoutComponent(String name, Component comp) {}

  public void removeLayoutComponent(Component comp) {}

  public void setSizes(Container parent)
  {
    if(sizesSet) return;
    int n=parent.getComponentCount();

    preferredWidth=0;
    preferredHeight=0;
    minWidth=0;
    minHeight=0;
    maxComponentWidth=0;
    maxComponentHeight=0;

    for(int i=0; i<n ; i++)
    {
      Component c = parent.getComponent(i);
      if(c.isVisible()) {
	Dimension d = c.getPreferredSize();
	maxComponentWidth = Math.max(maxComponentWidth, d.width);
	maxComponentHeight = Math.max(maxComponentHeight, d.height);
	minHeight += d.height +1;
      }
    }

    preferredHeight = minHeight;
    minWidth  = preferredWidth = maxComponentWidth;
    sizesSet  = true;
  }

  public Dimension preferredLayoutSize(Container parent)
  {
    Dimension dim = new Dimension(0,0);
    setSizes(parent);
    Insets insets = parent.getInsets();
    dim.width = preferredWidth + insets.left + insets.right;
    dim.height = preferredHeight + insets.top + insets.bottom;
    return dim;
  }

  public Dimension minimumLayoutSize(Container parent)
  {
    Dimension dim = new Dimension(0,0);
    setSizes(parent);
    Insets insets = parent.getInsets();
    dim.width = minWidth + insets.left + insets.right;
    dim.height = minHeight + insets.top + insets.bottom;
    return dim;

  }

  public void layoutContainer(Container parent)
  {
    Insets insets = parent.getInsets();
    int currentrow=0;

    setSizes(parent);

    int containerWidth = parent.getSize().width - insets.left - insets.right;
    int containerHeight = parent.getSize().height - insets.top - insets.bottom;

    int n=parent.getComponentCount();
    for (int i = 0; i < n; i++)
    {
      Component c = parent.getComponent(i);
      
      if(c.isVisible()) {
	Dimension d = c.getPreferredSize();
	c.setBounds(insets.left, insets.top + currentrow,
		    containerWidth, d.height);
	currentrow+=d.height+1;
      }
	
    }
  }

  private int minWidth = 0;
  private int minHeight = 0;
  private int preferredWidth = 0, preferredHeight = 0;
  private boolean sizesSet = false;
  private int maxComponentWidth = 0;
  private int maxComponentHeight = 0;

}










