/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;
import java.awt.event.*;


/** Does not work correctly at the moment. Do not use.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class ErrorDialog extends Dialog implements ActionListener
{

  private Button quitButton, restartButton;
  private Frame parentWindow;
  private Frame restartWindow;
  
  private Button createButton(String name) {
    Button b = new Button(name);
    b.addActionListener(this);
    return b;
  }

  public ErrorDialog(Frame parent, Frame restart, String errorText) { 
    super(parent, errorText, true);

    setTitle(errorText);
    int xSize=200, ySize=140;
    parentWindow=parent;
    restartWindow=restart;

    Dimension parentSize = parent.getSize();

    Point parentP = parent.getLocation();
    setLocation(parentP.x+(parentSize.width-xSize)/2, parentP.y+(parentSize.height-ySize)/2);
    setSize(xSize,ySize);

    setLayout(null);

    quitButton=createButton("Quit");
    restartButton=createButton("Restart");

    setLayout(new BorderLayout());
    Panel p = new Panel();
    p.setLayout(new FlowLayout());

    p.add(quitButton);
    p.add(restartButton);
        
    add(p,"South");
    add(new Label(errorText),"North");
  }
    
  public void actionPerformed(ActionEvent evt) {
    String arg = evt.getActionCommand();
    
    if(arg.equals("Quit")) System.exit(0);
    else if(arg.equals("Restart")) {
      if(restartWindow!=null)
	restartWindow.show();
      dispose();
    }
  }
}

