/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;
import java.awt.event.*;


/**
 * Simple frame that supports all absolutely nessecery window
 * functions. It also contains some often needed methods for layout.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public abstract class LayoutableFrame extends Frame {
  private GridBagConstraints gridBgConstraints=null;

  /** Creates new frame with size of 300x200. Sets the name of the Frame to
   * be the name of it's class.
   */

  public LayoutableFrame() {
    setSize(300,200);
    setTitle(getClass().getName());
    
  }

  /** Creates button and adds action listener into it.
   *
   * @param name Buttons text.
   * @param a ActionListener for the button.
   * @return The button.
   */

  public Button createButton(String name, ActionListener a) {
    Button b = new Button(name);
    b.addActionListener(a);
    return b;
  }

  /** Adds component in this frame that has GribBackLayout set as it's
   * layout manager.
   *
   * @param c Component to add.
   * @param gbc Constraints for the layout.
   * @param x X-position in the grid.
   * @param y Y-position in the grid.
   * @param w Width of the component in the grid.
   * @param h Height of the component in the grid.
   */
  
  public void add(Component c, GridBagConstraints gbc, int x, int y, int w, int h) {
    gbc.gridx=x;
    gbc.gridy=y;
    gbc.gridwidth=w;
    gbc.gridheight=h;
    add(c,gbc);
  }

  /** Adds component in this frame that has GribBackLayout set as it's
   * layout manager.
   *
   * @param c Component to add.
   * @param gbcWeightx X-weight of the component.
   * @param gbcWeighty Y-weight of the component.
   * @param anchor Anchor direction of the component.
   * @param fill Fill directions of the component.
   * @param x X-position in the grid.
   * @param y Y-position in the grid.
   * @param w Width of the component in the grid.
   * @param h Height of the component in the grid.
   */

  public void gbAdd(Component c, int gbcWeightx, int gbcWeighty, int anchor, int fill, int x, int y, int w, int h) {
    if(gridBgConstraints == null) {
      gridBgConstraints = new GridBagConstraints();
    }

    gridBgConstraints.weightx=gbcWeightx;
    gridBgConstraints.weighty=gbcWeighty;
    gridBgConstraints.anchor=anchor;
    gridBgConstraints.fill=fill;
    gridBgConstraints.gridx=x;
    gridBgConstraints.gridy=y;
    gridBgConstraints.gridwidth=w;
    gridBgConstraints.gridheight=h;
    add(c,gridBgConstraints);
  }

  /** Adds component in this frame that has GribBackLayout set as it's
   * layout manager. Sets center anchoring and bi-directional filling.
   *
   * @param c Component to add.
   * @param gbcWeightx X-weight of the component.
   * @param gbcWeighty Y-weight of the component.
   * @param x X-position in the grid.
   * @param y Y-position in the grid.
   * @param w Width of the component in the grid.
   * @param h Height of the component in the grid.
   */

  public void gbAdd(Component c, int gbcWeightx, int gbcWeighty, int x, int y, int w, int h) {
    if(gridBgConstraints == null) {
      gridBgConstraints = new GridBagConstraints();
    }

    gridBgConstraints.weightx=gbcWeightx;
    gridBgConstraints.weighty=gbcWeighty;
    gridBgConstraints.anchor=GridBagConstraints.CENTER;
    gridBgConstraints.fill=GridBagConstraints.BOTH;
    gridBgConstraints.gridx=x;
    gridBgConstraints.gridy=y;
    gridBgConstraints.gridwidth=w;
    gridBgConstraints.gridheight=h;
    add(c,gridBgConstraints);
  }

  /** Sets frames size and centers it on the screen.
   *
   * @param width New width of the frame.
   * @param height New height of the frame.
   */

  public void centerSetSize(int width, int height) {
    Toolkit tk = Toolkit.getDefaultToolkit();
    Dimension d = tk.getScreenSize();
    int screenHeight = d.height;
    int screenWidth  = d.width;

    setSize(width,height);
    setLocation((screenWidth-width)/2,(screenHeight-height)/2);
  }
}
