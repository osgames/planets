/*
  Planetary Hoppers - The networked strategy game
  Copyright (c) 1999-2000 Dodekaedron Software Creations, Inc.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

  Dodekaedron Software Creations, Inc.
  P.O. Box 143
  Fin-01511 Vantaa
  Finland
  
  tjt@users.sourceforge.net  
*/

package dsc.awt;

import java.awt.*;


/** Simple component that holds and image and maintains fixed size.
 *
 * @author Dodekaedron Software Creations, Inc. -- Wraith
 */

public class FixedsizeImageComponent extends ImageComponent {
    private int xsize, ysize;

    /** Paints the image 
     */

    public void paint(Graphics g) {
	g.drawImage(getImage(), 0, 0, xsize, ysize, this);
    }
    
    /** Creates new component that holds given image.
     *
     * @param i Image to show on the component.
     */

    public FixedsizeImageComponent (Image i, int xsize, int ysize) {
	super(i);
	this.xsize = xsize;
	this.ysize = ysize;
    }

    /** Gets dimensions of the component (and the image)
     *
     * @return Mininum dimensions of the component.
     */

    public Dimension getMinimumSize() {
	return new Dimension(xsize, ysize);
    }
}
